<html>
<head>
    <title>Form Pendaftaran Pemohon</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <title>BLUD</title>
    <!-- Latest compiled and minified CSS -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!--Font Awesome Bootsrap 4 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!--Fonts Google-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
    <style type="text/css">
    body{
        background-image: url(<?php echo base_url('assets/images/bg/bgBlud.png') ?>);
        background-repeat: no-repeat, repeat;
        background-size:cover;
    }

.container{
height: 100%;
align-content: center;
}

.card{
height: 340px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5)!important;
}

.social_icon span{
font-size: 60px;
margin-left: 10px;
color: #FFC312;
}

.social_icon span:hover{
color: white;
cursor: pointer;
}

.card-header h3{
color: white;
margin-top: 20px;
margin-bottom:-10px  
}

.social_icon{
position: absolute;
right: 20px;
top: -45px;
}

.input-group-prepend span{
width: 50px;
background-color: #FFC312;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}

.remember{
color: white;
}

.remember input
{
width: 20px;
height: 20px;
margin-left: 15px;
margin-right: 5px;
}

.login_btn{
color: black;
background-color: orange;
width: 130px;

}

.login_btn:hover{
color: black;
background-color: white;
}

.login_bck{
color: black;
background-color: orange;
width: 220px;

}

.login_bck:hover{
color: black;
background-color: red;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}
/*<!-- CSS Start -->*/

/* unvisited link */
a:link {
  color: orange;
  text-decoration: none;
  display: inline-block;
}

/* visited link */
a:visited {
  color: orange;
}

/* mouse over link */
a:hover {
  color: red;
}

/* selected link */
a:active {
  color: orange ;
} 

    </style>   
</head>
<body>
    <!-- <h2>Form Pendaftaran Pemohon</h2>
    <h4>Silahkan Isi Data Pada Kolom Tersedia!</h4>
    <form action="<?php echo base_url(). 'pemohon/req_pemohon/tambah_aksi'?>" method="post">        
        <table>
            <tr>
                <td width="120">Username</td>
                <td><input type="text" name="username"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="pass"></td>
            </tr>
            <tr>
                <td>Level</td>
                <td><input type="text" name="level" value="pemohon" readonly="" ></td>
            </tr>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Submit"></td>
            </tr>
        </table>
    </form>


 -->

 <div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card zoomIn animated">
            <div class="card-header">
                <h3 style="font-family: 'Roboto Condensed', sans-serif;"><center>Form Pendaftaran Pemohon</center></h3>
            </div>
            <div class="card-body">
                <a><?php echo $this->session->flashdata('user_ada'); ?></a>
                <form action="<?php echo base_url(). 'pemohon/req_pemohon/tambah_aksi'?>" method="post">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input method="post" type="text" name="username" class="form-control" placeholder="Masukkan Nama Pemohon" required="true" 
                        >
                        
                    </div>  
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input method="post" type="password" name="pass" class="form-control" placeholder="Masukkan Kata Sandi" required="true">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-id-badge"></i></span>
                        </div>
                        <input method="post" type="text" name="level" class="form-control" value="pemohon" readonly="true">
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="submit" value="Daftar" class="btn float-right login_btn ">
                        <!-- <input type="submit" value="Keluar" class="btn float-lefts login_bck "> -->
                    </div>
                </form>
                <form method="post" action="<?php echo base_url() ?>" class="user">
                    <div class="form-group">
                        <input type="submit" value="Kembali Ke Halaman Utama" class="btn float-lefts login_bck ">
                    </div>
                </form>
                <div style="color: white; text-align: center">Jika Sudah Pernah Mendaftar Silahkan <b><a href="<?php echo base_url('auth/login') ?>">LOGIN !</a></b></div>
            </div>

        </div>
    </div>
</div>
</body>
</html>