<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <div>
                        <center><h2>Halaman Ini Memberikan Informasi Proses Permohonan Anda</h2></center>
                        <center><h3 >Mohon Dicek Secara Berkala Dengan Login Ke Sistem </h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                          <form method="post" action="<?php echo base_url('pemohon/data_kuasa/tambah_kuasa');?>" enctype="multipart/form-data">
                              <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th class="center">Nama Lengkap</th>
                                    <th class="center">No NIK</th>
                                    <th class="center">Tanggal Permohonan</th>
                                    <th class ="center">Keterangan</th>
                                  </tr>
                                </thead>
                                <?php foreach ($data as $key => $dt): ?>
                                <tbody>
                                  <tr>
                                    <td><?php echo $dt->nama_lengkap; ?></td>
                                    <td><?php echo $dt->no_ktp; ?></td>
                                    <td><?php echo $dt->tgl_pemohon; ?></td>
                                    <td><?php echo $dt->ket; ?></td>
                                  </tr>
                                </tbody>
                              <?php endforeach;?>
                              </table>
                          </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>