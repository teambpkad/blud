<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="">
<!--PAGE NEXT-->  <div class="">
                    <div class="">
                        <center><h3>Laporan Laba/Rugi Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                          <form method="post" action="<?php echo base_url('pemohon/data_laba/tambah_laba');?>" enctype="multipart/form-data">
                            <div class="table-responsive">
                              <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <div class="form-group">
                                      <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                    </div>
                                    <th scope="col">NO</th>
                                    <th scope="col">URAIAN</th>
                                    <th>BULAN<span style="margin:5px "></span>
                                      <select name="bulan1">
                                        <option value="Januari"> Januari </option>
                                        <option value="Februari"> Februari </option>
                                        <option value="Maret"> Maret </option>
                                        <option value="April"> April </option>
                                        <option value="Mei"> Mei </option>
                                        <option value="Juni"> Juni </option>
                                        <option value="Juli"> Juli </option>
                                        <option value="Agustus"> Agustus </option>
                                        <option value="September"> September </option>
                                        <option value="November"> November </option>
                                        <option value="Oktober"> Oktober </option>
                                        <option value="Desember"> Desember </option>
                                      </select>
                                    </th>
                                    <th>BULAN<span style="margin:5px "></span>
                                      <select name="bulan2">
                                        <option value="Januari"> Januari </option>
                                        <option value="Februari"> Februari </option>
                                        <option value="Maret"> Maret </option>
                                        <option value="April"> April </option>
                                        <option value="Mei"> Mei </option>
                                        <option value="Juni"> Juni </option>
                                        <option value="Juli"> Juli </option>
                                        <option value="Agustus"> Agustus </option>
                                        <option value="September"> September </option>
                                        <option value="November"> November </option>
                                        <option value="Oktober"> Oktober </option>
                                        <option value="Desember"> Desember </option>
                                      </select>
                                    </th>
                                    <th>BULAN<span style="margin:5px "></span>
                                      <select name="bulan3">
                                        <option value="Januari"> Januari </option>
                                        <option value="Februari"> Februari </option>
                                        <option value="Maret"> Maret </option>
                                        <option value="April"> April </option>
                                        <option value="Mei"> Mei </option>
                                        <option value="Juni"> Juni </option>
                                        <option value="Juli"> Juli </option>
                                        <option value="Agustus"> Agustus </option>
                                        <option value="September"> September </option>
                                        <option value="November"> November </option>
                                        <option value="Oktober"> Oktober </option>
                                        <option value="Desember"> Desember </option>
                                      </select>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th rowspan="3">1</th> 
                                    <td><strong><u>PENDAPATAN</u></strong> </td>
                                    <td colspan="3"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>PENDAPATAN USAHA</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendusaha11" type="text" name="pendusaha1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendusaha22"  type="text" name="pendusaha2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendusaha33" type="text" name="pendusaha3" style="width: 80%"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>PENDAPATAN LAIN_LAIN</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendlain1" type="text" name="pendlain1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendlain22" type="text" name="pendlain2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pendlain33" type="text" name="pendlain3" style="width: 80%"></td>
                                  </tr>
                                   <tr>
                                    <th rowspan="8">2</th>
                                    <td><strong><u>PENGELUARAN</u></strong> </td>
                                    <td colspan="3"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>PEMBELIAN BAHAN BAKU</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengbahan11" type="text" name="pengbahan1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengbahan22" type="text" name="pengbahan2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengbahan33" type="text" name="pengbahan3" style="width: 80%"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>TENAGA KERJA<strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtenaga11" type="text" name="pengtenaga1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtenaga22" type="text" name="pengtenaga2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtenaga33" type="text" name="pengtenaga3" style="width: 80%"></td>
                                  <tr>
                                    <td><strong>LISTRIK, AIR, TELP/HP, DLL</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglistrik11" type="text" name="penglistrik1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglistrik22" type="text" name="penglistrik2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglistrik33" type="text" name="penglistrik3" style="width: 80%"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>BIAYA ADMINISTRASI</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengadm11" type="text" name="pengadm1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengadm22" type="text" name="pengadm2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengadm33" type="text" name="pengadm3" style="width: 80%"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>TRANSAPORTASI</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtran11" type="text" name="pengtran1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtran22" type="text" name="pengtran2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="pengtran33" type="text" name="pengtran3" style="width: 80%"></td>
                                  </tr>
                                  <tr>
                                    <td><strong>PENGELUARAN LAIN-LAIN</strong></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglain11" type="text" name="penglain1" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglain22" type="text" name="penglain2" style="width: 80%"></td>
                                    <td><span style="margin: 10px">Rp</span><input id="penglain33" type="text" name="penglain3" style="width: 80%"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                              <div class="pull-right">
                                  <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                              </div>
                          </form>
                      </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
                    $('.mirror').on('keyup', function() {
    $('.'+$(this).attr('class')).val($(this).val());
});
</script>
<script type="text/javascript">
  var pendusaha11 = document.getElementById("pendusaha11");
  pendusaha11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendusaha11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendusaha11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      pendusaha11 += separator + ribuan.join(".");
    }

    pendusaha11 = split[1] != undefined ? pendusaha11 + "," + split[1] : pendusaha11;
    return prefix == undefined ? pendusaha11 : pendusaha11 ? pendusaha11 : "";
  }

  var pendusaha22 = document.getElementById("pendusaha22");
  pendusaha22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendusaha22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah2(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendusaha22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pendusaha22 += separator + ribuan.join(".");
    }

    pendusaha22 = split[1] != undefined ? pendusaha22 + "," + split[1] : pendusaha22;
    return prefix == undefined ? pendusaha22 : pendusaha22 ? pendusaha22 : "";
  }

   var pendusaha33 = document.getElementById("pendusaha33");
  pendusaha33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendusaha33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatrupiah3(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendusaha33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      pendusaha33 += separator + ribuan.join(".");
    }

    pendusaha33 = split[1] != undefined ? pendusaha33 + "," + split[1] : pendusaha33;
    return prefix == undefined ? pendusaha33 : pendusaha33 ? pendusaha33 : "";
  }

  var pendlain1 = document.getElementById("pendlain1");
  pendlain1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendlain1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendlain1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pendlain1 += separator + ribuan.join(".");
    }

    pendlain1 = split[1] != undefined ? pendlain1 + "," + split[1] : pendlain1;
    return prefix == undefined ? pendlain1 : pendlain1 ? pendlain1 : "";
  }

  var pendlain22 = document.getElementById("pendlain22");
  pendlain22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendlain22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendlain22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pendlain22 += separator + ribuan.join(".");
    }

    pendlain22 = split[1] != undefined ? pendlain22 + "," + split[1] : pendlain22;
    return prefix == undefined ? pendlain22 : pendlain22 ? pendlain22 : "";
  }

  var pendlain33 = document.getElementById("pendlain33");
  pendlain33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pendlain33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pendlain33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pendlain33 += separator + ribuan.join(".");
    }

    pendlain33 = split[1] != undefined ? pendlain33 + "," + split[1] : pendlain33;
    return prefix == undefined ? pendlain33 : pendlain33 ? pendlain33 : "";
  }

  var pengbahan11 = document.getElementById("pengbahan11");
  pengbahan11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengbahan11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengbahan11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengbahan11 += separator + ribuan.join(".");
    }

    pengbahan11 = split[1] != undefined ? pengbahan11 + "," + split[1] : pengbahan11;
    return prefix == undefined ? pengbahan11 : pengbahan11 ? pengbahan11 : "";
  }

  var pengbahan22 = document.getElementById("pengbahan22");
  pengbahan22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengbahan22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengbahan22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengbahan22 += separator + ribuan.join(".");
    }

    pengbahan22 = split[1] != undefined ? pengbahan22 + "," + split[1] : pengbahan22;
    return prefix == undefined ? pengbahan22 : pengbahan22 ? pengbahan22 : "";
  }

  var pengbahan33 = document.getElementById("pengbahan33");
  pengbahan33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengbahan33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengbahan33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengbahan33 += separator + ribuan.join(".");
    }

    pengbahan33 = split[1] != undefined ? pengbahan33 + "," + split[1] : pengbahan33;
    return prefix == undefined ? pengbahan33 : pengbahan33 ? pengbahan33 : "";
  }

  var pengtenaga11 = document.getElementById("pengtenaga11");
  pengtenaga11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtenaga11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtenaga11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtenaga11 += separator + ribuan.join(".");
    }

    pengtenaga11 = split[1] != undefined ? pengtenaga11 + "," + split[1] : pengtenaga11;
    return prefix == undefined ? pengtenaga11 : pengtenaga11 ? pengtenaga11 : "";
  }

  var pengtenaga22 = document.getElementById("pengtenaga22");
  pengtenaga22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtenaga22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtenaga22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtenaga22 += separator + ribuan.join(".");
    }

    pengtenaga22 = split[1] != undefined ? pengtenaga22 + "," + split[1] : pengtenaga22;
    return prefix == undefined ? pengtenaga22 : pengtenaga22 ? pengtenaga22 : "";
  }

  var pengtenaga33 = document.getElementById("pengtenaga33");
  pengtenaga33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtenaga33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtenaga33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtenaga33 += separator + ribuan.join(".");
    }

    pengtenaga33 = split[1] != undefined ? pengtenaga33 + "," + split[1] : pengtenaga33;
    return prefix == undefined ? pengtenaga33 : pengtenaga33 ? pengtenaga33 : "";
  }

  var penglistrik11 = document.getElementById("penglistrik11");
  penglistrik11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglistrik11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglistrik11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglistrik11 += separator + ribuan.join(".");
    }

    penglistrik11 = split[1] != undefined ? penglistrik11 + "," + split[1] : penglistrik11;
    return prefix == undefined ? penglistrik11 : penglistrik11 ? penglistrik11 : "";
  }

  var penglistrik22 = document.getElementById("penglistrik22");
  penglistrik22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglistrik22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglistrik22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglistrik22 += separator + ribuan.join(".");
    }

    penglistrik22 = split[1] != undefined ? penglistrik22 + "," + split[1] : penglistrik22;
    return prefix == undefined ? penglistrik22 : penglistrik22 ? penglistrik22 : "";
  }

  var penglistrik33 = document.getElementById("penglistrik33");
  penglistrik33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglistrik33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglistrik33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglistrik33 += separator + ribuan.join(".");
    }

    penglistrik33 = split[1] != undefined ? penglistrik33 + "," + split[1] : penglistrik33;
    return prefix == undefined ? penglistrik33 : penglistrik33 ? penglistrik33 : "";
  }

  var pengadm11 = document.getElementById("pengadm11");
  pengadm11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengadm11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengadm11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengadm11 += separator + ribuan.join(".");
    }

    pengadm11 = split[1] != undefined ? pengadm11 + "," + split[1] : pengadm11;
    return prefix == undefined ? pengadm11 : pengadm11 ? pengadm11 : "";
  }

  var pengadm22 = document.getElementById("pengadm22");
  pengadm22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengadm22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengadm22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengadm22 += separator + ribuan.join(".");
    }

    pengadm22 = split[1] != undefined ? pengadm22 + "," + split[1] : pengadm22;
    return prefix == undefined ? pengadm22 : pengadm22 ? pengadm22 : "";
  }

  var pengadm33 = document.getElementById("pengadm33");
  pengadm33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengadm33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengadm33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengadm33 += separator + ribuan.join(".");
    }

    pengadm33 = split[1] != undefined ? pengadm33 + "," + split[1] : pengadm33;
    return prefix == undefined ? pengadm33 : pengadm33 ? pengadm33 : "";
  }

  var pengtran11 = document.getElementById("pengtran11");
  pengtran11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtran11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtran11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtran11 += separator + ribuan.join(".");
    }

    pengtran11 = split[1] != undefined ? pengtran11 + "," + split[1] : pengtran11;
    return prefix == undefined ? pengtran11 : pengtran11 ? pengtran11 : "";
  }

  var pengtran22 = document.getElementById("pengtran22");
  pengtran22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtran22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtran22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtran22 += separator + ribuan.join(".");
    }

    pengtran22 = split[1] != undefined ? pengtran22 + "," + split[1] : pengtran22;
    return prefix == undefined ? pengtran22 : pengtran22 ? pengtran22 : "";
  }

  var pengtran33 = document.getElementById("pengtran33");
  pengtran33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    pengtran33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      pengtran33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      pengtran33 += separator + ribuan.join(".");
    }

    pengtran33 = split[1] != undefined ? pengtran33 + "," + split[1] : pengtran33;
    return prefix == undefined ? pengtran33 : pengtran33 ? pengtran33 : "";
  }

  var penglain11 = document.getElementById("penglain11");
  penglain11.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglain11.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglain11 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglain11 += separator + ribuan.join(".");
    }

    penglain11 = split[1] != undefined ? penglain11 + "," + split[1] : penglain11;
    return prefix == undefined ? penglain11 : penglain11 ? penglain11 : "";
  }

  var penglain22 = document.getElementById("penglain22");
  penglain22.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglain22.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglain22 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglain22 += separator + ribuan.join(".");
    }

    penglain22 = split[1] != undefined ? penglain22 + "," + split[1] : penglain22;
    return prefix == undefined ? penglain22 : penglain22 ? penglain22 : "";
  }

  var penglain33 = document.getElementById("penglain33");
  penglain33.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    penglain33.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah4(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      penglain33 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "," : "";
      penglain33 += separator + ribuan.join(".");
    }

    penglain33 = split[1] != undefined ? penglain33 + "," + split[1] : penglain33;
    return prefix == undefined ? penglain33 : penglain33 ? penglain33 : "";
  }
</script>

