<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h3>Biodata Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                                <form method="post" action="<?php echo base_url('pemohon/data_biodata/tambah_bio');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap"required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No KTP</label>
                                                <input type="text" class="form-control" name="no_ktp" placeholder="No KTP" required=""  value="<?php echo  $this->session->userdata("username");?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai KTP</label><br>
                                            <textarea name="alamat_ktp" style="width:100%;padding:10px;" placeholder="Alamat KTP" rows="2" cols="80" ></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai Domisili</label><br>
                                            <textarea name="alamat_domisili" style="width:100%;padding:10px;" placeholder="Alamat Domisili" rows="2" cols="80"></textarea>
                                        </div>
                                        <div class="row"> 
                                            <div class="form-group col-md-6">
                                                <label>No HP/Telp</label><br>
                                                <input type="text" class="form-control" name="no_hp" placeholder="No HP/Telp"> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Status Rumah</label><br>
                                                        <select type="text" name="status_rumah" class="form-control" required="">
                                                        <option value="Milik Sendiri">Milik Sendiri</option>
                                                        <option value="Sewa">Sewa</option>
                                                      </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="tgl_lahir" required="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status</label>
                                                <select type="text" name="status" class="form-control">
                                                  <option>Menikah</option>
                                                  <option>Belum Menikah</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jenis Kelamin</label>
                                                <select type="text" name="jk" class="form-control">
                                                  <option>Laki-Laki</option>
                                                  <option>Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Pendidikan Terakhir</label>
                                                <input type="text" class="form-control" name="pendidikan" placeholder="Pendidikan Terakhir" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Ibu Kandung</label>
                                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu Kandung" required="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Kerabat Yang Bisa Dihubungi</label>
                                                <input type="text" class="form-control" name="nama_kerabat" placeholder="Nama Kerabat" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Kerabat</label>
                                                <input type="text" class="form-control" name="hp_kerabat" placeholder="Hp/Telp Kerabat" required="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nominal Yang Diajukan</label><br>
                                                <input type="text" class="form-control" name="nominal" placeholder="RP " required=""> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Keperluan</label><br>
                                                <input type="text" class="form-control" name="keperluan" placeholder="Keperluan Pinjaman " required=""> 
                                            </div>
                                        </div>

                                    </div>
                                    <div class="box-footer">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>