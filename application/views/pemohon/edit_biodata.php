<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        
                         <center><h3>Biodata Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                            
                                <?php foreach ($biodata as $key => $us) : ?>

                              <form method="post" action="<?php echo base_url('pemohon/data_biodata/tambah_bio');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $us->nama_lengkap?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No KTP</label>
                                                <input type="text" class="form-control" name="no_ktp" placeholder="No KTP" value="<?php echo $us->no_ktp?>" readonly >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai KTP</label><br>
                                            <textarea name="alamat_ktp" style="width:100%;padding:10px;" placeholder="Alamat KTP" rows="2" cols="80" readonly=""><?php echo $us->alamat_ktp?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai Domisili</label><br>
                                            <textarea name="alamat_domisili" style="width:100%;padding:10px;" placeholder="Alamat Domisili" rows="2" cols="80" readonly=""><?php echo $us->alamat_domisili; ?></textarea>
                                        </div>
                                        <div class="row"> 
                                            <div class="form-group col-md-6">
                                                <label>No HP/Telp</label><br>
                                                <input type="text" class="form-control" name="no_hp" placeholder="No HP/Telp" value="<?php echo $us->no_hp?>" readonly> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Status Rumah</label><br>
                                                    <input type="text" class="form-control" name="no_hp" placeholder="No HP/Telp" value="<?php echo $us->status_rumah?>" readonly> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $us->tempat_lahir?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal Lahir</label>
                                                <input type="text" class="form-control" name="tgl_lahir" value="<?php echo $us->tgl_lahir?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status</label>
                                                <input type="text" class="form-control" name="status" value="<?php echo $us->status?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jenis Kelamin</label>
                                                <input type="text" class="form-control" name="jk" value="<?php echo $us->jk?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Pendidikan Terakhir</label>
                                                <input type="text" class="form-control" name="pendidikan" placeholder="Pendidikan Terakhir" value="<?php echo $us->pendidikan?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Ibu Kandung</label>
                                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu Kandung" value="<?php echo $us->nama_ibu?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Kerabat Yang Bisa Dihubungi</label>
                                                <input type="text" class="form-control" name="nama_kerabat" placeholder="Nama Kerabat" value="<?php echo $us->nama_kerabat?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Kerabat</label>
                                                <input type="text" class="form-control" name="hp_kerabat" placeholder="Hp/Telp Kerabat" value="<?php echo $us->hp_kerabat?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nominal Yang Diajukan</label><br>
                                                <input type="text" class="form-control" name="nominal" placeholder="RP " value="<?php echo $us->nominal?>" required=""> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Keperluan</label><br>
                                                <input type="text" class="form-control" name="keperluan" placeholder="Keperluan Pinjaman" value="<?php echo $us->keperluan ?>" readonly> 
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                <?php endforeach;?>
                            </div>
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>