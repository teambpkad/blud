<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h3>Data Usaha Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                                <form method="post" action="<?php echo base_url('pemohon/data_file/tambah_file');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto KTP Suami</label>
                                            <input type="file" class="form-control" name="ktp_suami" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto KTP Istri</label>
                                            <input type="file" class="form-control" name="ktp_istri" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto KK (Kartu Keluarga)</label>
                                            <input type="file" class="form-control" name="kk" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto Surat Keterangan Memiliki Usaha dari Kelurahan Setempat / Izin Usaha dari Kecamatan </label>
                                            <input type="file" class="form-control" name="iumk" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto Buku Nikah </label>
                                            <input type="file" class="form-control" name="buku_nikah" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Pas Foto Suami Terbaru</label>
                                            <input type="file" class="form-control" name="foto_suami" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Pas Foto Istri Terbaru </label>
                                            <input type="file" class="form-control" name="foto_istri" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto Usaha</label>
                                            <input type="file" class="form-control" name="foto_usaha" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword5">Foto Sertifikat Jaminan</label>
                                            <input type="file" class="form-control" name="ser_jaminan" required="">
                                            <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                              JPG|JPEG|PNG Ukuran 200KB
                                            </small>
                                        </div>
                                    </div>
                                        <div class="box-footer">
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                            </div>
                                        </div>  
                                </form>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>