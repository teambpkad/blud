<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="">
<!--PAGE NEXT-->  <div class="">
                    <div class="">
                        <center><h3>Laporan Laba/Rugi Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                                <form method="post" action="<?php echo base_url('pemohon/data_jaminan/tambah_jaminan');?>" enctype="multipart/form-data">
                                  <?php foreach ($lab1 as $key => $us) : ?>
                                  
                                  <?php function rupiah($angka){
                                    $hasil_rupiah = "" . number_format($angka,0,',','.');
                                    return $hasil_rupiah;
                                  }?>

                                  <div class="table-responsive">
                                    <table class="table table-bordered">
                                      <thead>
                                        <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        <tr>
                                          <th scope="col">NO</th>
                                          <th scope="col">URAIAN</th>
                                          <th>BULAN<span style="margin:5px "></span>
                                            <input type="" name="" value="<?php echo $us->bulan1?>" readonly>
                                          </th>
                                          <th>BULAN<span style="margin:5px "></span>
                                            <input type="" name="" value="<?php echo $us->bulan2?>" readonly>
                                          </th>
                                          <th>BULAN<span style="margin:5px "></span>
                                             <input type="" name="" value="<?php echo $us->bulan3?>" readonly>
                                          </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th rowspan="4">1</th>
                                          <td><strong><u>PENDAPATAN</u></strong> </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENDAPATAN USAHA</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha1" style="width: 80%" value="<?php echo rupiah($us->pendusaha1)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha2" style="width: 80%" value="<?php echo rupiah($us->pendusaha2)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha3" style="width: 80%" value="<?php echo rupiah($us->pendusaha3)?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENDAPATAN LAIN_LAIN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain1" style="width: 80%" value="<?php echo rupiah($us->pendlain1)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain2" style="width: 80%" value="<?php echo rupiah($us->pendlain2)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain3" style="width: 80%" value="<?php echo rupiah($us->pendlain3)?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TOTAL PENDAPATAN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpend1)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpend2)?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpend3) ?>" readonly></td>
                                        </tr>
                                         <tr>
                                          <th rowspan="8">2</th>
                                          <td><strong><u>PENGELUARAN</u></strong> </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PEMBELIAN BAHAN BAKU</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan1" style="width: 80%" value="<?php echo rupiah($us->pengbahan1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan2" style="width: 80%" value="<?php echo rupiah($us->pengbahan2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan3" style="width: 80%" value="<?php echo rupiah($us->pengbahan3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TENAGA KERJA<strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga1" style="width: 80%" value="<?php echo rupiah($us->pengtenaga1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga2" style="width: 80%" value="<?php echo rupiah($us->pengtenaga2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga3" style="width: 80%" value="<?php echo rupiah($us->pengtenaga3) ?>" readonly></td>
                                        <tr>
                                          <td><strong>LISTRIK, AIR, TELP/HP, DLL</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik1" style="width: 80%" value="<?php echo rupiah($us->penglistrik1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik2" style="width: 80%" value="<?php echo rupiah($us->penglistrik2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik3" style="width: 80%" value="<?php echo rupiah($us->penglistrik3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>BIAYA ADMINISTRASI</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm1" style="width: 80%" value="<?php echo rupiah($us->pengadm1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm2" style="width: 80%" value="<?php echo rupiah($us->pengadm2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm3" style="width: 80%" value="<?php echo rupiah($us->pengadm3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TRANSAPORTASI</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran1" style="width: 80%" value="<?php echo rupiah($us->pengtran1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran2" style="width: 80%" value="<?php echo rupiah($us->pengtran2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran3" style="width: 80%" value="<?php echo rupiah($us->pengtran3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENGELUARAN LAIN-LAIN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain1" style="width: 80%" value="<?php echo rupiah($us->penglain1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain2" style="width: 80%" value="<?php echo rupiah($us->penglain2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain3" style="width: 80%" value="<?php echo rupiah($us->penglain3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TOTAL PENGELUARAN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpeng1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpeng2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->totalpeng3) ?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <th rowspan="1">3</th>
                                          <td><strong><u>SURPLUS/DEFISIT (1-2)</u></strong> </td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->def1) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->def2) ?>" readonly></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo rupiah($us->def3) ?>" readonly></td>
                                        </tr>

                                      </tbody>
                                    </table>
                                  </div>
                                </form>
                                <?php endforeach;?>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>