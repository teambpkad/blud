<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <center><h3>Data Pemberi Kuasa Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                          <form method="post" action="<?php echo base_url('pemohon/data_kuasa/tambah_kuasa');?>" enctype="multipart/form-data">
                              <div class="box-body">
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                  </div>
                                  <div class="row">
                                      <div class="form-group col-md-6">
                                          <label>Nama Kuasa</label>
                                          <input type="text" class="form-control" name="nm_kuasa" placeholder="Nama Kuasa" required="">
                                      </div>
                                      <div class="form-group col-md-6">
                                          <label>No KTP/NIK</label>
                                          <input type="text" class="form-control" name="no_ktp" placeholder="No KTP/NIK" required="">
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="form-group col-md-6">
                                          <label>Tempat Lahir</label>
                                          <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" required="">
                                      </div>
                                      <div class="form-group col-md-6">
                                          <label>Tanggal Lahir </label>
                                          <input type="date" class="form-control" name="tgl" required="">
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <label>Alamat Lengkap Kuasa</label><br>
                                      <textarea name="alamat_kuasa" style="width:100%;padding:10px;" placeholder="Alamat Kuasa" rows="2" cols="80" required=""></textarea>
                                  </div>

                                  <div class="row">
                                      <div class="form-group col-md-6">
                                          <label>Nama Saksi Pertama</label>
                                          <input type="text" class="form-control" name="nm_saksi1" placeholder="Nama Saksi Pertama" required="">
                                      </div>
                                      <div class="form-group col-md-6">
                                          <label>Nama Saksi Kedua </label>
                                          <input type="text" class="form-control" name="nm_saksi2" placeholder="Nama Saksi Kedua" required="">
                                      </div>
                                  </div>
                              </div>
                              <div class="box-footer">
                                  <div class="pull-right">
                                      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                  </div>
                              </div>
                          </form>
                      </div>   
                        
                       
                    </div>
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>