<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h3>Data Usaha Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                           <?php foreach ($biodata as $key => $us) : ?>
                                <form method="post" action="<?php echo base_url('auth/update_usaha');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Usaha</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" value="<?php echo $us->nama_usaha?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" value="<?php echo $us->bidang_usaha?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80" readonly="" ><?php echo $us->alamat_usaha;?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Mulai Usaha</label>
                                                <input type="text" class="form-control" name="mulai_usaha" placeholder="Mulai Usaha" value="<?php echo $us->mulai_usaha?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" class="form-control" name="jmlh_karyawan" placeholder="Jumlah Karyawan" value="<?php echo $us->jmlh_karyawan?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" value="<?php echo $us->hp_usaha?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </form>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>