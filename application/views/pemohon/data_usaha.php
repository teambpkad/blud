<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h3>Data Usaha Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                                <form method="post" action="<?php echo base_url('pemohon/data_usaha/tambah_usaha');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Usaha</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80" required=""></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Mulai Usaha</label>
                                                <input type="text" class="form-control" name="mulai_usaha" placeholder="Mulai Usaha" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" class="form-control" name="jmlh_karyawan" placeholder="Jumlah Karyawan" required="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" required="">
                                            </div>
                                        </div>
                                    </div>
                                        <div class="box-footer">
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                            </div>
                                        </div>  
                                </form>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>