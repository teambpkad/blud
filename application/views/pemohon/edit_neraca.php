<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="">
<!--PAGE NEXT-->  <div class="">
                    <div class="">
                      <center><h3>Laporan Neraca</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                          <form method="post" action="<?php echo base_url('pemohon/data_neraca/tambah_neraca');?>" enctype="multipart/form-data">
                            <?php foreach ($biodata as $key => $us) : ?>
                              <?php function rupiah($angka){
                                $hasil_rupiah = "" . number_format($angka,0,',','.');
                                return $hasil_rupiah;
                              }?>
                              
                            <div class="form-group">
                              <center>
                                <h4>
                                  <th>Bulan<span style="margin:5px "></span>
                                    <input type="hidden" value="<?php echo $us->bulan?>" name="" readonly>
                                  </th>
                                </h4>                        
                              </center>
                              <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                            </div>
                            <div class="table-responsive">
                              <table class="table table-bordered">
                                <thead>

                                  <tr>
                                    <th>No</th>
                                    <th>Perkiraan</th>
                                    <th>Jumlah</th>
                                    <th>No</th>
                                    <th>Perkiraan</th>
                                    <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th rowspan="6">A</th>
                                    <td>Aktiva Lancar</td>
                                    <td></td>
                                    <td rowspan="6">C</td>
                                    <td>Hutang</td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <td>1. Kas</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kas" style="width: 80%" value="<?php echo rupiah($us->kas)?>" readonly></td>
                                    <td rowspan="4">1. Hutang Usaha</td>
                                    <td rowspan="4"><span style="margin: 10px">Rp</span><input type="text" name="htgusaha" style="width: 80%" value="<?php echo rupiah($us->htgusaha)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td>2. Bank</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="bank" style="width: 80%" value="<?php echo rupiah($us->bank)?>" readonly></td>
                                  </tr>

                                  <tr>
                                    <td>3. Piutang</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="piutang" style="width: 80%" value="<?php echo rupiah($us->piutang)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td>4. Persediaan</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo rupiah($us->persediaan)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td><center><strong>Jumlah Aktiva Lancar</strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo rupiah($us->jmlhaktiva_lancar)?>" readonly></td>
                                    <td><center><strong>Jumlah Hutang</strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo rupiah($us->jmlhhutang)?>" readonly></td>
                                  </tr>
                                  <tr></tr>
                                  <tr>
                                    <th rowspan="6">B</th>
                                    <td>Aktiva Tetap</td>
                                    <td></td>
                                    <td rowspan="6">D</td>
                                    <td>Modal</td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <td>1. Tanah</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="tanah" style="width: 80%" value="<?php echo rupiah($us->tanah)?>" readonly></td>
                                    <td>1.Modal Usaha</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="mdlusaha" style="width: 80%" value="<?php echo rupiah($us->mdlusaha)?>" readonly></td>
                                  </tr>

                                  <tr>
                                    <td>2. Bangunan</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="bangunan" style="width: 80%" value="<?php echo rupiah($us->bangunan)?>" readonly></td>
                                    <td rowspan="2">2.Laba Bulan Berjalan</td>
                                    <td rowspan="2"><span style="margin: 10px">Rp</span><input type="text" name="laba" style="width: 80%" value="<?php echo rupiah($us->laba)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td>3. Kendaraan</td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo rupiah($us->kendaraan)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td><center><strong>Jumlah Aktiva Tetap</strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo rupiah($us->jmlhaktiva_tetap)?>" readonly></td>
                                    <td><center><strong>Jumlah Modal</strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo rupiah($us->jmljhmodal)?>" readonly></td>
                                  </tr>
                                  <tr>
                                    <td><center><strong>Jumlah Aktiva </strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo rupiah($us->totalaktiva)?>" readonly></td>
                                    <td><center><strong>Jumlah Hutang dan Modal</strong></center></td>
                                    <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo rupiah($us->totalhtg)?>" readonly></td>
                                  </tr>
                                </tbody>
                              </table>
                              <?php endforeach;?>
                          </form>
                      </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>      
</div>