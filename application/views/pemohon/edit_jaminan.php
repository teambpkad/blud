<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h3>Data Jaminan Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                           <?php foreach ($biodata as $key => $us) : ?>
                                <form method="post" action="<?php echo base_url('data_pinjaman/tambah_pinjaman');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="row">
                                          <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                            <div class="form-group col-md-6">
                                                <label>Bentuk/Jenis Jaminan</label>
                                                <input type="text" class="form-control" name="bentuk_jaminan" placeholder="Bentuk Jaminan" value="<?php echo $us->bentuk_jaminan?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No Jaminan/Sertifikat</label>
                                                <input type="text" class="form-control" name="bukti_jaminan" placeholder="Bukti Jaminan" value="<?php echo $us->bukti_jaminan?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Pemilik Jaminan</label>
                                                <input type="text" class="form-control" name="nama_pemilik" placeholder="Nama Pemilik" value="<?php echo $us->nama_pemilik?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal Jaminan</label>
                                                <input type="text" class="form-control" name="tgl_jaminan" placeholder="Tanggal Jaminan" value="<?php echo $us->tgl_jaminan?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Jaminan</label><br>
                                            <textarea name="alamat_jaminan" style="width:100%;padding:10px;" placeholder="Alamat Jaminan" rows="2" cols="80" readonly=""><?php echo $us->alamat_jaminan; ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status Jaminan</label>
                                                <input type="text" class="form-control" name="nilai_jaminan" placeholder="Nilai Jaminan" value="<?php echo $us->status_jaminan?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nilai Jaminan (Harga)</label>
                                                <input type="text" class="form-control" name="nilai_jaminan" placeholder="Nilai Jaminan" value="<?php echo $us->nilai_jaminan?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form>
                                <?php endforeach;?>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>