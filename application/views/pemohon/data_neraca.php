<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="">
<!--PAGE NEXT-->  <div class="">
                    <div class="">
                        <center><h3>Laporan Neraca</h3></center>
                        
                        <div class="active tab-content" id="pills-tabContent">
                                <form method="post" action="<?php echo base_url('pemohon/data_neraca/tambah_neraca');?>" enctype="multipart/form-data">
                                  <div class="form-group">
                                    <center>
                                      <h4>
                                        <th>Bulan<span style="margin:5px "></span>
                                          <select name="bulan">
                                            <option value="Januari"> Januari </option>
                                            <option value="Februari"> Februari </option>
                                            <option value="Maret"> Maret </option>
                                            <option value="April"> April </option>
                                            <option value="Mei"> Mei </option>
                                            <option value="Juni"> Juni </option>
                                            <option value="Juli"> Juli </option>
                                            <option value="Agustus"> Agustus </option>
                                            <option value="September"> September </option>
                                            <option value="November"> November </option>
                                            <option value="Oktober"> Oktober </option>
                                            <option value="Desember"> Desember </option>
                                          </select>
                                        </th>
                                      </h4>                        
                                    </center>
                                    <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                  </div>
                                  <div class="table-responsive">
                                    <table class="table table-bordered">
                                      <thead>

                                        <tr>
                                          <th>No</th>
                                          <th>Perkiraan</th>
                                          <th>Jumlah</th>
                                          <th>No</th>
                                          <th>Perkiraan</th>
                                          <th>Jumlah</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th rowspan="5">A</th>
                                          <td>Aktiva Lancar</td>
                                          <td></td>
                                          <td rowspan="5">C</td>
                                          <td>Hutang</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>1. Kas</td>
                                          <td><span style="margin: 10px">Rp</span><input id="kas1" type="text" name="kas" style="width: 80%"></td>
                                          <td rowspan="4">1. Hutang Usaha</td>
                                          <td rowspan="4"><span style="margin: 10px">Rp</span><input id="htgusaha1" type="text" name="htgusaha" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <td>2. Bank</td>
                                          <td><span style="margin: 10px">Rp</span><input id="bank1" type="text" name="bank" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <td>3. Piutang</td>
                                          <td><span style="margin: 10px">Rp</span><input id="piutang1" type="text" name="piutang" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <td>4. Persediaan</td>
                                          <td><span style="margin: 10px">Rp</span><input id="persediaan1" type="text" name="persediaan" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <th rowspan="5">B</th>
                                          <td>Aktiva Tetap</td>
                                          <td></td>
                                          <td rowspan="5">D</td>
                                          <td>Modal</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>1. Tanah</td>
                                          <td><span style="margin: 10px">Rp</span><input id="tanah1" type="text" name="tanah" style="width: 80%"></td>
                                          <td>1.Modal Usaha</td>
                                          <td><span style="margin: 10px">Rp</span><input id="mdlusaha1" type="text" name="mdlusaha" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <td>2. Bangunan</td>
                                          <td><span style="margin: 10px">Rp</span><input id="bangunan1" type="text" name="bangunan" style="width: 80%"></td>
                                          <td rowspan="2">2.Laba Bulan Berjalan</td>
                                          <td rowspan="2"><span style="margin: 10px">Rp</span><input id="laba1" type="text" name="laba" style="width: 80%"></td>
                                        </tr>
                                        <tr>
                                          <td>3. Kendaraan</td>
                                          <td><span style="margin: 10px">Rp</span><input id="kendaraan" type="text" name="kendaraan" style="width: 80%"></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  <div class="pull-right">
                                      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                  </div>
                                    
                                </form>
                            </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>      
</div>

<script type="text/javascript">
  var kas1 = document.getElementById("kas1");
  kas1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    kas1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      kas1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      kas1 += separator + ribuan.join(".");
    }

    kas1 = split[1] != undefined ? kas1 + "," + split[1] : kas1;
    return prefix == undefined ? kas1 : kas1 ? kas1 : "";
  }

  var htgusaha1 = document.getElementById("htgusaha1");
  htgusaha1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    htgusaha1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      htgusaha1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      htgusaha1 += separator + ribuan.join(".");
    }

    htgusaha1 = split[1] != undefined ? htgusaha1 + "," + split[1] : htgusaha1;
    return prefix == undefined ? htgusaha1 : htgusaha1 ? htgusaha1 : "";
  }

  var bank1 = document.getElementById("bank1");
  bank1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    bank1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      bank1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      bank1 += separator + ribuan.join(".");
    }

    bank1 = split[1] != undefined ? bank1 + "," + split[1] : bank1;
    return prefix == undefined ? bank1 : bank1 ? bank1 : "";
  }

  var piutang1 = document.getElementById("piutang1");
  piutang1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    piutang1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      piutang1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      piutang1 += separator + ribuan.join(".");
    }

    piutang1 = split[1] != undefined ? piutang1 + "," + split[1] : piutang1;
    return prefix == undefined ? piutang1 : piutang1 ? piutang1 : "";
  }

  var persediaan1 = document.getElementById("persediaan1");
  persediaan1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    persediaan1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      persediaan1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      persediaan1 += separator + ribuan.join(".");
    }

    persediaan1 = split[1] != undefined ? persediaan1 + "," + split[1] : persediaan1;
    return prefix == undefined ? persediaan1 : persediaan1 ? persediaan1 : "";
  }

  var tanah1 = document.getElementById("tanah1");
  tanah1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    tanah1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      tanah1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      tanah1 += separator + ribuan.join(".");
    }

    tanah1 = split[1] != undefined ? tanah1 + "," + split[1] : tanah1;
    return prefix == undefined ? tanah1 : tanah1 ? tanah1 : "";
  }

  var mdlusaha1 = document.getElementById("mdlusaha1");
  mdlusaha1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    mdlusaha1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      mdlusaha1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      mdlusaha1 += separator + ribuan.join(".");
    }

    mdlusaha1 = split[1] != undefined ? mdlusaha1 + "," + split[1] : mdlusaha1;
    return prefix == undefined ? mdlusaha1 : mdlusaha1 ? mdlusaha1 : "";
  }

  var bangunan1 = document.getElementById("bangunan1");
  bangunan1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    bangunan1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      bangunan1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      bangunan1 += separator + ribuan.join(".");
    }

    bangunan1 = split[1] != undefined ? bangunan1 + "," + split[1] : bangunan1;
    return prefix == undefined ? bangunan1 : bangunan1 ? bangunan1 : "";
  }

  var laba1 = document.getElementById("laba1");
  laba1.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    laba1.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      laba1 = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      laba1 += separator + ribuan.join(".");
    }

    laba1 = split[1] != undefined ? laba1 + "," + split[1] : laba1;
    return prefix == undefined ? laba1 : laba1 ? laba1 : "";
  }

  var kendaraan = document.getElementById("kendaraan");
  kendaraan.addEventListener("keyup", function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    kendaraan.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      kendaraan = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      kendaraan += separator + ribuan.join(".");
    }

    kendaraan = split[1] != undefined ? kendaraan + "," + split[1] : kendaraan;
    return prefix == undefined ? kendaraan : kendaraan ? kendaraan : "";
  }
</script>