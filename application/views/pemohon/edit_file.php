
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        <center><h1>Data Usaha Pemohon</h1></center>
                        <div class="active tab-content" id="pills-tabContent">
                           <?php foreach ($biodata as $key => $us) : ?>
                          <form method="post" action="<?php echo base_url('auth/update_usaha');?>" enctype="multipart/form-data">
                            <div></div>
                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                            <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
                            <style>
                                body {font-family: Arial, Helvetica, sans-serif;}

                              #myImg {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg:hover {opacity: 0.7;}

                              #myImg1 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg1:hover {opacity: 0.7;}

                              #myImg2 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg2:hover {opacity: 0.7;}

                              #myImg3 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg3:hover {opacity: 0.7;}

                              #myImg4 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg4:hover {opacity: 0.7;}

                              #myImg5 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg5:hover {opacity: 0.7;}

                              #myImg6 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg6:hover {opacity: 0.7;}


                              /* The Modal (background) */
                              .modal {
                                display: none; /* Hidden by default */
                                position: fixed; /* Stay in place */
                                z-index: 1; /* Sit on top */
                                padding-top: 100px; /* Location of the box */
                                left: 0;
                                top: 0;
                                width: 100%; /* Full width */
                                height: 100%; /* Full height */
                                overflow: auto; /* Enable scroll if needed */
                                background-color: rgb(0,0,0); /* Fallback color */
                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                              }

                              /* Modal Content (image) */
                              .modal-content {
                                margin: auto;
                                display: block;
                                width: 80%;
                                max-width: 700px;
                              }

                              /* Add Animation */
                              .modal-content, #caption {  
                                -webkit-animation-name: zoom;
                                -webkit-animation-duration: 0.6s;
                                animation-name: zoom;
                                animation-duration: 0.6s;
                              }

                              @-webkit-keyframes zoom {
                                from {-webkit-transform:scale(0)} 
                                to {-webkit-transform:scale(1)}
                              }

                              @keyframes zoom {
                                from {transform:scale(0)} 
                                to {transform:scale(1)}
                              }

                              

                              /* 100% Image Width on Smaller Screens */
                              @media only screen and (max-width: 700px){
                                .modal-content {
                                  width: 100%;
                                }
                              }
                              </style>
                              <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center">
                                    <center><label><h1>KTP Suami</h1></label></center>
                                    <center><img data-u="image" id="myImg" class="img-fluid" style="width:50%; height:50%" src="<?php echo base_url().'uploads/file/'.$us->ktp_suami;?>" alt="KTP Suami"></center>
                                  </div>
                                  
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>KTP Istri</h1></label></center>
                                    <center><img data-u="image" id="myImg1" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->ktp_istri;?>" alt="KTP Istri"></center>
                                  </div>
                                </div>
                                  <hr>
                                <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center">
                                    <center><label><h1>Kartu Keluarga</h1></label></center>
                                    <center><img data-u="image" id="myImg2" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->kk;?>"></center>
                                  </div>
                                  
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>IUMK</h1></label></center>
                                    <center><img data-u="image" id="myImg3" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->iumk;?>"></center>
                                  </div>
                                  
                                </div>
                                <hr>
                                <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Suami</h1></label></center>
                                    <center><img class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_suami;?>"></center>
                                  </div>
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Istri</h1></label></center>
                                    <center><img class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_istri;?>"></center>
                                  </div>                               
                                  
                                  </div>
                                  <hr>
                                  <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Buku Nikah</h1></label></center>
                                    <center><img data-u="image" id="myImg4" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->buku_nikah;?>"></center>
                                  </div>
                                 
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Usaha</h1></label></center>
                                    <center><img data-u="image" id="myImg5" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_usaha;?>"></center>
                                  </div>
                                </div>
                                  <hr>
                               
                                  <div data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Sertifikat Jaminan</h1></label></center>
                                    <center><img data-u="image" id="myImg6" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->ser_jaminan;?>"></center>
                                  </div>
                                  <hr>
                              </div>
                          </form>
                          <?php endforeach;?>


                          <!-- The Modal KTP SUAMI -->
                          <div id="myModal" class="modal">
     
                            <img class="modal-content" id="img01">
                          
                          </div>

                          <!-- The Modal KTP ISTRI -->
                          <div id="myModal1" class="modal">
                            <img class="modal-content" id="img02">
                          </div>

                          <!-- The Modal KK -->
                          <div id="myModal2" class="modal">
                            <img class="modal-content" id="img03">
                          </div>

                           <!-- The Modal IUMK -->
                          <div id="myModal3" class="modal">
                            <img class="modal-content" id="img04">
                          </div>

                          <!-- The Modal BUKU NIKAH -->
                          <div id="myModal4" class="modal">
                            <img class="modal-content" id="img05">
                          </div>

                          <!-- The Modal FOTO USAHA -->
                          <div id="myModal5" class="modal">
                            <img class="modal-content" id="img06">
                          </div>

                          <!-- The Modal SERTIFIKAT JAMINAN -->
                          <div id="myModal6" class="modal">
                            <img class="modal-content" id="img07">
                          </div>

                          <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
                          <script>
                            AOS.init();
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg");
                          var modalImg = document.getElementById("img01");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // Get the <span> element that closes the modal
                          var span = document.getElementsByClassName("close")[0];

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal1");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg1");
                          var modalImg = document.getElementById("img02");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal2");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg2");
                          var modalImg = document.getElementById("img03");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal3");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg3");
                          var modalImg = document.getElementById("img04");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal4");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg4");
                          var modalImg = document.getElementById("img05");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal5");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg5");
                          var modalImg = document.getElementById("img06");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal6");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg6");
                          var modalImg = document.getElementById("img07");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>


                      </div>   
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>



