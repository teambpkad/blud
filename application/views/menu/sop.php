<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>BLUD</title>
	<!-- Latest compiled and minified CSS -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<!--Font Awesome Bootsrap 4 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--Fonts Google-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
	
	

 
	<style type="text/css">
	body{
		background-image: url(<?php echo base_url('assets/images/bg/bgBlud.png') ?>);
		background-repeat: no-repeat, repeat;
		background-size:cover;
	}

	.circle-menu-box {
	width:700px;
	height: 700px;
	position: relative;
	
}
	.circle-menu-box a.menu-item {
		display: block;
		text-decoration: none;
		/*border-radius: 100%;*/
		/*margin:20px;*/
		/*text-align: center;*/
		width:150px;
		height:150px;
		/*background-color:#fff;*/
		/*color:#777;
		padding:27px;*/
		position: absolute;
		/*font-size: 27px;*/

		transition:all 0.5s;
		-moz-transition:all 0.5s;
		-webkit-transition:all 0.5s;
		-o-transition:all 0.5s;
	}

	.circle-menu-box a.menu-item:hover {
		transform:scale(1.5);
		-webkit-transform:scale(1.5);
		-moz-transform:scale(1.5);
		-o-transform:scale(1.5);
		/*color:#fff;
		background: #ff3333;*/
	}

	.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  color: white;
		  text-align: center;
		  font-family: 'Rajdhani', sans-serif;
		}

.modal.fade .modal-dialog {
 -webkit-transform: translate(0);
 -moz-transform: translate(0);
 transform: translate(0);
 }
	</style>
</head>
<body>

<div class="bounceInDown animated container" style="margin-top: 50px">
<div class="card bg-light text-dark">
        <div class="card-body">
           <a href="#" data-toggle="modal" class="color-gray-darker c6 td-hover-none">
               
                  <center><img style="max-width: 100%; " class="img-fluid" src="<?php echo base_url()?>assets/images/SOP.jpg"></center>
                 
            </a>
        </div>
    </div>
</div>



<a data-toggle="tooltip" title="Kembali KeHalaman Utama" data-placement="bottom" id="example" href="<?php echo base_url('') ?>" class="back-to-top fadeIn animated"><i style="font-size:48px;color:orange; margin-right:35px;float: right;" class="fa fa-arrow-left"></i></a>

  <script type="text/javascript">
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<!--Scroll Top -->

<footer class="footer">
	<center>Copyright © 2019 | Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam</center>
</footer>

<!-- mobile-->



</body>
</html>