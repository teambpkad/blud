<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>BLUD</title>
	<!-- Latest compiled and minified CSS -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<!--Font Awesome Bootsrap 4 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--Fonts Google-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
	
	

 
	<style type="text/css">
	body{
		background-image: url(<?php echo base_url('assets/images/bg/bgBlud.png') ?>);
		background-repeat: no-repeat, repeat;
		background-size:cover;
	}

	.circle-menu-box {
	width:700px;
	height: 700px;
	position: relative;
	
}
	.circle-menu-box a.menu-item {
		display: block;
		text-decoration: none;
		/*border-radius: 100%;*/
		/*margin:20px;*/
		/*text-align: center;*/
		width:150px;
		height:150px;
		/*background-color:#fff;*/
		/*color:#777;
		padding:27px;*/
		position: absolute;
		/*font-size: 27px;*/

		transition:all 0.5s;
		-moz-transition:all 0.5s;
		-webkit-transition:all 0.5s;
		-o-transition:all 0.5s;
	}

	.circle-menu-box a.menu-item:hover {
		transform:scale(1.5);
		-webkit-transform:scale(1.5);
		-moz-transform:scale(1.5);
		-o-transform:scale(1.5);
		/*color:#fff;
		background: #ff3333;*/
	}

	.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  color: white;
		  text-align: center;
		  font-family: 'Rajdhani', sans-serif;
		}

.modal.fade .modal-dialog {
 -webkit-transform: translate(0);
 -moz-transform: translate(0);
 transform: translate(0);
 }

 .bgtab
 {
  background-image: url(<?php echo base_url('assets/images/bg/watermarkblud.png') ?>)
 }
	</style>
</head>
<body>


<div class="container fadeIn animated" style="margin-top: 50px ">
<div class="card bg-light text-dark" style="font-family: 'Roboto'">

	<div class="card body">
 
  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home" style="font-family: 'Roboto Condensed'">KOPERASI</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1" style="font-family: 'Roboto Condensed'">USAHA MIKRO</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class="container tab-pane active bgtab"><br>
      <h3 style="font-family: 'Russo One';color: orange">KRITERIA KOPERASI</h3>
      <p><h3 style="font-family: Roboto Condensed">Koperasi :</h3>
            <ol type="a" style="font-family: 'Khand'">

            <li>Mempunyai akta pendirian/perubahan anggaran dasar sesuai dengan ketentuan yang berlaku; </li>
            <li>Telah memiliki badan hukum yang sudah disahkan oleh Pemerintah; </li>
            <li> Telah melaksanakan rapat anggota tahunan (RAT) tahun buku terakhir dilengkapi dengan Neraca dan perhitungan hasil usaha koperasi dan susunan pengurus/pengawas;</li>
            <li>Untuk unit usaha simpan pinjam wajib memiliki modal sendiri minimal <b>Rp. 15.000.000,- (lima belas juta rupiah)</b> dan dikelola secara terpisah yang dibuktikan dalam neraca tahun buku berjalan; dan</li>
            <li>Diutamakan bagi koperasi yang memiliki peringkat minimal cukup sehat;</li>
        </ol>
            <br>
            <h3 style="font-family: 'Roboto Condensed'">Persyaratan Koperasi :</h3>
            <ol type="a" style="font-family: 'Khand'">
            <li>Pemohon mengajukan proposal dan mengisi formulir;</li>
            <li>Melampirkan foto copy Akta Pendirian/akta Perubahan anggaran dasar koperasi;</li>
            <li>Melampirkan foto copy SK pengesahan badan hukum/SK perubahan anggaran dasar koperasi yang dibuat sesuai dengan ketentuan yang berlaku;</li>
            <li>Melampirkan laporan rapat anggota tahunan (RAT) tahun buku terakhir;</li>
            <li>Melampirkan laporan keuangan (Neraca dan Rugi Laba);</li>
            <li>Melampirkan foto copy tanda daftar umum koperasi (TDUK) dan nomor pokok wajib pajak (NPWP);</li>
            <li>Selain koperasi simpan pinjam wajib melampirkan foto copy surat izin usaha perdagangan (SIUP);</li>
            <li>Melampirkan foto copy rekening Bank yang ditunjuk;</li>
            <li>Melampirkan foto copy KTP Pengurus yang masih berlaku;</li>
            <li>Melampirkan foto copy Surat Keterangan domisili usaha dari Instansi yang berwenang;</li>
            <li>Pas foto terbaru Pengurus masing-masing ukuran 3x4 cm sebanyak 2 lembar;</li>
            <li>Melampirkan rencana penyaluran atau penggunaan dana pinjaman;</li>
            <li>Melampirkan pernyataan keputusan rapat (PKR) yang dibuatkan oleh notaris pembuatan akta koperasi yang menyatakan bahwa   rapat anggota menyetujui untuk pengajuan pinjam dana bergulir; dan</li>
            <li>Melampirkan foto copy dokumen jaminan;</li></p>
    </div>
    <div id="menu1" class="container tab-pane fade bgtab"><br>
      <h3 style="font-family: 'Russo One';color: orange">USAHA MIKRO</h3>
      <p><h3 style="font-family: 'Roboto Condensed'">Kriteria Usaha Mikro :</h3>
        <ol type="a" style="font-family: 'Khand'">
            <li>Penduduk kota Batam.</li>
            <li>Berusia minimal 17 (tujuh belas) tahun dan maksimal 60 (enam puluh) tahun.</li>
            <li>Tidak berprofesi sebagai Pegawai Negri Sipil (PNS) aktif, Tentara Nasional Indonesia (TNI) aktif, Polisi Republik Indonesia (POLRI) aktif, Anggota DPRD aktif dan Suami atau Isteri Anggota DPRD aktif.</li>
            <li>Memiliki usaha produktif dan jasa yang layak dikembangkan;</li>
            <li>Telah mengajukan usaha minimal 6 (enam) bulan; </li>
            <li>Bertempat tinggal dipemukiman resmi; dan</li>
            <li>Untuk usaha mikro memiliki aset sampai dengan <b>Rp.50.000.000,- (lima puluh juta rupiah)</b> tidak termasuk tanah dan bangunan serta memiliki omset sampai dengan <b>Rp.300.000.000,- (tiga ratus juta rupiah)</b> pertahun, yang dibuktikan dengan neraca dan rincian rugi laba;</li>
        </ol>
            <br>
            <h3 style="font-family: 'Roboto Condensed'">Persyaratan Usaha Mikro :</h3>
        <ol type="a" style="font-family: 'Khand'">
            <li>Pemohon mengajukan proposal dan mengisi formulir;</li>
            <li>Melampirkan foto copy KTP suami dan isteri yang masih berlaku dan Kartu Keluarga (KK);</li>
            <li>Melampirkan surat keterangan domisili dari kelurahan setempat;</li>
            <li>foto copy surat izin usaha mikro dari kecamatan (IUMK);</li>
            <li>Pas foto terbaru suami dan isteri ukuran 3x4 cm sebanyak 2 (dua) lembar;</li>
            <li>Melampirkan foto copy surat nikah bagi yang sudah menikah;</li>
            <li>Melampirkan surat persetujuan suami/isteri;</li>
            <li>Melampirkan Pencatatan total penerima dan pengeluaran usaha 3 (tiga) bulan terakhir;</li>
            <li>Melampirkan foto usaha;</li>
            <li>Melampirkan foto copy dokumen jaminan;</li>
            <li>Melampirkan foto jaminan; dan</li>
            <li>Melampirkan foto copy rekening Bank yang ditunjuk;</li>
        </ol></p>
    </div>
  </div>
</div>
</div>
</div>


<a data-toggle="tooltip" title="Kembali KeHalaman Utama" data-placement="bottom" id="example" href="<?php echo base_url('') ?>" class="back-to-top fadeIn animated"><i style="font-size:48px;color:orange; margin-right:35px;float: right;" class="fa fa-arrow-left"></i></a>

  <script type="text/javascript">
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<!--Scroll Top -->

<footer class="footer">
	<center>Copyright © 2019 | Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam</center>
</footer>

<!-- mobile-->



</body>
</html>