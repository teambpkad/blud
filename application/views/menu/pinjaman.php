<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>BLUD</title>
	<!-- Latest compiled and minified CSS -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<!--Font Awesome Bootsrap 4 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--Fonts Google-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
	
	

 
	<style type="text/css">
	body{
		background-image: url(<?php echo base_url('assets/images/bg/bgBlud.png') ?>);
		background-repeat: no-repeat, repeat;
		background-size:cover;
	}

	.circle-menu-box {
	width:700px;
	height: 700px;
	position: relative;
	
}
	.circle-menu-box a.menu-item {
		display: block;
		text-decoration: none;
		/*border-radius: 100%;*/
		/*margin:20px;*/
		/*text-align: center;*/
		width:150px;
		height:150px;
		/*background-color:#fff;*/
		/*color:#777;
		padding:27px;*/
		position: absolute;
		/*font-size: 27px;*/

		transition:all 0.5s;
		-moz-transition:all 0.5s;
		-webkit-transition:all 0.5s;
		-o-transition:all 0.5s;
	}

	.circle-menu-box a.menu-item:hover {
		transform:scale(1.5);
		-webkit-transform:scale(1.5);
		-moz-transform:scale(1.5);
		-o-transform:scale(1.5);
		/*color:#fff;
		background: #ff3333;*/
	}

	.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  color: white;
		  text-align: center;
		  font-family: 'Rajdhani', sans-serif;
		}

.modal.fade .modal-dialog {
 -webkit-transform: translate(0);
 -moz-transform: translate(0);
 transform: translate(0);
 }

  .bgtab
 {
  background-image: url(<?php echo base_url('assets/images/bg/watermarkblud.png') ?>)
 }
	</style>
</head>
<body>


<div class="container fadeIn animated" style="margin-top: 50px ">
<div class="card bg-light text-dark" style="font-family: 'Roboto'">

	<div class="card body">
 
  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home" style="font-family: 'Roboto Condensed'">BESARAN JASA PINJAMAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1" style="font-family: 'Roboto Condensed'">BESARAN PINJAMAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2" style="font-family: 'Roboto Condensed'">JAMINAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu3" style="font-family: 'Roboto Condensed'">WAKTU PINJAMAN</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class="container tab-pane active bgtab"><br>
      <h3 style="font-family: 'Roboto Condensed'">BESARAN JASA PINJAMAN</h3>
      <p>
            <ol type="1" style="font-family: 'Khand'">
            <li>Penerima pinjaman dikenakan bunga/jasa pinjaman sebesar 6% (enam persen) flat pertahun;</li>
            <li>Pada saat pencairan pinjaman, penerima pinjaman wajib membayar angsuran pertama pokok dan bunga/jasa pinjaman.</li>
            <li>Penerima pinjaman wajib membayar angsuran pinjaman ke Bank yang ditunjuk menggunakan slip pembayaran pokok dan slip penyetoran bunga/jasa dibuat secara terpisah, dengan rincian sebagai berikut : 
                <ol type="a">
                <li style="margin-left: -24px">Setoran pokok nomer rekening : <b>106.02.02000</b> atas nama penerima pokok UPT dana bergulir dan</li>
                <li style="margin-left: -24px">Setoran bunga/jasa nomor rekening : <b>106.02.04000</b> atas nama penerima bunga/jasa UPT dana bergulir.</li>
            </li>
            <li style="margin-left: -24px">Pengembalian pokok pinjaman dan bunga/jasa dilakukan sesuai dengan perjanjian pinjaman.</li>
            <li style="margin-left: -24px">Penerima pinjaman yang melakukan pelunasan dipercepat wajib membayar sisa pokok dan bunga/jasa pinjaman pada bulan berjalan.</li></ol></p>
    </div>
    <div id="menu1" class="container tab-pane fade bgtab"><br>
      <h3 style="font-family: 'Roboto Condensed'">BESARAN PINJAMAN YANG DIBERIKAN ADALAH:</h3>
      <p>
          <ol type="a" style="font-family: 'Khand'">
          <li style="margin-left: -24px">Koperasi maksimal <b>Rp.300.000.000,-</b> (Tiga Ratus Juta Rupiah)</li>
          <li style="margin-left: -24px">Usaha Mikro maksimal <b>Rp.100.000.000,-</b> (Seratus Ratus Juta Rupiah)</li>
          </p>
          </ol></p>
    </div>

    <div id="menu2" class="container tab-pane fade bgtab"><br>
      <h3 style="font-family: 'Roboto Condensed'">JAMINAN</h3>
      <p>
          <ol type="1" style="font-family: 'Khand'">
          <li>Setiap pemohon yang mengajukan pinjaman dana bergulir wajib menyertakan jaminan atas nama pemohon dan atau suami atau isteri pemohon.</li>
          <li>Jaminan atas nama suami atau isteri pemohon, wajib melampirkan surat kuasa dan surat persetujuan dari pemilik jaminan dengan materai dan disaksikan sekurang-kurangnya 2 (dua) orang.</li>
          <li>Jaminan yang diberikan pemohon harus berada di wilayah Kota Batam.</li>
          <li>Melampirkan Akta Kuasa Menjual dari pemohon/Pemilik Jaminan yang dibuat dihadapan Notaris.</li>
          <li>Jaminan yang diberikan oleh pemohon dapat berupa :</li>
            <ol type="a">
            <li style="margin-left: -24px">Tanah/bangunan (Sertifikat Kepemilikan/Surat Kepemilikan yang sah dikeluarkan oleh pihak yang berwenang), melampirkan Foto copy bukti pembayaran pajak bumi dan bangunan (PBB) tahun terakhir.</li>
          </ol>
          <li>Jaminan asli diserahkan pada saat penandatanganan akad kredit pinjaman.</li>
          <li>UPT-PDB wajib menyimpan dan memeilihara jaminan asli sehingga tetap dalam keadaan baik dan terawat.</li>
          <li>Jaminan asli dikembalikan kepada penerima pinjaman setelah pokok, bunga/jasa dan denda pinjaman dibayarkan lunas.</li>
          </ol></p>
    </div>

    <div id="menu3" class="container tab-pane fade bgtab"><br>
      <h3 style="font-family: 'Roboto Condensed'">WAKTU PINJAMAN YANG DIBERIKAN ADALAH:</h3>
      <p>
          <ol type="a" style="font-family: 'Khand'">
          <li>Jangka waktu pinjaman dana bergulir maksimal selama 36 (tiga puluh enam) bulan, terhitung sejak tanggak penandatanganan perjanjian pinjaman.</li>
          <li>Perubahan jangka waktu pengembalian pinjaman dituangkan dalam perubahan perjanjian pinjaman.</li>
          <li>Pinjaman dana bergulir oleh penerima pinjaman digunakan untuk modal kerja sebagaimana proposal pengajuan pinjaman.</li>
      </ol></p>
    </div>
  </div>
</div>
</div>
</div>



<a data-toggle="tooltip" title="Kembali KeHalaman Utama" data-placement="bottom" id="example" href="<?php echo base_url('') ?>" class="back-to-top fadeIn animated"><i style="font-size:48px;color:orange; margin-right:35px;float: right;" class="fa fa-arrow-left"></i></a>

  <script type="text/javascript">
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<!--Scroll Top -->

<footer class="footer">
	<center>Copyright © 2019 | Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam</center>
</footer>

<!-- mobile-->



</body>
</html>