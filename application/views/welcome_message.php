<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>BLUD</title>
	<!--Fonts Google-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani|Bebas+Neue&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>" />
	
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 
	<style type="text/css">
	body{
		background-image: url(<?php echo base_url('assets/images/bg/bgBlud.png') ?>);
		background-repeat: no-repeat, repeat;
		background-size:cover;
	}

	.circle-menu-box {
	width:700px;
	height: 700px;
	position: relative;
	
}
	.circle-menu-box a.menu-item {
		display: block;
		text-decoration: none;
		/*border-radius: 100%;*/
		/*margin:20px;*/
		/*text-align: center;*/
		width:150px;
		height:150px;
		/*background-color:#fff;*/
		/*color:#777;
		padding:27px;*/
		position: absolute;
		/*font-size: 27px;*/

		transition:all 0.5s;
		-moz-transition:all 0.5s;
		-webkit-transition:all 0.5s;
		-o-transition:all 0.5s;
	}

	.circle-menu-box a.menu-item:hover {
		transform:scale(1.5);
		-webkit-transform:scale(1.5);
		-moz-transform:scale(1.5);
		-o-transform:scale(1.5);
		/*color:#fff;
		background: #ff3333;*/
	}

	.footer {
		  position: fixed;
		  left: 0;
		  bottom: 0;
		  width: 100%;
		  color: white;
		  text-align: center;
		  font-family: 'Rajdhani', sans-serif;
		}

.modal.fade .modal-dialog {
 -webkit-transform: translate(0);
 -moz-transform: translate(0);
 transform: translate(0);
 }
	</style>
</head>
<body>
<!--Desktop-->

<!-- <a class="btn btn-primary" href="<?php echo base_url('pemohon/req_pemohon')?>" role="button">Link</a>

 -->
<center>
	<h1 style="color: white;font-family: 'Bebas Neue', cursive;color:orange;margin-top: 50px">BADAN LAYANAN UMUM DAERAH (BLUD) DANA BERGULIR</h1>
<div style="margin-top: 50px">

		<div class="fadeIn animated circle-menu-box">

			
			<a href="<?php echo base_url('persyaratan')?>" class="menu-item">
				<img width="100%" src="<?php echo base_url('assets/images/bg/persyaratan.png')?>">
			</a>
			
			<a href="<?php echo base_url('tujuan')?>" class="menu-item">
				<img width="100%" src="<?php echo base_url('assets/images/bg/tujuan.png')?>">
			</a>

			<a href="<?php echo base_url('pinjaman')?>" class="menu-item">
				<img width="100%" src="<?php echo base_url('assets/images/bg/pinjaman.png')?>">
			</a>

			<a href="<?php echo base_url('sop')?>" class="menu-item">
				<img width="100%" src="<?php echo base_url('assets/images/bg/sop.png')?>">
			</a>

			<a href="<?php echo base_url('pemohon/req_pemohon')?>" class="menu-item">
				<img width="100%" src="<?php echo base_url('assets/images/bg/login.png')?>">
			</a>		

		</div>

	</div>
</div>
</center>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script type="text/javascript">

	var items = document.querySelectorAll('.circle-menu-box a.menu-item');

	for(var i = 0, l = items.length; i < l; i++) {
	  items[i].style.left = (40 - 35*Math.cos(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%";
	  
	  items[i].style.top = (40 + 35*Math.sin(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%";
	}

</script>

<footer class="footer">
	<center>Copyright © 2019 | Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam</center>
</footer>



<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>



<!-- mobile-->



</body>
</html>