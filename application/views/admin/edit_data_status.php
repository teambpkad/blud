<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT--><div class="row">
                  <div class="col-xs-12">
                    <a href="<?=base_url('admin/dashboard_admin')?>" class="btn btn-warning">
                        <i class="fa fa-undo"></i> Kembali
                    </a>
                    <center><h3>Edit Status Pemohon</h3></center>
                        <div class="active tab-content" id="pills-tabContent">
                          <?php foreach ($biodata as $key => $b) : ?>  
                            <form method="post" action="<?php echo base_url('admin/dashboard_admin/update_data_status');?>" enctype="multipart/form-data">
                                <div class="form-group">
                                  <label>Nama Lengkap</label>
                                  <input type="hidden" name="id_user" class="form-control" value="<?php echo $b->id_user?>">
                                  <input type="text" name="username" class="form-control" value="<?php echo $b->nama_lengkap?>" readonly>
                                </div>
                                <div class="form-group">
                                  <label>Status Survei</label>
                                  <input type="text" name="username" class="form-control" value="<?php echo $b->status_survei?>" readonly>
                                </div>
                                <div class="form-group">
                                  <label>Hasil Survei</label>
                                  <input type="text" name="username" class="form-control" value="<?php echo $b->hasil_survei?>" readonly>
                                </div>
                                <div class="form-group">
                                  <label>Status Akhir</label>
                                  <input type="text" name="username" class="form-control" value="<?php echo $b->status_akhir?>">
                                </div>
                                <div class="form-group">
                                  <label>Keterangan</label>
                                  <input type="text" name="ket" class="form-control" value="<?php echo $b->ket?>">
                                </div>
                                <div class="box-footer">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>  
                            </form>
                          <?php endforeach;?>
                        </div>   
                  </div>
                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>