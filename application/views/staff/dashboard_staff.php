<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <table id="table-renstra" class="table table-striped table-bordered" width="100%" cellspacing="0" enctype="multipart/form-data">
                          <thead>
                            <tr>
                              <th class="center">No</th>
                                <th class="center">Nama Lengkap</th>
                                <th class="center">No KTP</th>
                                <th class="center">Tanggal Permohonan</th>
                                <th Class="Center">No Hp</th>
                                <th Class="Center">Status Survei</th>
                                <th Class="Center">Hasil Survei</th>
                                <th Class="Center">Status Akhir</th>
                                <th Class="center">Keterangan</th>
                                <th class="Center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($b as $key => $us):

                                $no=1;
                                if($us->status_survei=="Sudah" && $us->status_akhir=="Sudah"){
                                  $test ="#00000 ";
                                }else{
                                  $test ="#FA8072";
                                }
                                ?>
                                
                                </tr>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $no+$key ?></td>
                                  <td style="background-color:  <?= $test ?>"><a href="<?php echo base_url('staff/dashboard_staff/edit/'.$us->id_user)?>"><?php  echo $us->nama_lengkap ?></a></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->no_ktp ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->tgl_pemohon ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->no_hp ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->status_survei ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->hasil_survei ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->status_akhir ?></td>
                                  <td style="background-color:  <?= $test ?>"><?php  echo $us->ket ?></td>
                                  <td>
                                    <div class="hidden-sm hidden-xs action-buttons">
                                      <a class="green" href="<?php echo base_url('staff/dashboard_staff/edit_data_status/'.$us->id_user)?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                      </a>
                                    </div>

                                    <div class="hidden-md hidden-lg">
                                      <div class="inline pos-rel">
                                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                          <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                          <li>
                                            <a href="<?php echo base_url('staff/dashboard_staff/edit_data_status/'.$us->id_user)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                              <span class="green">
                                                <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                              </span>
                                            </a>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                  </td>
                                </tr>

                      <?php endforeach ?>
                          </tbody>
                          </table>
                    </div>

                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>