<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div>
<!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Biodata Pemohon</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Data Usaha Pemohon</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Data Jaminan Pemohon</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-laba-tab" data-toggle="pill" href="#pills-laba" role="tab" aria-controls="pills-laba" aria-selected="false">Laporan Laba/Rugi</a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" id="pills-neraca-tab" data-toggle="pill" href="#pills-neraca" role="tab" aria-controls="pills-neraca" aria-selected="false">Laporan Neraca</a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" id="pills-lampiran-tab" data-toggle="pill" href="#pills-lampiran" role="tab" aria-controls="pills-lampiran" aria-selected="false">Lampiran Berkas</a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" id="pills-cetak-tab" data-toggle="pill" href="#pills-cetak" role="tab" aria-controls="pills-cetak" aria-selected="false">Cetak Laporan</a>
                          </li>
                        </ul>
                    </div>
                        <div class="tab-content" id="pills-tabContent">
                          <div class="tab-pane fade in" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                              <form method="post" action="<?php echo base_url('staff/data_pemohon/edit_bio');?>" enctype="multipart/form-data">
                                
                           <?php foreach ($biodata as $key => $us) : ?> 

                                    <div class="box-body">
                                        <div class="form-group">
                                           <!--  <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $us->nama_lengkap?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No KTP</label>
                                                <input type="text" class="form-control" name="no_ktp" placeholder="No KTP" value="<?php echo $us->no_ktp?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>No HP/Telp</label><br>
                                            <input type="text" class="form-control" name="no_hp" placeholder="No HP/Telp" value="<?php echo $us->no_hp?>"> 
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai KTP</label><br>
                                            <textarea name="alamat_ktp" style="width:100%;padding:10px;" placeholder="Alamat KTP" rows="2" cols="80" ><?php echo $us->alamat_ktp?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Sesuai Domisili</label><br>
                                            <textarea name="alamat_domisili" style="width:100%;padding:10px;" placeholder="Alamat Domisili" rows="2" cols="80" ><?php echo $us->alamat_domisili; ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $us->tempat_lahir?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal Lahir</label>
                                                <input type="text" class="form-control" name="tgl_lahir" value="<?php echo $us->tgl_lahir?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status</label>
                                                <input type="text" class="form-control" name="status" value="<?php echo $us->status?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jenis Kelamin</label>
                                                <input type="text" class="form-control" name="jk" value="<?php echo $us->jk?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Pendidikan Terakhir</label>
                                                <input type="text" class="form-control" name="pendidikan" placeholder="Pendidikan Terakhir" value="<?php echo $us->pendidikan?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Ibu Kandung</label>
                                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Ibu Kandung" value="<?php echo $us->nama_ibu?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Kerabat Yang Bisa Dihubungi</label>
                                                <input type="text" class="form-control" name="nama_kerabat" placeholder="Nama Kerabat" value="<?php echo $us->nama_kerabat?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Kerabat</label>
                                                <input type="text" class="form-control" name="hp_kerabat" placeholder="Hp/Telp Kerabat" value="<?php echo $us->hp_kerabat?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nominal Yang Diajukan</label><br>
                                            <input type="text" class="form-control" name="nominal" placeholder="RP " value="<?php echo $us->nominal?>"> 
                                        </div>

                                        <div class="box-footer">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Update</button>
                                        </div>
                                    </div>
                                    </div>
                                  <?php endforeach;?>
                                </form>
                            </div>

                          <div  class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <?php foreach ($biodata as $key => $us) : ?>
                                <form method="post" action="<?php echo base_url('staff/data_pemohon/edit_usaha');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Usaha</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" value="<?php echo $us->nama_usaha?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" value="<?php echo $us->bidang_usaha?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80"><?php echo $us->alamat_usaha;?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Mulai Usaha</label>
                                                <input type="text" class="form-control" name="mulai_usaha" placeholder="Mulai Usaha" value="<?php echo $us->mulai_usaha?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" class="form-control" name="jmlh_karyawan" placeholder="Jumlah Karyawan" value="<?php echo $us->jmlh_karyawan?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" value="<?php echo $us->hp_usaha?>">
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Update</button>
                                        </div>
                                    </div>
                                </form>
                                <?php endforeach;?>
                                
                          </div>
                          <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            
                                <?php foreach ($biodata as $key => $us) : ?>
                            <form method="post" action="<?php echo base_url('staff/data_pemohon/edit_jaminan');?>" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <!-- <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bentuk/Jenis Jaminan</label>
                                                <input type="text" class="form-control" name="bentuk_jaminan" placeholder="Bentuk Jaminan" value="<?php echo $us->bentuk_jaminan?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanda/Bukti Jaminan</label>
                                                <input type="text" class="form-control" name="bukti_jaminan" placeholder="Bukti Jaminan" value="<?php echo $us->bukti_jaminan?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Pemilik Jaminan</label>
                                                <input type="text" class="form-control" name="nama_pemilik" placeholder="Nama Pemilik" value="<?php echo $us->nama_pemilik?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal Jaminan</label>
                                                <input type="date" class="form-control" name="tgl_jaminan" placeholder="Tanggal Jaminan" value="<?php echo $us->tgl_jaminan?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Jaminan</label><br>
                                            <textarea name="alamat_jaminan" style="width:100%;padding:10px;" placeholder="Alamat Jaminan" rows="2" cols="80"><?php echo $us->alamat_jaminan; ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nilai Jaminan</label>
                                                <input type="text" class="form-control" name="nilai_jaminan" placeholder="Nilai Jaminan" value="<?php echo $us->nilai_jaminan?>">
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Update</button>
                                        </div>
                                    </div>
                                    
                                </form>
                                <?php endforeach;?>
                                
                          </div>


                          <div class="tab-pane fade" id="pills-laba" role="tabpanel" aria-labelledby="pills-laba-tab">
                             <form method="post" action="<?php echo base_url('staff/data_pemohon/edit_laba');?>" enctype="multipart/form-data">
                                  <?php foreach ($biodata as $key => $us) : ?>
                                  <div class="table-responsive">
                                    <table class="table table-bordered">
                                      <thead>
                                        <div class="form-group">
                                            <!-- <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                                        </div>
                                        <tr>
                                          <th scope="col">NO</th>
                                          <th scope="col">URAIAN</th>
                                          <th>BULAN<span style="margin:5px "></span>
                                            <input type="" name="" value="<?php echo $us->bulan1?>">
                                          </th>
                                          <th>BULAN<span style="margin:5px "></span>
                                            <input type="" name="" value="<?php echo $us->bulan2?>">
                                          </th>
                                          <th>BULAN<span style="margin:5px "></span>
                                             <input type="" name="" value="<?php echo $us->bulan3?>">
                                          </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th rowspan="4">1</th>
                                          <td><strong><u>PENDAPATAN</u></strong> </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENDAPATAN USAHA</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha1" style="width: 80%" value="<?php echo $us->pendusaha1?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha2" style="width: 80%" value="<?php echo $us->pendusaha2?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendusaha3" style="width: 80%" value="<?php echo $us->pendusaha3?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENDAPATAN LAIN_LAIN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain1" style="width: 80%" value="<?php echo $us->pendlain1?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain2" style="width: 80%" value="<?php echo $us->pendlain2?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pendlain3" style="width: 80%" value="<?php echo $us->pendlain3?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TOTAL PENDAPATAN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpend1?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpend2?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpend3 ?>"></td>
                                        </tr>
                                         <tr>
                                          <th rowspan="8">2</th>
                                          <td><strong><u>PENGELUARAN</u></strong> </td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PEMBELIAN BAHAN BAKU</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan1" style="width: 80%" value="<?php echo $us->pengbahan1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan2" style="width: 80%" value="<?php echo $us->pengbahan2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengbahan3" style="width: 80%" value="<?php echo $us->pengbahan3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TENAGA KERJA<strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga1" style="width: 80%" value="<?php echo $us->pengtenaga1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga2" style="width: 80%" value="<?php echo $us->pengtenaga2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtenaga3" style="width: 80%" value="<?php echo $us->pengtenaga3 ?>"></td>
                                        <tr>
                                          <td><strong>LISTRIK, AIR, TELP/HP, DLL</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik1" style="width: 80%" value="<?php echo $us->penglistrik1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik2" style="width: 80%" value="<?php echo $us->penglistrik2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglistrik3" style="width: 80%" value="<?php echo $us->penglistrik3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>BIAYA ADMINISTRASI</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm1" style="width: 80%" value="<?php echo $us->pengadm1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm2" style="width: 80%" value="<?php echo $us->pengadm2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengadm3" style="width: 80%" value="<?php echo $us->pengadm3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TRANSAPORTASI</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran1" style="width: 80%" value="<?php echo $us->pengtran1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran2" style="width: 80%" value="<?php echo $us->pengtran2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pengtran3" style="width: 80%" value="<?php echo $us->pengtran3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>PENGELUARAN LAIN-LAIN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain1" style="width: 80%" value="<?php echo $us->penglain1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain2" style="width: 80%" value="<?php echo $us->penglain2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="penglain3" style="width: 80%" value="<?php echo $us->penglain3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <td><strong>TOTAL PENGELUARAN</strong></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpeng1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpeng2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->totalpeng3 ?>"></td>
                                        </tr>
                                        <tr>
                                          <th rowspan="1">3</th>
                                          <td><strong><u>SURPLUS/DEFISIT (1-2)</u></strong> </td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->def1 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->def2 ?>"></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="pusaha" style="width: 80%" value="<?php echo $us->def3 ?>"></td>
                                        </tr>

                                      </tbody>
                                    </table>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Update</button>
                                    </div>
                                  </div>
                                    
                                </form>
                                <?php endforeach;?>
                          </div>

                          <div class="tab-pane fade" id="pills-neraca" role="tabpanel" aria-labelledby="pills-neraca-tab">

                           <form method="post" action="<?php echo base_url('pemohon/data_neraca/tambah_neraca');?>" enctype="multipart/form-data">
                                  <?php foreach ($biodata as $key => $us) : ?>
                                  <div class="form-group">
                                    <center>
                                      <h4>
                                        <th>Bulan<span style="margin:5px "></span>
                                          <input type="text" value="<?php echo $us->bulan?>" name="" readonly>
                                        </th>
                                      </h4>                        
                                    </center>
                                    <div class="form-group">
                                        <!-- <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                                    </div>
                                  </div>
                                  <div class="table-responsive">
                                    <table class="table table-bordered">
                                      <thead>

                                        <tr>
                                          <th>No</th>
                                          <th>Perkiraan</th>
                                          <th>Jumlah</th>
                                          <th>No</th>
                                          <th>Perkiraan</th>
                                          <th>Jumlah</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th rowspan="6">A</th>
                                          <td>Aktiva Lancar</td>
                                          <td></td>
                                          <td rowspan="6">C</td>
                                          <td>Hutang</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>1. Kas</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kas" style="width: 80%" value="<?php echo $us->kas?>" readonly></td>
                                          <td rowspan="4">1. Hutang Usaha</td>
                                          <td rowspan="4"><span style="margin: 10px">Rp</span><input type="text" name="htgusaha" style="width: 80%" value="<?php echo $us->htgusaha?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td>2. Bank</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="bank" style="width: 80%" value="<?php echo $us->bank?>" readonly></td>
                                        </tr>

                                        <tr>
                                          <td>3. Piutang</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="piutang" style="width: 80%" value="<?php echo $us->piutang?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td>4. Persediaan</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo $us->persediaan?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><center><strong>Jumlah Aktiva Lancar</strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo $us->jmlhaktiva_lancar?>" readonly></td>
                                          <td><center><strong>Jumlah Hutang</strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="persediaan" style="width: 80%" value="<?php echo $us->jmlhhutang?>" readonly></td>
                                        </tr>
                                        <tr></tr>
                                        <tr>
                                          <th rowspan="6">B</th>
                                          <td>Aktiva Tetap</td>
                                          <td></td>
                                          <td rowspan="6">D</td>
                                          <td>Modal</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>1. Tanah</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="tanah" style="width: 80%" value="<?php echo $us->tanah?>" readonly></td>
                                          <td>1.Modal Usaha</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="mdlusaha" style="width: 80%" value="<?php echo $us->mdlusaha?>" readonly></td>
                                        </tr>

                                        <tr>
                                          <td>2. Bangunan</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="bangunan" style="width: 80%" value="<?php echo $us->bangunan?>" readonly></td>
                                          <td rowspan="2">2.Laba Bulan Berjalan</td>
                                          <td rowspan="2"><span style="margin: 10px">Rp</span><input type="text" name="laba" style="width: 80%" value="<?php echo $us->laba?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td>3. Kendaraan</td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo $us->kendaraan?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><center><strong>Jumlah Aktiva Tetap</strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo $us->jmlhaktiva_tetap?>" readonly></td>
                                          <td><center><strong>Jumlah Modal</strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo $us->jmljhmodal?>" readonly></td>
                                        </tr>
                                        <tr>
                                          <td><center><strong>Jumlah Aktiva </strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo $us->totalaktiva?>" readonly></td>
                                          <td><center><strong>Jumlah Hutang dan Modal</strong></center></td>
                                          <td><span style="margin: 10px">Rp</span><input type="text" name="kendaraan" style="width: 80%" value="<?php echo $us->totalhtg?>" readonly></td>
                                        </tr>

                                      </tbody>
                                    </table>
                                    <?php endforeach;?>
                                </form>
                            </div>
                          </div>



                          <div class="tab-pane fade" id="pills-lampiran" role="tabpanel" aria-labelledby="pills-lampiran-tab">
                            <?php foreach ($biodata as $key => $us) : ?>
                          <form method="post" action="<?php echo base_url('auth/update_usaha');?>" enctype="multipart/form-data">
                            <div></div>
                            <div class="form-group">
                                <!-- <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> --> 
                            </div>
                            <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
                            <style>
                              

                              #myImg {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg:hover {opacity: 0.7;}

                                #myImg1 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg1:hover {opacity: 0.7;}
                              
                              #myImg2 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg2:hover {opacity: 0.7;}

                              #myImg3 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg3:hover {opacity: 0.7;}

                              #myImg4 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg4:hover {opacity: 0.7;}

                              #myImg5 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg5:hover {opacity: 0.7;}

                              #myImg6 {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg6:hover {opacity: 0.7;}


                              /* The Modal (background) */
                              .modal {
                                display: none; /* Hidden by default */
                                position: fixed; /* Stay in place */
                                z-index: 1; /* Sit on top */
                                padding-top: 100px; /* Location of the box */
                                left: 0;
                                top: 0;
                                width: 100%; /* Full width */
                                height: 100%; /* Full height */
                                overflow: auto; /* Enable scroll if needed */
                                background-color: rgb(0,0,0); /* Fallback color */
                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                              }

                              /* Modal Content (image) */
                              .modal-content {
                                margin: auto;
                                display: block;
                                width: 80%;
                                max-width: 700px;
                              }

                              /* Add Animation */
                              .modal-content, #caption {  
                                -webkit-animation-name: zoom;
                                -webkit-animation-duration: 0.6s;
                                animation-name: zoom;
                                animation-duration: 0.6s;
                              }

                              @-webkit-keyframes zoom {
                                from {-webkit-transform:scale(0)} 
                                to {-webkit-transform:scale(1)}
                              }

                              @keyframes zoom {
                                from {transform:scale(0)} 
                                to {transform:scale(1)}
                              }

                              

                              /* 100% Image Width on Smaller Screens */
                              @media only screen and (max-width: 700px){
                                .modal-content {
                                  width: 100%;
                                }
                              }
                              </style>
                              <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center">
                                    <center><label><h1>KTP Suami</h1></label></center>
                                    <center><img data-u="image" id="myImg" class="img-fluid" style="width:50%; height:50%" src="<?php echo base_url().'uploads/file/'.$us->ktp_suami;?>" alt="KTP Suami"></center>
                                  </div>
                                  
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>KTP Istri</h1></label></center>
                                    <center><img data-u="image" id="myImg1" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->ktp_istri;?>"></center>
                                  </div>
                                </div>
                                  <hr>
                                <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center">
                                    <center><label><h1>Kartu Keluarga</h1></label></center>
                                    <center><img data-u="image" id="myImg2" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->kk;?>"></center>
                                  </div>
                                  
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>IUMK</h1></label></center>
                                    <center><img data-u="image" id="myImg3" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->iumk;?>"></center>
                                  </div>
                                  
                                </div>
                                <hr>
                                <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Suami</h1></label></center>
                                    <center><img class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_suami;?>"></center>
                                  </div>
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Istri</h1></label></center>
                                    <center><img class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_istri;?>"></center>
                                  </div>                               
                                  
                                  </div>
                                  <hr>
                                  <div class="row">
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Buku Nikah</h1></label></center>
                                    <center><img data-u="image" id="myImg4" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->buku_nikah;?>"></center>
                                  </div>
                                 
                                  <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Foto Usaha</h1></label></center>
                                    <center><img data-u="image" id="myImg5" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->foto_usaha;?>"></center>
                                  </div>
                                </div>
                                  <hr>
                               
                                  <div data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center" >
                                    <center><label><h1>Sertifikat Jaminan</h1></label></center>
                                    <center><img data-u="image" id="myImg6" class="img-fluid" style="width:50%;height: 50% " src="<?php echo base_url().'uploads/file/'.$us->ser_jaminan;?>"></center>
                                  </div>
                                  <hr>
                                  </form>
                          <?php endforeach;?>
                              </div>
                               <div  class="tab-pane fade" id="pills-cetak" role="tabpanel" aria-labelledby="pills-cetak-tab">
                                    <div class="row">
                                       
                                        <?php {?>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportdata/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Data Calon Mitra Binaan</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportpernyataan/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Surat Pernyataan</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportneraca/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Neraca</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportpermohonanpinjaman/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Surat Permohonan Pinjaman</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportkuasasuamiistri/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Surat Persetujuan & Kuasa Suami/ Istri</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportkuasapersetujuan/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Surat Kuasa dan Persetujuan</button></a>
                                        <br><br></div>
                                        <div class="col-md-3"> 
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportpenerimaandanpengeluaran/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Penerimaan dan Pengeluaran</button></a> 
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportsurveypenilaianjaminan/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Survey Penilaian Jaminan</button></a>
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportsurveypinjamandanabergulir/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Survey Pinjaman Dana Bergulir</button></a>
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportpetalokasi/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Peta Lokasi</button></a>
                                        <br><br></div>
                                        <div class="col-md-3">
                                        <a href="<?php echo base_url('C_laporan/C_DataReport/reportsurveypelaksana/'.$us->id_user)?>" target="_blank"><button class="btn btn-md btn-link"><i class="fa fa-print fa-2x" aria-hidden="true"></i> Survey Pelaksana</button></a>
                                        <br><br></div>
                                    <?php } ?>
                                    </div>

                          </div>
                              <!-- The Modal KTP SUAMI -->
                          <div id="myModal" class="modal">
     
                            <img class="modal-content" id="img01">
                          
                          </div>

                          <!-- The Modal KTP ISTRI -->
                          <div id="myModal1" class="modal">
                            <img class="modal-content" id="img02">
                          </div>

                          <!-- The Modal KK -->
                          <div id="myModal2" class="modal">
                            <img class="modal-content" id="img03">
                          </div>

                           <!-- The Modal IUMK -->
                          <div id="myModal3" class="modal">
                            <img class="modal-content" id="img04">
                          </div>

                          <!-- The Modal BUKU NIKAH -->
                          <div id="myModal4" class="modal">
                            <img class="modal-content" id="img05">
                          </div>

                          <!-- The Modal FOTO USAHA -->
                          <div id="myModal5" class="modal">
                            <img class="modal-content" id="img06">
                          </div>

                          <!-- The Modal SERTIFIKAT JAMINAN -->
                          <div id="myModal6" class="modal">
                            <img class="modal-content" id="img07">
                          </div>

                          <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
                          <script>
                            AOS.init();
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg");
                          var modalImg = document.getElementById("img01");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // Get the <span> element that closes the modal
                          var span = document.getElementsByClassName("close")[0];

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal1");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg1");
                          var modalImg = document.getElementById("img02");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal2");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg2");
                          var modalImg = document.getElementById("img03");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal3");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg3");
                          var modalImg = document.getElementById("img04");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal4");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg4");
                          var modalImg = document.getElementById("img05");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal5");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg5");
                          var modalImg = document.getElementById("img06");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal6");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg6");
                          var modalImg = document.getElementById("img07");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>

                       <!--End TabPane-->
              </div>                  
            </div>  
          </div>
        </div>
      </div>             
</div>
</div>