<!-- <style type="text/css">
  .grid-container {
  display: grid;
  grid-template-columns: auto auto ;
  grid-gap: 10px;
  /*background-color: #2196F3;*/
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  /*s*/
  text-align: justify;
  font-size: 12px;
}
</style> -->
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        
                      <div class="active tab-content" id="pills-tabContent">
                          <center><h3>LAPORAN PENILAIAN (SURVEY LAPANGAN)</h3></center>
                                <form method="post" action="<?php echo base_url('pemohon/data_usaha/tambah_usaha');?>" enctype="multipart/form-data">
                          <div class="box-body">
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_user");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Usaha</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" required="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80" required=""></textarea>
                                        </div>                                   
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" required="">
                                            </div>
                                        </div>


                          <center><h3>KONDISI RUMAH/TEMPAT USAHA</h3></center><br>
                              <div class="row">
                                <form action="">
                                  <div class="col-md-6"><p>Rumah yang ditempati sekarang ini merupakan :</p>
                                              
                                      <input type="radio" name="ms" value="ms"> Milik Sendiri<br>
                                      <input type="radio" name="sb" value="sb"> Sewa Bulanan<br>
                                      <input type="radio" name="dinas" value="dinas"> Dinas<br>
                                      <input type="radio" name="rot" value="rot"> Rumah orang tua/Keluarga<br>     
                                      <input type="radio" name="kt" value="kt"> Kontrak Tahunan<br>
                                      <input type="radio" name="kredit" value="kredit"> Milik Sendiri (Kredit)<br>
                                                              
                                  </div>
                                  </form>
                                  <div class="col-md-6"><p>Lokasi Tempat Usaha :</p>
                                      <input type="checkbox" name="vehicle1" value=""> Pemukiman<br>
                                      <input type="checkbox" name="vehicle2" value=""> Perkampungan<br>
                                      <input type="checkbox" name="vehicle3" value=""> Pabrik<br>
                                      <input type="checkbox" name="vehicle4" value=""> Pasar/Mall<br>
                                  </div>
                              </div>


                          <center><h3>KONDISI USAHA</h3></center><br>
                            <div class="row">
                              <div class="form-group">
                              <div class="form-group col-md-6" style="background-color:;">
                                  <p>1. Usaha yang dijalankan saat ini :</p>
                                  <input type="checkbox" name="vehicle1" value=""> 1 Tahun<br>
                                  <input type="checkbox" name="vehicle2" value=""> -2 Tahun<br>
                                  <input type="checkbox" name="vehicle3" value=""> 2-5 Tahun<br>
                                  <input type="checkbox" name="vehicle4" value=""> >5 Tahun<br>
                                  <br>
                                  <p>2. Jumlah Karyawan saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Orang" required="">
                                  </div>
                                  <p>3. Omzet Usaha saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Per Bulan" required="">
                                  </div>
                                  <p>4. Biaya Operasional</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Per Bulan" required="">
                                  </div>
                                  <p>5. Biaya Keluarga</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Per Bulan" required="">
                                  </div>                         
                              </div>
                              <div class="col-xs-3 col-md-6" style="background-color:;">
                                  <p>6. Administrasi pembukuan :</p>
                                  <input type="checkbox" name="vehicle1" value=""> Ada dokumen, tercatat sesuai kaidah akuntansi & rapi<br>
                                  <input type="checkbox" name="vehicle2" value=""> Ada dokumen, tercatat tidak sesuai kaidah akuntansi<br>
                                  <input type="checkbox" name="vehicle3" value=""> Ada dokumen, tidak ada catatan<br>
                                  <input type="checkbox" name="vehicle4" value=""> Tidak ada dokumen, ada catatan<br><br>
                              </div>
                              
                              <div class="col-xs-3 col-md-6" style="background-color:;">
                                  <p>7. Etika Usaha : Pernah Mendapatkan Pinjaman (Kota/Prov/Pusat) </p>
                                  <input type="checkbox" name="vehicle1" value=""> Belum Pernah<br>
                                  <input type="checkbox" name="vehicle2" value=""> Sudah Pernah : Tunggakan Rp.<br>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Nominal Tunggakan" required="">
                                  </div>
                                  <div class="form-group col-md-12" style="margin-top: -10px">
                                      <input type="text" class="form-control" name="nama_usaha" placeholder="Dari (Kota/Prov/Pusat)" required="">
                                  </div>
                              </div>
                              </div>
                            </div>

                            <div class="row">
                              <form action="">
                                <div class="col-md-6"> <center><h3>PENGAJUAN PINJAMAN</h3></center>    
                                  <div class="form-group col-md-6" style="margin-left: 50px">                                
                                    <div class="form-group row">
                                      <label for="" class="col-sm-9 col-form-label">Pengajuan Pinjaman :</label>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" placeholder="Rp">
                                      </div>
                                      <label for="" class="col-sm-9 col-form-label">Kegunaan Dana :</label>
                                    
                                    <div class="col-sm-9" style="">
                                        <input type="text" class="form-control-plaintext" placeholder="1.">
                                      </div>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" placeholder="Rp">
                                      </div>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control-plaintext" placeholder="2.">
                                      </div>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" placeholder="Rp">
                                      </div>
                                      </div>
                                </div>
                              </form>
                            </div>
                                  <div class="col-md-6"> <center><h3>KESANGGUPAN MEMBAYAR</h3></center>
                                    <div class="form-group col-md-6" style="margin-left: 50px">                                
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Cicilan Setiap Bulan :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" placeholder="Rp">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Sumber Pengembalian Selain Usaha :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" placeholder="">


                                          </div> 
                                      </div>
                                    </div>
                                  </div>
                            </div>

                      <center><h2>FORM PENILAIAN JAMINAN</h2></center><br>
                      <div class="container-fluid">
                            <p><b>A. DATA PETUGAS PENILAIAN/SUVEYOR</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Nama Petugas Penilaian/Surveyor</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Tujuan Penilaian</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Tanggal Penilaian</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>B. DATA CALON PEMINJAM</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Nama Calon Peminjam</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat Peminjam</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Alamat Jaminan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Hubungan Peminjam dan Jaminan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>C. DATA TANAH</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Hak Tanah/Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. No. Sertipikat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Status Sertipikat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Tanggal Sertipikat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Nama Pemegang Hak</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Wilayah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tanggal dikeluarkan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Tanggal Jatuh Tempo Sertipikat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Luas Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Bentuk Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>D. DATA BANGUNAN</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Dibangun Tahun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Renovasi Terakhir Tahun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Umur Efektif</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Luas Bangunan Sesuai Fisik</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan Sesuai IMB</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Luas Tapak Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Dihuni Tahun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Jenis Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Penggunaan Bangunan saat ini</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Kondisi Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">11. Fasilitas Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>E. SARANA PELENGKAP</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Taman</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Halaman</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Pagar</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Lainnya</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>F. DATA PEMBANDING</b></p>
                            <p><b>PEMBANDING 1</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>PEMBANDING 2</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>PEMBANDING 3</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%; margin-bottom: 10px">
                              </div>
                            </div>

                            <center><h2>PETA LOKASI RUMAH DAN USAHA</h2></center><br>
                            
                            <center><h4>LOKASI RUMAH</h4></center><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <center><h4>LOKASI USAHA</h4></center><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                      </div>      

                            <div class="box-footer">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                          </div>  
                     </form>  
                 </div>
            </div>
       </div>
        