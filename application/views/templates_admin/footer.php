<!-- footer 
  
  data->template/footer 

/footer---------------------------------------------------------------------------------->
 
      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
      </a>
</div><!-- /.main-container -->

    <!-- basic scripts -->
    <div class="main-container ace-save-state" id="main-container">
      <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
      </script>

   <div class="footer">
        <div class="footer-inner">
          <div class="footer-content">
            <span class="bigger-120">
              Dashboard administrator <span class="blue bolder">bpkad-batam</span> &copy; 2019-2025
            </span>
          </div>
        </div>
      </div>

   <!--[if !IE]> -->
    <script src="<?php echo base_url('assets/back')?>/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!-- dataTables -->
    <script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.bootstrap.min.js"></script>
    <!-- /dataTables -->
    <!--[if IE]>
<script src="<?php echo base_url('assets/back')?>/js/jquery-1.11.3.min.js"></script>
<![endif]-->
    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url('assets/back')?> /js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="<?php echo base_url('assets/back')?>/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="<?php echo base_url('assets/back')?>/js/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url('assets/back')?>/js/jquery-ui.custom.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.flot.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.flot.resize.min.js"></script>

    <!-- ace scripts -->
    <script src="<?php echo base_url('assets/back')?>/js/ace-elements.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/ace.min.js"></script>

    <!-- dataTables -->
    <script src="<?php echo base_url('assets/back')?>/js/.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/back')?>/js/buttons.flash.min.js"></script>

    <script src="<?php echo base_url('assets/back')?>/js/dataTables.select.min.js"></script>
    <!-- inline scripts related to this page -->


    <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
  $(document).ready(function(){
    $('#table-user').DataTable();
  });

 $(document).ready(function(){
    $('#log-table').DataTable();
  });

 $(document).ready(function(){
    $('#regulasi-table').DataTable();
  });

 $(document).ready(function(){
    $('#foto-table').DataTable();
  });

 $(document).ready(function(){
    $('#table-renstra').DataTable();
  });
  
</script>

<script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/back')?>/js/jquery.dataTables.bootstrap.min.js"></script>

<!--<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script><!-->
<!--<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>!-->

  </body>
</html>  