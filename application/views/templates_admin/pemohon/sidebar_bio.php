     <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <ul class="nav nav-list">
          
          <li class="">
            <a href="<?php echo base_url('pemohon/dashboard_pemohon')?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="active">
            <a href="<?php echo base_url('pemohon/data_biodata')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Biodata</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_usaha')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Data Usaha</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_jaminan')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Data Jaminan</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_kuasa')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Data Kuasa</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_laba')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Laporan Laba/Rugi</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_neraca')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Laporan Neraca</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('pemohon/data_file')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Lampiran Berkas</span>
            </a>

            <b class="arrow"></b>
          </li>

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>