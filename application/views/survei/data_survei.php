<!-- <style type="text/css">
  .grid-container {
  display: grid;
  grid-template-columns: auto auto ;
  grid-gap: 10px;
  /*background-color: #2196F3;*/
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  /*s*/
  text-align: justify;
  font-size: 12px;
}
</style> -->
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">==
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        
                      <div class="active tab-content" id="pills-tabContent">
                          <center><h3>LAPORAN PENILAIAN (SURVEY LAPANGAN)</h3></center>
                          <?php foreach ($biodata as $key => $b) : ?>
                                <form method="post" action="<?php echo base_url('survei/data_survei/tambah_survei');?>" enctype="multipart/form-data">
                          <div class="box-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" value="<?php echo $b->nama_lengkap ?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" value="<?php echo $b->bidang_usaha ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80" readonly=""><?php echo $b->alamat_usaha; ?></textarea>
                                        </div>                                   
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" value="<?php echo $b->hp_usaha; ?>" readonly>
                                            </div>
                                        </div>

                          <center><h3>KONDISI RUMAH/TEMPAT USAHA</h3></center><br>
                              <div class="row">
                                <form action="">
                                  <div class="col-md-6"><p>Rumah yang ditempati sekarang ini merupakan :</p>
                                       <div class="form-check">
                                          <input class="form-check-input" type="radio" name="rumah" value="Milik Sendiri" checked>
                                          <label class="form-check-label">
                                            Milik Sendiri
                                          </label>
                                        </div>
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="rumah"  value="Sewa Bulanan">
                                          <label class="form-check-label">
                                            Sewa Bulanan
                                          </label>
                                        </div>
                                        <div class="form-check disabled">
                                          <input class="form-check-input" type="radio" name="rumah"value="Dinas">
                                          <label class="form-check-label">
                                            Dinas
                                          </label>
                                        </div> 
                                        <div class="form-check disabled">
                                          <input class="form-check-input" type="radio" name="rumah" value="Rumah orang tua/Keluarga">
                                          <label class="form-check-label">
                                            Rumah orang tua/Keluarga
                                          </label>
                                        </div>  
                                        <div class="form-check disabled">
                                          <input class="form-check-input" type="radio" name="rumah" value="Kontrak Tahunan">
                                          <label class="form-check-label">
                                            Kontrak Tahunan
                                          </label>
                                        </div> 
                                        <div class="form-check disabled">
                                          <input class="form-check-input" type="radio" name="rumah"value="Milik Sendiri (Kredit)">
                                          <label class="form-check-label">
                                            Milik Sendiri (Kredit)
                                          </label>
                                        </div>                  
                                  </div>
                                  </form>
                                  <div class="col-md-6"><p>Lokasi Tempat Usaha :</p>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="lokasi" value="Pemukiman" checked>
                                      <label class="form-check-label">
                                       Pemukiman
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="lokasi" value="Perkampungan" >
                                      <label class="form-check-label">
                                        Perkampungan
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="lokasi" value="Pabrik" >
                                      <label class="form-check-label">
                                        Pabrik
                                      </label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" name="lokasi" value="Pasar/Mall" >
                                      <label class="form-check-label">
                                        Pasar/Mall
                                      </label>
                                    </div>
                                  </div>
                              </div>


                          <center><h3>KONDISI USAHA</h3></center><br>
                            <div class="row">
                              <div class="form-group">
                              <div class="form-group col-md-6" style="background-color:;">
                                  <p>1. Usaha yang dijalankan saat ini :</p>
                                    <input type="radio" name="lama" value="1 Tahun" checked=""> 1 Tahun<br>
                                    <input type="radio" name="lama" value="-2 Tahun" > -2 Tahun<br>
                                    <input type="radio" name="lama" value="2-5 Tahun" > 2-5 Tahun<br>
                                    <input type="radio" name="lama" value=">5 Tahun" > >5 Tahun<br>
                                <br>
                                  <p>2. Jumlah Karyawan saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" value="<?php echo $b->jmlh_karyawan ?> Orang" readonly>
                                  </div>
                                  <p>3. Omzet Usaha saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="omset" placeholder="Per Bulan" >
                                  </div>
                                  <p>4. Biaya Operasional</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="operasional" placeholder="Per Bulan">
                                  </div>
                                  <p>5. Biaya Keluarga</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="keluarga" placeholder="Per Bulan">
                                  </div>                         
                              </div>
                              <div class="col-xs-3 col-md-6" style="background-color:;">
                                  <p>6. Administrasi pembukuan :</p>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="adm" value="Ada dokumen, tercatat sesuai kaidah akuntansi & rapi" checked="">
                                    <label class="form-check-label">
                                      Ada dokumen, tercatat sesuai kaidah akuntansi & rapi
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="adm" value="Ada dokumen, tercatat tidak sesuai kaidah akuntansi">
                                    <label class="form-check-label">
                                      Ada dokumen, tercatat tidak sesuai kaidah akuntansi
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="adm" value="Ada dokumen, tidak ada catatan">
                                    <label class="form-check-label">
                                      Ada dokumen, tidak ada catatan
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="adm" >
                                    <label class="form-group">
                                      Tidak ada dokumen, ada catatan
                                    </label>
                                  </div>
                              </div>
                              
                              <div class="col-xs-3 col-md-6" style="background-color:;">
                                  <p>7. Etika Usaha : Pernah Mendapatkan Pinjaman (Kota/Prov/Pusat) </p>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="etika" checked="">
                                    <label class="form-check-label">
                                      Belum Pernah
                                    </label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="etika" >
                                    <input class="form-group " type="text" name="etika" placeholder="Sudah Pernah (Nominal)">
                                  </div>
                              </div>
                              </div>
                            </div>

                            <div class="row">
                              <form action="">
                                <div class="col-md-6"> <center><h3>PENGAJUAN PINJAMAN</h3></center>    
                                  <div class="form-group col-md-6" style="margin-left: 50px">                                
                                    <div class="form-group row">
                                      <label for="" class="col-sm-9 col-form-label">Pengajuan Pinjaman :</label>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" value="<?php echo $b->nominal ?>" readonly>
                                      </div>
                                      <label for="" class="col-sm-9 col-form-label">Kegunaan Dana :</label>
                                    
                                    <div class="col-sm-9" style="">
                                        <input type="text" class="form-control-plaintext" value="<?php echo $b->keperluan ?>" readonly>
                                      </div>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" value="RP <?php echo $b->nominal ?>" readonly>
                                      </div>
                                    </div>
                                </div>
                              </form>
                            </div>
                                  <div class="col-md-6"> <center><h3>KESANGGUPAN MEMBAYAR</h3></center>
                                    <div class="form-group col-md-6" style="margin-left: 50px">                                
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Cicilan Setiap Bulan :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" name="cicilan" placeholder="Rp">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Sumber Pengembalian Selain Usaha :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" name="sumber" placeholder="">
                                          </div> 
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label>Hal Lain Yang Dapat Dijadikan Pertimbangan</label>
                                <input type="text" class="form-control" name="hal" placeholder="Hal Pertimbangan" >
                            </div>

                      <center><h2>FORM PENILAIAN JAMINAN</h2></center><br>
                      <div class="container-fluid">
                            <p><b>A. DATA PETUGAS PENILAIAN/SUVEYOR</b></p>
                            <div class="row">
                              <div class="form-group col-sm-4">1. Nama Petugas Penilaian/Surveyor</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="nama_survei" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Tujuan Penilaian</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Tanggal Penilaian</div>
                              <div class="col-sm-8">
                                <input type="date" class="form-control-plaintext" name="tgl_survei" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>B. DATA CALON PEMINJAM</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Nama Calon Peminjam</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->nama_lengkap ?>" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">2. Alamat Peminjam</div>
                              <div class="form-group  col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" style="width: 100%" readonly=""><?php echo $b->alamat_domisili; ?></textarea>                       
                              </div>
                              <div class="col-sm-4">3. Alamat Jaminan</div>
                              <div class="form-group col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" style="width: 100%" readonly=""><?php echo $b->alamat_jaminan; ?></textarea>                        
                              </div>
                              <div class="col-sm-4">4. Hubungan Peminjam dan Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->status_jaminan ?>" style="width: 100%" readonly>                        
                              </div>
                            </div>

                            <p><b>C. DATA TANAH</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Hak Tanah/Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->bentuk_jaminan ?>" readonly style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. No. Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="no_jaminan" value="<?php echo $b->bukti_jaminan ?>" placeholder="" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">3. Status Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->status_jaminan ?>"  placeholder="" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">4. Tanggal Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->tgl_jaminan ?>" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Nama Pemegang Hak</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext"  value="<?php echo $b->nama_pemilik ?>" readonly style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Wilayah</div>
                              <div class="form-group col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" readonly="" style="width: 100%"><?php echo $b->alamat_jaminan ?> </textarea>                         
                              </div>
                              <div class="col-sm-4">7. Tanggal dikeluarkan</div>
                              <div class="form-group col-sm-8">
                                <input type="date" class="form-control-plaintext" name="tgl_keluar_jaminan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Tanggal Jatuh Tempo Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="date" class="form-control-plaintext" name="tgl_jatuh_jaminan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Bentuk Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->bentuk_jaminan ?>" placeholder="" style="width: 100%" readonly>                        
                              </div>
                            </div>

                            <p><b>D. DATA BANGUNAN</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Dibangun Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Renovasi Terakhir Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_renovasi" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Umur Efektif</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="umur_efektif" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Luas Bangunan Sesuai Fisik</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangunan_fisik" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan Sesuai IMB</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangunan_imb" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Luas Tapak Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_tapak_bangunan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Dihuni Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_huni" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Jenis Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_bangunan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Penggunaan Bangunan saat ini</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="pemakai_bangunan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Kondisi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kondisi_bangunan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">11. Fasilitas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="fasilitas_bangunan" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>E. SARANA PELENGKAP</b></p>
                            <div class="row">
                              <div class="form-group col-sm-4">1. Taman</div>
                              <div class="form-group col-sm-8">  
                                <select type="text" name="taman" class="form-control">
                                  <option>Ada</option>
                                  <option>Tidak</option>
                                </select>                    
                              </div>
                              <div class="col-sm-4">2. Halaman</div>
                              <div class="form-group col-sm-8">
                                <select type="text" name="halaman" class="form-control">
                                  <option>Ada</option>
                                  <option>Tidak</option>
                                </select>                      
                              </div>
                              <div class="col-sm-4">3. Pagar</div>
                              <div class="form-group col-sm-8">
                                <select type="text" name="pagar" class="form-control">
                                  <option>Ada</option>
                                  <option>Tidak</option>
                                </select>                       
                              </div>
                              <div class="col-sm-4">4. Lainnya</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="lainnya" placeholder="" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>F. DATA PEMBANDING</b></p>
                            <p><b>PEMBANDING 1</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding1"  placeholder="" style="width: 100%"></textarea>                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding1" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding1" placeholder="" style="width: 100%">                        
                              </div>

                              <center><h5>LOKASI PEMBANDING 1</h5></center><br>
                              <div class="form-group">
                                  <input type="file" class="form-control" name="lokasi_banding1" required="">
                                  <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                    JPG|JPEG|PNG Ukuran 200KB
                                  </small>
                              </div>
                            </div>

                            <p><b>PEMBANDING 2</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding2"  placeholder="" style="width: 100%"></textarea>                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding2" placeholder="" style="width: 100%">                        
                              </div>
                              <center><h5>LOKASI PEMBANDING 2</h5></center><br>
                              <div class="form-group">
                                  <input type="file" class="form-control" name="lokasi_banding2" required="">
                                  <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                    JPG|JPEG|PNG Ukuran 200KB
                                  </small>
                              </div>
                            </div>

                            <p><b>PEMBANDING 3</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding3"  placeholder="" style="width: 100%"></textarea>                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding3" placeholder="" style="width: 100%">                        
                              </div>
                              <center><h5>LOKASI PEMBANDING 3</h5></center><br>
                              <div class="form-group">
                                  <input type="file" class="form-control" name="lokasi_banding3" required="">
                                  <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                    JPG|JPEG|PNG Ukuran 200KB
                                  </small>
                              </div>
                            </div>

                            <center><h2>PETA LOKASI RUMAH DAN USAHA</h2></center><br>
                            
                            <center><h4>LOKASI RUMAH</h4></center><br>
                            <div class="form-group">
                                <input type="file" class="form-control" name="lokasi_rumah" required="">
                                <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                  JPG|JPEG|PNG Ukuran 200KB
                                </small>
                            </div>
                            <center><h4>LOKASI USAHA</h4></center><br>
                            <div class="form-group">
                                <input type="file" class="form-control" name="lokasi_usaha" required="">
                                <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                  JPG|JPEG|PNG Ukuran 200KB
                                </small>
                            </div>
                      </div>      

                            <div class="box-footer">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                </div>
                          </div>  
                        <?php endforeach; ?>
                     </form>  
                 </div>
            </div>
       </div>
        