<!-- <style type="text/css">
  .grid-container {
  display: grid;
  grid-template-columns: auto auto ;
  grid-gap: 10px;
  /*background-color: #2196F3;*/
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  /*s*/
  text-align: justify;
  font-size: 12px;
}
</style> -->
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Pemohon</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">==
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Pemohon Dana Bergulir bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                        
                      <div class="active tab-content" id="pills-tabContent">
                          <center><h3>EDIT LAPORAN PENILAIAN (SURVEY LAPANGAN)</h3></center>
                          <?php foreach ($biodata as $key => $b) : ?>
                                <form method="post" action="<?php echo base_url('survei/data_survei/tambah_survei');?>" enctype="multipart/form-data">
                          <div class="box-body">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="id_user" value="<?php echo  $this->session->userdata("id_pemohon");?>"> 
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_usaha" placeholder="Nama Usaha" value="<?php echo $b->nama_lengkap ?>" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bidang Usaha</label>
                                                <input type="text" class="form-control" name="bidang_usaha" placeholder="Bidang Usaha" value="<?php echo $b->bidang_usaha ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap Usaha</label><br>
                                            <textarea name="alamat_usaha" style="width:100%;padding:10px;" placeholder="Alamat Usaha" rows="2" cols="80" readonly=""><?php echo $b->alamat_usaha; ?></textarea>
                                        </div>                                   
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Hp/Telp Usaha</label>
                                                <input type="text" class="form-control" name="hp_usaha" placeholder="Hp/Telp Usaha" value="<?php echo $b->hp_usaha; ?>" readonly>
                                            </div>
                                        </div>

                          <center><h3>KONDISI RUMAH/TEMPAT USAHA</h3></center><br>
                              <div class="row">
                                <form action="">
                                  <div class="col-md-6"><p>Rumah yang ditempati sekarang ini merupakan :</p>
                                    <input class="form-control" type="text" name="rumah" value="<?php echo $b->rumah ?>" > 
                                  </div>
                                  </form>
                                  <div class="col-md-6"><p>Lokasi Tempat Usaha :</p>
                                    <div class="form-check">
                                      <input class="form-control" type="text" name="lokasi" value="<?php echo $b->lokasi ?>">
                                    </div>
                                  </div>
                              </div>


                          <center><h3>KONDISI USAHA</h3></center><br>
                            <div class="row">
                              <div class="form-group">
                              <div class="form-group col-md-6" style="background-color:;">
                                  <p>1. Usaha yang dijalankan saat ini :</p>
                                    <input class="form-control" type="text" name="lama" value="<?php echo $b->lama ?>"><br>
                                <br>
                                  <p>2. Jumlah Karyawan saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" value="<?php echo $b->jmlh_karyawan ?> Orang" readonly>
                                  </div>
                                  <p>3. Omzet Usaha saat ini</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="omset" value="<?php echo $b->omset ?>">
                                  </div>
                                  <p>4. Biaya Operasional</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="operasional" value="<?php echo $b->operasional ?>">
                                  </div>
                                  <p>5. Biaya Keluarga</p>
                                  <div class="form-group col-md-12">
                                      <input type="text" class="form-control" name="keluarga" value="<?php echo $b->keluarga ?>">
                                  </div>                         
                              </div>
                              <div class="col-xs-3 col-md-6" style="background-color:;">
                                  <p>6. Administrasi pembukuan :</p>
                                  <div class="form-group">
                                    <input class="form-control" type="text" name="adm" value="<?php echo $b->adm ?>">
                                  </div>
                              </div>
                              
                              <div class=" col-md-6" style="background-color:;">
                                  <p>7. Etika Usaha : Pernah Mendapatkan Pinjaman (Kota/Prov/Pusat) </p>
                                  <div class="form-group">
                                    <input class="form-control" type="text" name="etika" value="<?php echo $b->etika ?>">
                                  </div>
                              </div>
                              </div>
                            </div>

                            <div class="row">
                              <form action="">
                                <div class="col-md-6"> <center><h3>PENGAJUAN PINJAMAN</h3></center>    
                                  <div class="form-group col-md-6" style="margin-left: 50px">                                
                                    <div class="form-group row">
                                      <label for="" class="col-sm-9 col-form-label">Pengajuan Pinjaman :</label>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" value="<?php echo $b->nominal ?>" readonly>
                                      </div>
                                      <label for="" class="col-sm-9 col-form-label">Kegunaan Dana :</label>
                                    
                                    <div class="col-sm-9" style="">
                                        <input type="text" class="form-control-plaintext" value="<?php echo $b->keperluan ?>" readonly>
                                      </div>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control-plaintext" value="RP <?php echo $b->nominal ?>" readonly>
                                      </div>
                                    </div>
                                </div>
                              </form>
                            </div>
                                  <div class="col-md-6"> <center><h3>KESANGGUPAN MEMBAYAR</h3></center>
                                    <div class="form-group col-md-6" style="margin-left: 50px">                                
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Cicilan Setiap Bulan :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" name="cicilan" value="<?php echo $b->cicilan ?>">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                        <label for="" class="col-sm-9 col-form-label">Sumber Pengembalian Selain Usaha :</label>
                                          <div class="col-sm-3">
                                          <input type="text" class="form-control-plaintext" name="sumber" value="<?php echo $b->sumber ?>">
                                          </div> 
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label>Hal Lain Yang Dapat Dijadikan Pertimbangan</label>
                                <input type="text" class="form-control" name="hal" value="<?php echo $b->hal ?>" >
                            </div>

                      <center><h2>FORM PENILAIAN JAMINAN</h2></center><br>
                      <div class="container-fluid">
                            <p><b>A. DATA PETUGAS PENILAIAN/SUVEYOR</b></p>
                            <div class="row">
                              <div class="form-group col-sm-4">1. Nama Petugas Penilaian/Surveyor</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="nama_survei" value="<?php echo $b->nama_survei ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Tujuan Penilaian</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->tjn_survei ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Tanggal Penilaian</div>
                              <div class="col-sm-8">
                                <input type="" class="form-control-plaintext" name="tgl_survei" value="<?php echo $b->tgl_survei ?>" style="width: 100%">                   
                              </div>
                            </div>

                            <p><b>B. DATA CALON PEMINJAM</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Nama Calon Peminjam</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->nama_lengkap ?>" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">2. Alamat Peminjam</div>
                              <div class="form-group  col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" style="width: 100%" readonly=""><?php echo $b->alamat_domisili; ?></textarea>                       
                              </div>
                              <div class="col-sm-4">3. Alamat Jaminan</div>
                              <div class="form-group col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" style="width: 100%" readonly=""><?php echo $b->alamat_jaminan; ?></textarea>                        
                              </div>
                              <div class="col-sm-4">4. Hubungan Peminjam dan Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->status_jaminan ?>" style="width: 100%" readonly>                        
                              </div>
                            </div>

                            <p><b>C. DATA TANAH</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Hak Tanah/Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->bentuk_jaminan ?>" readonly style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. No. Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="no_jaminan" value="<?php echo $b->bukti_jaminan ?>" placeholder="" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">3. Status Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->status_jaminan ?>"  placeholder="" style="width: 100%" readonly>                        
                              </div>
                              <div class="col-sm-4">4. Tanggal Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->tgl_jaminan ?>" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Nama Pemegang Hak</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext"  value="<?php echo $b->nama_pemilik ?>" readonly style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Wilayah</div>
                              <div class="form-group col-sm-8">
                                <textarea  type="text" class="form-control-plaintext" readonly="" style="width: 100%"><?php echo $b->alamat_jaminan ?> </textarea>                         
                              </div>
                              <div class="col-sm-4">7. Tanggal dikeluarkan</div>
                              <div class="form-group col-sm-8">
                                <input type="date" class="form-control-plaintext" name="tgl_keluar_jaminan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Tanggal Jatuh Tempo Sertipikat</div>
                              <div class="form-group col-sm-8">
                                <input type="date" class="form-control-plaintext" name="tgl_jatuh_jaminan" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan" value="<?php echo $b->luas_jaminan ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Bentuk Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" value="<?php echo $b->bentuk_jaminan ?>" placeholder="" style="width: 100%" readonly>                        
                              </div>
                            </div>

                            <p><b>D. DATA BANGUNAN</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Dibangun Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun" value="<?php echo $b->tahun_bangun ?>"  style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Renovasi Terakhir Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_renovasi" value="<?php echo $b->tahun_renovasi ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">3. Umur Efektif</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="umur_efektif" value="<?php echo $b->umur_efektif ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Luas Bangunan Sesuai Fisik</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangunan_fisik" value="<?php echo $b->luas_bangunan_fisik ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan Sesuai IMB</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangunan_imb" value="<?php echo $b->luas_bangunan_imb ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Luas Tapak Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_tapak_bangunan" value="<?php echo $b->luas_tapak_bangunan ?>" placeholder="" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Dihuni Tahun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_huni" value="<?php echo $b->tahun_huni ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Jenis Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_bangunan" value="<?php echo $b->jenis_bangunan ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Penggunaan Bangunan saat ini</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="pemakai_bangunan" value="<?php echo $b->pemakai_bangunan ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Kondisi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kondisi_bangunan" value="<?php echo $b->kondisi_bangunan ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">11. Fasilitas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="fasilitas_bangunan" value="<?php echo $b->fasilitas_bangunan ?>" style="width: 100%">                        
                              </div>
                            </div>

                            <p><b>E. SARANA PELENGKAP</b></p>
                            <div class="row">
                              <div class="form-group col-sm-4">1. Taman</div>
                              <div class="form-group col-sm-8">       
                                <input type="text" class="form-control" name="fasilitas_bangunan" value="<?php echo $b->taman ?>" style="width: 100%">                  
                              </div>
                              <div class="col-sm-4">2. Halaman</div>
                              <div class="form-group col-sm-8">  
                                <input type="text" class="form-control" name="fasilitas_bangunan" value="<?php echo $b->halaman ?>" style="width: 100%">                     
                              </div>
                              <div class="col-sm-4">3. Pagar</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control" name="fasilitas_bangunan" value="<?php echo $b->pagar ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Lainnya</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="lainnya" value="<?php echo $b->lainnya ?>" style="width: 100%">                        
                              </div>
                            </div>
                            <style>
                              #myImg {
                                border-radius: 5px;
                                cursor: pointer;
                                transition: 0.3s;
                              }

                              #myImg:hover {opacity: 0.7;}
                              /* The Modal (background) */
                              .modal {
                                display: none; /* Hidden by default */
                                position: fixed; /* Stay in place */
                                z-index: 1; /* Sit on top */
                                padding-top: 100px; /* Location of the box */
                                left: 0;
                                top: 0;
                                width: 100%; /* Full width */
                                height: 100%; /* Full height */
                                overflow: auto; /* Enable scroll if needed */
                                background-color: rgb(0,0,0); /* Fallback color */
                                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                              }

                              /* Modal Content (image) */
                              .modal-content {
                                margin: auto;
                                display: block;
                                width: 80%;
                                max-width: 700px;
                              }

                              /* Add Animation */
                              .modal-content, #caption {  
                                -webkit-animation-name: zoom;
                                -webkit-animation-duration: 0.6s;
                                animation-name: zoom;
                                animation-duration: 0.6s;
                              }

                              @-webkit-keyframes zoom {
                                from {-webkit-transform:scale(0)} 
                                to {-webkit-transform:scale(1)}
                              }

                              @keyframes zoom {
                                from {transform:scale(0)} 
                                to {transform:scale(1)}
                              }

                              /* 100% Image Width on Smaller Screens */
                              @media only screen and (max-width: 700px){
                                .modal-content {
                                  width: 100%;
                                }
                              }
                            </style>
                            <p><b>F. DATA PEMBANDING</b></p>
                            <p><b>PEMBANDING 1</b></p>
                            <div class="row">
                              <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding1" value="<?php echo $b->jenis_jaminan_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding1" style="width: 100%"><?php echo "$b->alamat_jaminan_banding1"; ?></textarea>                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding1" value="<?php echo $b->luas_jaminan_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding1" value="<?php echo $b->dokumen_tanah_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding1" value="<?php echo $b->luas_jaminan_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding1" value="<?php echo $b->kontruksi_bangun_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding1" value="<?php echo $b->tahun_bangun_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding1" value="<?php echo $b->harga_penawaran_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding1" value="<?php echo $b->waktu_penjual_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding1" value="<?php echo $b->sumber_dana_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding1" value="<?php echo $b->telp_banding1 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding1" value="<?php echo $b->catatan_banding1 ?>" style="width: 100%">                        
                              </div>

                              <center><h5>LOKASI PEMBANDING 1</h5></center><br>
                              <div class="form-group">
                                  <img src="<?php echo base_url().'uploads/survei/'.$b->lokasi_banding1; ?>" type="file" class="form-control" name="lokasi_banding1" required="">
                                  
                              </div>
                              <div class="col-md-6" data-aos="zoom-in"
                                        data-aos-delay="50"
                                        data-aos-duration="1000"
                                        data-aos-mirror="true"
                                        data-aos-anchor-placement="top-center">
                                    <center><label><h1>KTP Suami</h1></label></center>
                                    <center><img data-u="image" id="myImg" class="img-fluid" style="width:50%; height:50%" src="<?php echo base_url().'uploads/survei/'.$b->lokasi_banding1;?>" alt="KTP Suami"></center>
                                  </div>
                            </div>

                            <p><b>PEMBANDING 2</b></p>
                            <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding1" value="<?php echo $b->jenis_jaminan_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding1" style="width: 100%"><?php echo "$b->alamat_jaminan_banding2"; ?></textarea>                        
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding1" value="<?php echo $b->luas_jaminan_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding1" value="<?php echo $b->dokumen_tanah_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding1" value="<?php echo $b->luas_jaminan_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding1" value="<?php echo $b->kontruksi_bangun_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding1" value="<?php echo $b->tahun_bangun_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding1" value="<?php echo $b->harga_penawaran_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding1" value="<?php echo $b->waktu_penjual_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding1" value="<?php echo $b->sumber_dana_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding1" value="<?php echo $b->telp_banding2 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding1" value="<?php echo $b->catatan_banding2 ?>" style="width: 100%">                        
                              </div>

                            <p><b>PEMBANDING 3</b></p>
                            <div class="col-sm-4">1. Jenis Jaminan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="jenis_jaminan_banding1" value="<?php echo $b->jenis_jaminan_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">2. Alamat</div>
                              <div class="form-group col-sm-8">
                                <textarea type="text" class="form-control-plaintext" name="alamat_jaminan_banding1" style="width: 100%"><?php echo "$b->alamat_jaminan_banding3"; ?></textarea>                    
                              </div>
                              <div class="col-sm-4">3. Luas Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_jaminan_banding1" value="<?php echo $b->luas_jaminan_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">4. Dokumen Tanah</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="dokumen_tanah_banding1" value="<?php echo $b->dokumen_tanah_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">5. Luas Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="luas_bangun_banding1" value="<?php echo $b->luas_jaminan_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">6. Konstruksi Bangunan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="kontruksi_bangun_banding1" value="<?php echo $b->kontruksi_bangun_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">7. Tahun dibangun</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="tahun_bangun_banding1" value="<?php echo $b->tahun_bangun_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">8. Harga Penawaran/Transaksi</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="harga_penawaran_banding1" value="<?php echo $b->harga_penawaran_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">9. Waktu Penjualan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="waktu_penjual_banding1" value="<?php echo $b->waktu_penjual_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">10. Sumber Data</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="sumber_dana_banding1" value="<?php echo $b->sumber_dana_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Telepon</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="telp_banding1" value="<?php echo $b->telp_banding3 ?>" style="width: 100%">                        
                              </div>
                              <div class="col-sm-4">Catatan</div>
                              <div class="form-group col-sm-8">
                                <input type="text" class="form-control-plaintext" name="catatan_banding1" value="<?php echo $b->catatan_banding3 ?>" style="width: 100%">                        
                              </div>

                              <center><h5>LOKASI PEMBANDING 3</h5></center><br>
                              <div class="form-group">
                                  <input type="file" class="form-control" name="lokasi_banding3" required="">
                                  <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                    JPG|JPEG|PNG Ukuran 200KB
                                  </small>
                              </div>
                            </div>

                            <center><h2>PETA LOKASI RUMAH DAN USAHA</h2></center><br>
                            
                            <center><h4>LOKASI RUMAH</h4></center><br>
                            <div class="form-group">
                                <input type="file" class="form-control" name="lokasi_rumah" required="">
                                <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                  JPG|JPEG|PNG Ukuran 200KB
                                </small>
                            </div>
                            <center><h4>LOKASI USAHA</h4></center><br>
                            <div class="form-group">
                                <input type="file" class="form-control" name="lokasi_usaha" required="">
                                <small id="passwordHelpBlock" class="form-text text-muted" style="color: red">
                                  JPG|JPEG|PNG Ukuran 200KB
                                </small>
                            </div>
                      </div>     
                        <?php endforeach; ?>
                     </form>  
                    <div id="myModal" class="modal">
                      <img class="modal-content" id="img01">
                    </div>
                    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
                          <script>
                            AOS.init();
                          </script>

                          <script>
                          // Get the modal
                          var modal = document.getElementById("myModal");

                          // Get the image and insert it inside the modal - use its "alt" text as a caption
                          var img = document.getElementById("myImg");
                          var modalImg = document.getElementById("img01");
                          img.onclick = function(){
                            modal.style.display = "block";
                            modalImg.src = this.src;
                            captionText.innerHTML = this.alt;
                          }

                          // Get the <span> element that closes the modal
                          var span = document.getElementsByClassName("close")[0];

                          // When the user clicks on <span> (x), close the modal
                          modal.onclick = function() { 
                            modal.style.display = "none";
                          }
                          </script>
                 </div>
            </div>
       </div>
        