<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------


// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('times', '', 10.7);
// set some text to print

$biodata = <<<EOD
<br><br>
<table border="0">
<tr>
	<td width="8%">
		Perihal : 
	</td>
	<td>
		<b> Permohonan Pinjaman</b>
	</td>
	<td width="30%">

	</td>
	<td width="40%">
		Batam,.............................
	</td>

</tr>
<tr>
	<td>
	
	</td>

	<td>
		 <b><u>Dana Bergulir</b></u>
	</td>
	<td>

	</td>
	<td>
		Kepada, Yth:
		<br>
		Kepala Badan Pengelolaan Keuangan dan Aset
		<br>
		Daerah Kota Batam
		<br>
		c/q Kepala UPT-Pengelolaan Dana Bergulir
		<br>
		di-
		<br>
			<b><u>B a t a m</b></u>
	</td>
	
</tr>
<tr>
	<td>
	
	</td>

	<td>
		
	</td>
	<td>

	</td>
	<td>

	</td>
	
</tr>
</table>
<br>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 12);
foreach ($data as $row) {
$isisurat= <<<EOD

Dengan Hormat,
<br>

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$isisurat = <<<EOD

Sehubungan dengan adanya kegiatan Penyaluran Pinjaman Dana Bergulir Pemerintah Kota
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'34','',$isisurat,0,1,0,true,'J',true);


// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$isisurat = <<<EOD

Batam kepada Koperasi, Usaha Mikro dan Lembaga Keuangan Mikro, maka dengan ini kami:
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'J',true);


// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$isisurat2 = <<<EOD
<br><br>
<table border="0">
<tr>
	<td width="28%">
		Nama Usaha
	</td>
	<td width="2%">
		:
	</td>
	<td width="70%">
		$row->nama_usaha
	</td>
</tr>

<tr>
	<td >
		Pimpinan/ Pemilik
	</td>
	<td>
		:
	</td>
	<td>
		$row->nama_lengkap
	</td>
</tr>

<tr>
	<td>
		Alamat Usaha
	</td>
	<td>
		:
	</td>
	<td>
		$row->alamat_usaha
	</td>
</tr>

<tr>
	<td>
		Telepon/ HP
	</td>
	<td>
		:
	</td>
	<td>
		$row->hp_usaha
	</td>
</tr>

<tr>
	<td>
		Milik Sendiri/ Sewa
	</td>
	<td>
		:
	</td>
	<td>
		$row->status_usaha
	</td>
</tr>

<tr>
	<td>
		Alamat Rumah
	</td>
	<td>
		:
	</td>
	<td>
		$row->alamat_domisili
	</td>
</tr>

<tr>
	<td>
		Telepon/ HP
	</td>
	<td>
		:
	</td>
	<td>
		$row->no_hp
	</td>
</tr>

<tr>
	<td>
		Milik Sendiri/ Sewa
	</td>
	<td>
		:
	</td>
	<td>
		$row->status_rumah
	</td>
</tr>

<tr>
	<td>
		No. KTP
	</td>
	<td>
		:
	</td>
	<td>
		$row->no_ktp
	</td>
</tr>

<tr>
	<td>
		Usaha yang dijalankan
	</td>
	<td>
		:
	</td>
	<td>
		$row->bidang_usaha
	</td>
</tr>
</table>

EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat2,0,1,0,true,'J',true);


function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

// set font
$pdf->SetFont('times', '', 12);
foreach ($data as $row) { 		
	
$isisurat= <<<EOD
<br>
<br>
Mengajukan permohonan pinjaman dana bergulir sebesar Rp.$row->nominal yang mana dana tersebut

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'25','',$isisurat,0,1,0,true,'Justify',true);

// set font
$pdf->SetFont('times', '', 12);
foreach ($data as $row) { 		
	
$isisurat= <<<EOD
akan kami gunakan untuk keperluan : <b>$row->keperluan

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'L',true);


$pdf->SetFont('times', '', 12);
foreach ($data as $row) { 		
	
$isisurat= <<<EOD
<br><br>
	Sebagai bahan pertimbangan Bapak, kami lampirkan persyaratan dan selanjutnya kami akan memenuhi

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'Justify',true);

// set font
$pdf->SetFont('times', '', 12);
foreach ($data as $row) { 		
	
$isisurat= <<<EOD
	memenuhi aturan-aturan yang berlaku serta besedia dikenakan sanksi apabila tidak mematuhi ketentuan 
	yang telah 
	ditetapkan.
	<br>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'Justify',true);

// set font
$pdf->SetFont('times', '', 12);
foreach ($data as $row) { 		
	
$isisurat= <<<EOD
	Demikian Permohonan ini kami sampaikan, atas pertimbangan Bapak kami ucapkan terima kasih.
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isisurat,0,1,0,true,'Justify',true);




// // set font
$pdf->SetFont('helvetica', '', 10);

// // --- Scaling ---------------------------------------------

// // Start Transformation
$pdf->ScaleXY(103,65,-5000);
$pdf->Text(140, 70 , 'Hormat Kami');
$pdf->Text(128.5, 110 , '(.............................................)');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpermohonanpinjaman.pdf','I');

//============================================================+
// END OF FILE
//============================================================+
?>