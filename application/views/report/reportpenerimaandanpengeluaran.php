<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(20, 20, 20, true); // set the margins 

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$pdf->ScaleXY(109, 65,-1500);
// set font
$pdf->SetFont('times', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
foreach ($data as $row) {
$SetTitle = <<<EOD
LAPORAN PENERIMAAN DAN PENGELUARAN
PER 3 (TIGA) BULAN TERAKHIR

EOD;
}
// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<br>
<div>
<table style="border: 1px solid black;">
<tr>
	<th width="5%"><b> NO</b> <br> </th>
	<th width="25%"><b>URAIAN</b></th>
	<th width="15%"><b>BULAN <i>$row->bulan1</i></b></th>
	<th width="16%"><b>BULAN <i>$row->bulan2</i></b></th>
	<th width="20%"><b>BULAN <i>$row->bulan3</i></b></th>
	
</tr>
<tr>
	<td width="5%"> 1. </td>
	<td><u>PENDAPATAN</u><br></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>PENDAPATAN USAHA<br></td>
	<td>Rp. $row->pendusaha1</td>
	<td>Rp. $row->pendusaha2</td>
	<td>Rp. $row->pendusaha3</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>PENDAPATAN LAIN-LAIN<br></td>
	<td>Rp. $row->pendlain1</td>
	<td>Rp. $row->pendlain2</td>
	<td>Rp. $row->pendlain3</td>
	
</tr>
<tr>
	<td width="5%"> </td>
	<td><b>TOTAL PENDAPATAN</b><br></td>
	<td>Rp. $row->totalpend1</td>
	<td>Rp. $row->totalpend2</td>
	<td>Rp. $row->totalpend3</td>
</tr>

<tr>
	<td width="5%">2.</td>
	<td><u>PENGELUARAN</u><br></td>
	<td></td>
	<td></td>
	<td></td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>A. Pembelian Bahan Baku <br></td>
	<td>Rp. $row->pengbahan1</td>
	<td>Rp. $row->pengbahan2</td>
	<td>Rp. $row->pengbahan3</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>B. Tenaga Kerja <br></td>
	<td>Rp. $row->pengtenaga1</td>
	<td>Rp. $row->pengtenaga2</td>
	<td>Rp. $row->pengtenaga3</td>
	
</tr>

<tr>
	<td width="5%"> </td>
	<td>C. Listrik, Air, Telp/HP, DLL <br></td>
	<td>Rp. $row->penglistrik1</td>
	<td>Rp. $row->penglistrik2</td>
	<td>Rp. $row->penglistrik3</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>D. Biaya Administrasi <br></td>
	<td>Rp. $row->pengadm1</td>
	<td>Rp. $row->pengadm2</td>
	<td>Rp. $row->pengadm3</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>E. Transportasi<br></td>
	<td>Rp. $row->pengtran1</td>
	<td>Rp. $row->pengtran2</td>
	<td>Rp. $row->pengtran3</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>F. Pengeluaran lain-lain<br></td>
	<td>Rp. $row->penglain1</td>
	<td>Rp. $row->penglain2</td>
	<td>Rp. $row->penglain3</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td><b>TOTAL PENGELUARAN</b><br></td>
	<td><b>Rp. $row->totalpeng1</b></td>
	<td><b>Rp. $row->totalpeng2</b></td>
	<td><b>Rp. $row->totalpeng3</b></td>
</tr>

<tr>
	<td width="5%">3.</td>
	<td>SURPLUS/ DEFISIT (1-2)<br></td>
	<td><b>Rp. $row->def1</b></td>
	<td><b>Rp. $row->def2</b></td>
	<td><b>Rp. $row->def3</b></td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(200,0,'28','',$isibiodata,0,1,0,true,'L',true);





$pdf->ScaleXY(109, 65,-1200);
$pdf->Text(127, 70 , 'Batam, .............................');
$pdf->SetFont('times', 'B', 9);
$pdf->Text(140, 75 , 'PEMOHON');
$pdf->SetFont('times', '', 9);
$pdf->Text(125, 100 , '( ....................................................... )');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpenerimaandanpengeluaran.pdf','I');

//============================================================+
// END OF FILE
//============================================================+