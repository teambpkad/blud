<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetMargins(15, 15, 15, true); // set the margins 
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
SURAT KUASA DAN PERSETUJUAN
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 11);
// set some text to print

$biodata = <<<EOD
<br><br><br>
Yang bertanda tangan dibawah ini
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	1.<td width="35%">Nama</td>
	<td width="65%">: $row->nama_lengkap</td>
</tr>

<tr>
	<td>NIK </td>
	<td>: $row->no_ktp</td>
</tr>

<tr>
	<td>Tempat Tanggal Lahir</td>
	<td>: $row->tempat_lahir , $row->tgl_lahir</td>
</tr>

<tr>
	<td>Alamat</td>
	<td>: $row->alamat_domisili</td>
</tr>
<tr>
	<td colspan="2">
	Selanjutnya disebut sebagai <b>Pihak Pertama</b>
	</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	2.<td width="35%">Nama</td>
	<td width="65%">: $row->nm_kuasa</td>
</tr>

<tr>
	<td>NIK </td>
	<td>: $row->no_ktp_kuasa</td>
</tr>

<tr>
	<td>Tempat Tanggal Lahir</td>
	<td>: $row->tempat_lahir_kuasa , $row->tgl_lahir_kuasa</td>
</tr>

<tr>
	<td>Alamat</td>
	<td>: $row->alamat_kuasa</td>
</tr>

<tr>
	<td colspan="2">
	Selanjutnya disebut sebagai <b>Pihak Kedua</b>
	</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', 'J', 11);
// set some text to print

$penutup = <<<EOD
<p>
Dengan ini menyatakan bahwa <b>Pihak Pertama</b> memberikan kuasa dan Persetujuan kepada <b>Pihak Kedua</b> menggunakan jaminan dibawah ini :<br>
</p>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);
// set font
$pdf->SetFont('times', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<table>
<tr>
	<td width="35%">Bentuk Jaminan</td>
	<td width="65%">: $row->bentuk_jaminan</td>
</tr>

<tr>
	<td>Nomor </td>
	<td>: $row->nilai_jaminan</td>
</tr>

<tr>
	<td>Atas Nama</td>
	<td>: $row->nama_pemilik</td>
</tr>
<tr>
	<td colspan="2">Selanjutnya disebut sebagai <b>Objek Jaminan</b></td>
<tr>
</table>
<br>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', 'J', 10);
// set some text to print

$penutup = <<<EOD
<p>
Untuk digunakan sebagai Jaminan Pinjaman Dana Bergulir pada Unit Pelaksana Teknis Pengelolaan Dana Bergulir Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.<br>Apabila Pihak Kedua yang menerima kuasa dan persetujua dari Pihak Pertama tidak melakukan kewajiban pembayaran angsuran pokok dan jasa pinjaman selama 3 (tiga) bulan, maka Pihak Unit Pelaksana Teknis Pengelolaan Dana Bergulir Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam berhak untuk menyita dan menjual barang yang dijaminkan tersebut.<br>
Pihak Pertama dengan ini menyatakan telah mengetahui dengan benar segala resiko yang timbul sehubungan dengan penjaminan tersebut tidak akan melakukan tuntutan berupa apapun kepada Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam bilamana terjadi penyitaan terhadap objek jaminan sebagai akibat tunggakan pembayaran.<br>
Demikian Surat Kuasa dan Persetujuan ini dibuat dengan sebenarnya tanpa ada paksaan dari pihak manapun dan untuk dapat dipergunakan sebagaimana mestinya.
</p>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);


// set font
$pdf->SetFont('helvetica', '', 9);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->ScaleXY(103, 65,-3400);

$pdf->ScaleXY(103, 65,-100);
$pdf->SetFont('times', '', 9);
$pdf->Text(34, 75 , 'Pihak Pertama');
$pdf->SetFont('times', '', 9);
$pdf->Text(24, 90 , 'Materai 6000.-');
$pdf->SetFont('times', '', 9);
$pdf->Text(20, 110 , '(.......................................................)');

$pdf->SetFont('times', '', 9);
$pdf->Text(95, 75 , 'Suami Istri');
$pdf->Text(93, 80 , 'Pihak Pertama');
$pdf->SetFont('times', '', 9);
$pdf->Text(80, 110 , '(.......................................................)');



$pdf->Text(139, 70 , 'Batam, ..............................................');
$pdf->SetFont('times', '', 9);
$pdf->Text(154, 75 , 'Pihak Kedua');
$pdf->SetFont('times', '', 9);
$pdf->Text(140, 110 , '(......................................................)');


$pdf->ScaleXY(103, 65,-1500);
$pdf->SetFont('times', '', 9);
$pdf->Text(34, 75 , 'Saksi Pertama');
$pdf->SetFont('times', '', 9);
$pdf->Text(21, 100 , '(......................................................)');


$pdf->SetFont('times', '', 9);
$pdf->Text(154, 75 , 'Saksi Kedua');
$pdf->SetFont('times', '', 9);
$pdf->Text(138, 100 , '(......................................................)');

$pdf->ScaleXY(103, 65,-1000);
$pdf->SetFont('times', 'BU', 9);
$pdf->Text(21, 75 , 'Catatan:');
$pdf->SetFont('times', '', 9);
$pdf->Text(21, 80 , '- Melampirkan Foto Copy KTP Pihak Pertama (Pemberi Kuasa) dan KTP saksi');

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportkuasapersetujuan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+