<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
SURAT PERSETUJUAN & KUASA SUAMI / ISTRI
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 12);
// set some text to print

$biodata = <<<EOD
<br><br><br>
Yang bertanda tangan dibawah ini
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Nama</td>
	<td width="65%">: $row->nama_lengkap</td>
</tr>

<tr>
	<td>NIK </td>
	<td>: $row->no_ktp</td>
</tr>

<tr>
	<td>Alamat</td>
	<td>: $row->alamat_domisili</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$penutup = <<<EOD

Selaku Suami/ Istri dari
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Nama</td>
	<td width="65%">: $row->nm_kuasa</td>
</tr>

<tr>
	<td>NIK </td>
	<td>: $row->no_ktp_kuasa</td>
</tr>

<tr>
	<td>Alamat</td>
	<td>: $row->alamat_kuasa</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$penutup = <<<EOD

<p>
Dengan ini memberikan persetujuan dan kuasa kepada Suami/ Istri untuk :
<br>
<br>
Mengajukan, memperoleh fasilitas pinjaman dana bergulir dari Unit Pelaksana Teknis Pengelolaan Dana Bergulir Badan Pengelolaan Keuangan Dan Aset Daerah Kota Batam, menghadap, memberikan dan menandatangani segala dokumen yang berhubungan dengan fasilitas pinjaman tersebut.
<br>
<br>
Demikian Surat Persetujuan ini dibuat, untuk dapat dipergunakan sebagaimana mestinya.
</p>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);


// set font
$pdf->SetFont('helvetica', '', 10);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->ScaleXY(103, 65,-2500);
$pdf->Text(123, 70 , 'Batam, ..................................................');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 75 , 'Yang Memberi Persetujuan');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 80 , 'Materai 6000.-');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 110 , '.......................................................');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportkuasasuamiistri.pdf','I');

//============================================================+
// END OF FILE
//============================================================+