<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
LAPORAN HASIL SURVEY LAPANGAN
TIM PENILAI PINJAMAN DANA BERGULIR
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);

// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<br><br>
<table border="1">
<tr>
	<td width="35%"> <U>I. DATA UMUM</U></td>
	<td width="2%"> </td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> NAMA UMK/ KOPERASI/ LKM</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> NAMA PEMILIK/ KETUA</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> ALAMAT USAHA</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> JENIS USAHA</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> TELP/ HP</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>


</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td width="35%"> <U>II.KLASIFIKASI USAHA</U></td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td width="35%"> ASSET (MODAL KERJA)</td>
	<td width="2%"> :</td>
	<td width="63%"></td>
</tr>

<tr>
	<td width="35%"> OMSET/ TAHUN</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td width="35%"> <U>III. JAMINAN</U></td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td width="35%"> BENTUK JAMINAN</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->bentuk_jaminan</td>
</tr>

<tr>
	<td width="35%"> NO. SURAT JAMINAN</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td width="35%"> NAMA PEMILIK</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->nama_pemilik </td>
</tr>

<tr>
	<td width="35%"> ALAMAT JAMINAN</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->alamat_jaminan</td>
</tr>

<tr>
	<td width="35%"> NILAI JAMINAN</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->nilai_jaminan</td>
</tr>

<tr>
	<td width="35%"> NILAI JUAL OBJEK PAJAK (NJOP)</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td width="35%"> HARGA PASAR</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td width="35%"> NILAI TAKSIRAN JAMINAN</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td width="35%"> <U>IV. KEMAMPUAN KEUANGAN</U></td>
	<td width="2%"> </td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> LABA USAHA/ SHU</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> PENDAPATAN SELAIN USAHA</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> HUTANG/ KEWAJIBAN YANG HARUS DIBAYAR</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> BIAYA KELUARGA PER BULAN</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> KESANGGUPAN MEMBAYAR ANGSURAN PER BULAN</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1" style="height:'50%'">
<tr>
	<td> <U>V.REKOMENDASI TIM PENILAI</U></td>
</tr>
<tr>
	<td> 1. </td>
</tr>
<tr>
	<td> 2. </td>
</tr>
<tr>
	<td> 3. </td>
</tr>
<tr>
	<td> 4. </td>
</tr>
<tr>
	<td> 5. </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table style="border: 1px solid black;">
<tr>
	<td width="50%" style="text-align:center"><br><br> KA. UPT-PENGELOLAAN DANA BERGULIR</td>
	<td style="text-align:center"><br><br>BATAM,................................</td>

</tr>
<tr>
	<td style="text-align:center"><br><br><br><br><br><br><u><center>ZULFAHRI, SE</u></td>
	<td style="text-align:center">Tim Penilai <br><br><br><br> 1. ..............................</td>
</tr>
<tr>
	<td style="text-align:center">NIP.19781119 200212 1 003<br></td>
	<td style="text-align:center"><br><br> 2. ..............................<br><br> </td>
</tr>
</table>

</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpernyataan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+