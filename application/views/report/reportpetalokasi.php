<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'BU', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
PETA LOKASI RUMAH DAN USAHA
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);

// set font


$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<br><br>
<table border="1">
<tr>
	<td width="35%"> NAMA UMK/ KOPERASI/ LKM</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->nama_usaha</td>
</tr>
<tr>
	<td width="35%"> NAMA PEMILIK/ KETUA</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->nama_lengkap</td>
</tr>
<tr>
	<td width="35%"> ALAMAT RUMAH</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->alamat_domisili</td>
</tr>
<tr>
	<td width="35%"> ALAMAT USAHA</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->alamat_usaha</td>
</tr>
<tr>
	<td colspan="3" style="text-align:center;background-color:#d6d6d6">
		<br><br><B>LOKASI RUMAH</B><br>
	</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);
$pdf->Image('uploads/file/'.$row->kk, '60', '80', 100, 70, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

$pdf->SetXY(110, 145);
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td colspan="3" style="text-align:center">
		<br><br>
			
			$x

		<br>
	</td>
</tr>
<tr>
	<td border="1" colspan="3" style="text-align:center;background-color:#d6d6d6">
		<br><br><B>LOKASI USAHA</B><br>
	</td>
</tr>
</table>
</div>

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);
$pdf->Image('uploads/file/'.$row->ktp_istri, '60', '180', 100, 70, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

// ---------------------------------------------------------

$pdf->SetFont('helvetica', '', 10);

// --- Scaling ---------------------------------------------

// Start Transformation

$pdf->ScaleXY(103, 65,-6100);
$pdf->Text(123, 70 , 'Batam, ..................................................');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 75 , 'Petugas Penilai');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 95 , '.......................................................');
$pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);
//Close and output PDF document
ob_clean();
$pdf->Output('reportpernyataan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+