<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
UNIT PELAKSANA TEKNIS PENGELOLAAN DANA BERGULIR
BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH KOTA BATAM
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);

// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<br><br>
<table border="1">
<tr>
	<td colspan="3" style="text-align:center"><b>LAPORAN PENILAI (SURVEY LAPANGAN) USAHA MIKRO</b></td>
</tr>
<tr>
	<td width="35%"> Tanggal Kunjungan</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Daerah Kunjungan</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Tanggal Laporan</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Nomor</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Nama Penilai</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>


</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td colspan="6" style="text-align:center"> <b>DATA CALON PENERIMA DANA BERGULIR</b></td>
</tr>

<tr>
	<td width="20%"> Nama Lengkap</td>
	<td width="2%"> :</td>
	<td width="30%"> </td>
	<td width="15%"> RT/ RW</td>
	<td width="2%"> :</td>
	<td width="31%"> </td>
</tr>
<tr>
	<td width="20%"> Bidang Usaha</td>
	<td width="2%"> :</td>
	<td width="30%"> </td>
	<td width="15%"> Kelurahan</td>
	<td width="2%"> :</td>
	<td width="31%"> </td>
</tr>
<tr>
	<td width="20%"> Alamat Usaha</td>
	<td width="2%"> :</td>
	<td width="30%"> </td>
	<td width="15%"> Kecamatan</td>
	<td width="2%"> :</td>
	<td width="31%"> </td>
</tr>
<tr>
	<td width="20%"> </td>
	<td width="2%"> </td>
	<td width="30%"> </td>
	<td width="15%"> Telp/ Hp</td>
	<td width="2%"> :</td>
	<td width="31%"> </td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td colspan="2" style="text-align:center"> <b>KONDISI RUMAH TEMPAT/ USAHA</b></td>
</tr>

<tr>
	<td width="35%"> Rumah Yang Ditempati saat ini merupakan</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->status_rumah</td>
</tr>

<tr>
	<td width="35%"> Lokasi Tempat Usaha</td>
	<td width="2%"> :</td>
	<td width="63%"> $row->alamat_usaha</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td colspan="3" style="text-align:center"> <b>KONDISI USAHA</b></td>
	
</tr>
<tr>
	<td width="35%"> Lama Usaha yang dijalankan saat ini</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Jumlah Karyawan saat ini</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Omzet Usaha saat ini</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Biaya Operasional</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Biaya Keluarga</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Administrasi Pembukuan</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Etika Usaha</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td colspan="3" style="text-align:center"> <b>PENGAJUAN PINJAMAN</b></td>
	
</tr>
<tr>
	<td width="35%"> Pengajuan Pinjaman</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Kegunaan Dana</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>

<tr>
	<td colspan="3" style="text-align:center"> <b>KESANGGUPAN MEMBAYAR</b></td>
	
</tr>
<tr>
	<td width="35%"> Kesanggupan Membayar cicilan setiap bulan</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Sumber pengembalian selain usaha</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table border="1">
<tr>
	<td colspan="3" style="text-align:center"> <b>CATATAN PENTING</b></td>
</tr>
<tr>
	<td width="100%" colspan="3" style="text-align:left"> Hal hal lain yang dapat dijadikan pertimbangan dalam permohonan bersangkutan :</td>
</tr>
<tr>
	<td width="100%" colspan="3" style="text-align:left">.............................................................................................................................................................................</td>
</tr>
<tr>
	<td width="35%"> Rekomendasi Penilai</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
<tr>
	<td width="35%"> Jaminan sebagai moral hazard</td>
	<td width="2%"> :</td>
	<td width="63%"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('helvetica', '', 10);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table style="border: 1px solid black;">
<tr>
	<td style="text-align:center;font-size:11px"><br><br> Kepala. UPT-Pengelolaan Dana Bergulir Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam</td>
	<td style="text-align:center;font-size:11px"><br><br> Penilai</td>
	<td style="text-align:center;font-size:11px"><br><br> Calon Mitra Binaan</td>

</tr>
<tr>
	<td style="text-align:center"><br><br><br><br><br><br><u><center>ZULFAHRI, SE</u></td>
	<td></td>
	<td style="text-align:center"></td>
</tr>
<tr>
	<td style="text-align:center">NIP.19781119 200212 1 003<br></td>
	<td style="text-align:center">(............................................)</td>
	<td style="text-align:center">(............................................)<br> </td>
</tr>
</table>

</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpernyataan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+