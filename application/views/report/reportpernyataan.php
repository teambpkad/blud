<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'BU', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
SURAT PERNYATAAN
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 12);
// set some text to print

$biodata = <<<EOD
<br><br>
Saya yang bertanda tangan dibawah ini :
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Nama</td>
	<td width="65%">: $row->nama_lengkap</td>
</tr>

<tr>
	<td>NIK </td>
	<td>: $row->no_ktp</td>
</tr>

<tr>
	<td>Tempat Tanggal Lahir</td>
	<td>: $row->tempat_lahir , $row->tgl_lahir</td>
</tr>

<tr>
	<td width="35%">Nama Usaha</td>
	<td width="65%">: $row->nama_usaha</td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', 'J', 12);
// set some text to print

$penutup = <<<EOD

<p>Dengan ini menyatakan bahwa sehubungan dengan permohonan pinjaman dana bergulir yang diajukan, <b><i><u>saya tidak akan memberikan hadiah baik berupa barang atau uang</u></b></i> kepada Pegawai Unit Pelaksana Teknis Pengelolaan Dana Bergulir Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.

<br>
<br>
Demikian Surat Pernyataan ini saya buat dengan sebenarnya tanpa ada paksaan dari pihak manapun dan apabila dikemudian hari ternyata pernyataan saya ini tidak benar/palsu, maka saya ebrsedia dikenai sanksi sesuai dengan ketentuan yang berlaku
</p>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);





// set font
$pdf->SetFont('helvetica', '', 10);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->ScaleXY(103, 65,-1500);
$pdf->Text(123, 70 , 'Batam, ..................................................');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 75 , 'Yang Membuat Pernyataan');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 80 , 'Materai 6000.-');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 110 , '.......................................................');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpernyataan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+