<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'BU', 14);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
DATA CALON MITRA BINAAN
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', 'BU', 12);
// set some text to print

$biodata = <<<EOD
<br><br>
I. BIODATA PEMOHON 
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);

foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Nama</td>
	<td width="65%">: $row->nama_lengkap</td>
</tr>

<tr>
	<td>No KTP </td>
	<td>: $row->no_ktp</td>
</tr>

<tr>
	<td>Status </td>
	<td>: $row->status</td>
</tr>

<tr>
	<td>Alamat Sesuai KTP </td>
	<td>: $row->alamat_ktp</td>
</tr>

<tr>
	<td>Alamat Sekarang </td>
	<td>: $row->alamat_domisili</td>
</tr>

<tr>
	<td>Telp/Hp </td>
	<td>: $row->no_hp</td>
</tr>

<tr>
	<td>Tempat Tanggal Lahir</td>
	<td>: $row->tempat_lahir , $row->tgl_lahir</td>
</tr>

<tr>
	<td>Jenis Kelamin</td>
	<td>: $row->jk</td>
</tr>

<tr>
	<td>Pendidikan Terakhir</td>
	<td>: $row->pendidikan</td>
</tr>

<tr>
	<td>Nama Ibu Kandung</td>
	<td>: $row->nama_ibu</td>
</tr>

<tr>
	<td>Kerabat Yg Tidak Tinggal Serumah</td>
	<td>: $row->nama_kerabat</td>
</tr>

<tr>
	<td>Hp/ Telp Kerabat</td>
	<td>: $row->hp_kerabat</td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', 'BU', 12);
// set some text to print

$usaha = <<<EOD
<br><br>
II. DATA USAHA KONDISI SEKARANG 
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$usaha,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isiusaha = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Nama Usaha</td>
	<td width="65%">: $row->nama_usaha</td>
</tr>

<tr>
	<td width="35%">Jumlah Karyawan</td>
	<td width="65%">: $row->jmlh_karyawan</td>
</tr>

<tr>
	<td width="35%">Alamat Usaha</td>
	<td width="65%">: $row->alamat_usaha</td>
</tr>

<tr>
	<td width="35%">Bidang Usaha</td>
	<td width="65%">: $row->bidang_usaha</td>
</tr>

<tr>
	<td width="35%">Mulai Usaha</td>
	<td width="65%">: $row->mulai_usaha</td>
</tr>

<tr>
	<td width="35%">Telp/Hp Usaha</td>
	<td width="65%">: $row->hp_usaha</td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isiusaha,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', 'BU', 12);
// set some text to print

$jaminan = <<<EOD
<br><br>
III. JAMINAN 
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$jaminan,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isijaminan = <<<EOD
<div>
<table>
<tr>
	<td width="35%">Bentuk/ Jenis Jaminan</td>
	<td width="65%">: $row->bentuk_jaminan</td>
</tr>

<tr>
	<td width="35%">Tanda/ Bukti Kepemilikan</td>
	<td width="65%">: $row->bukti_jaminan</td>
</tr>

<tr>
	<td width="35%">Tanggal</td>
	<td width="65%">: $row->tgl_jaminan</td>
</tr>

<tr>
	<td width="35%">Nama Pemilik</td>
	<td width="65%">: $row->nama_pemilik</td>
</tr>

<tr>
	<td width="35%">Nilai Jaminan</td>
	<td width="65%">: $row->nilai_jaminan</td>
</tr>

<tr>
	<td width="35%">Alamat</td>
	<td width="65%">: $row->alamat_jaminan</td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isijaminan,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 12);
// set some text to print

$penutup = <<<EOD
<br><br>
<p>Demikian Keterangan ini disampaikan dengan sebenar-benarnya dan untuk dapat dipergunakan sebagaimana mestinya.

</p>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$penutup,0,1,0,true,'J',true);





// set font
$pdf->SetFont('helvetica', '', 8);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->StartTransform();
// Scale by 150% centered by (50,80) which is the lower left corner of the rectangle
$pdf->ScaleXY(111, 455,-1500);
$pdf->Rect(60, 65, 20, 30, 'D');
$pdf->Text(63, 70 , 'Pas Photo');
$pdf->Text(66, 75, '3x4');
$pdf->Text(66, 80, 'Istri');
// Stop Transformation
$pdf->StopTransform();


// Start Transformation
$pdf->StartTransform();
// Scale by 150% centered by (50,80) which is the lower left corner of the rectangle
$pdf->ScaleXY(111, 200,-1500);
$pdf->Rect(60, 65, 20, 30, 'D');
$pdf->Text(63, 70 , 'Pas Photo');
$pdf->Text(66, 75, '3x4');
$pdf->Text(66, 80, 'Suami');
// Stop Transformation
$pdf->StopTransform();

$pdf->ScaleXY(111, 65,-1500);
$pdf->Text(123, 70 , 'Batam, ..................................................');
$pdf->SetFont('times', 'B', 9);
$pdf->Text(140, 75 , 'PEMOHON');
$pdf->SetFont('times', '', 9);
$pdf->Text(125, 100 , '( ....................................................... )');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportdatamurni.pdf','I');

//============================================================+
// END OF FILE
//============================================================+