<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'BU', 12);

// add a page
$pdf->AddPage();

// set some text to print
$SetTitle = <<<EOD
FORM PENILAIAN JAMINAN
EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 9);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>A. DATA PETUGAS PENILAIAN/ SURVEYOR</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Nama Petugas Penilai/ Surveyor</td>
	<td width="65%" border="1"> $row->nama_survei</td>
</tr>

<tr>
	<td>2. Tujuan Penilaian </td>
	<td border="1"> $row->keperluan</td>
</tr>

<tr>
	<td>3. Tanggal Penilaian</td>
	<td border="1"> $row->tempat_lahir , $row->tgl_lahir</td>
</tr>

<tr>
	<td width="35%">4. Nama Usaha</td>
	<td width="65%" border="1"> $row->tgl_survei</td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>B. DATA CALON PEMINJAM</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Nama Calon Peminjam</td>
	<td width="65%" border="1"> $row->nama_lengkap</td>
</tr>

<tr>
	<td>2. Alamat Peminjam</td>
	<td border="1"> $row->alamat_domisili</td>
</tr>

<tr>
	<td>3. Alamat Jaminan</td>
	<td border="1"> $row->alamat_jaminan</td>
</tr>

<tr>
	<td width="35%">4. Hubungan Peminjam dan Jaminan</td>
	<td width="65%" border="1"> $row->status_jaminan</td>
</tr>

</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>C. DATA TANAH</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Hak Tanah/ Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. No Sertifikat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Status Sertifikat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Tanggal Sertifikat</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">5. Nama Pemegang Hak</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">6. Wilayah</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">7. Tanggal dikeluarkan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">8. Tanggal Jatuh Tempo Sertifikat</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">9. Luas Tanah</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">10. Bentuk Tanah</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>D. DATA BANGUNAN</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Dibangun Tahun</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. Renovasi Terakhir Tahun</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Umur Efektif</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Luas Bangunan Sesuai Fisik</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">5. Luas Bangunan Sesuai IMB</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">6. Dihuni Tahun</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">7. Jenis Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">8. Penggunaan Bangunan Saat ini</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">9. Kondisi Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">10. Fasilitas Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>E. SARANA PELENGKAP</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Hak Tanah/ Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. No Sertifikat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Status Sertifikat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Tanggal Sertifikat</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);




// Page 2

// add a page
$pdf->AddPage();

// set font
$pdf->SetFont('times', '', 9);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>F. DATA PEMBANDING</b><br>
<b>PEMBANDING 1</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Jenis Jaminan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. Alamat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Luas Tanah</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Dokumen Tanah</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">5. Luas Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">6. Konstruksi Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">7. Tahun Dibangun</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">8. Harga Penawaran/ Transaksi</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">9. Waktu Penjualan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">10. Sumber Data</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Telp/Hp</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Catatan</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>PEMBANDING 2</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Jenis Jaminan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. Alamat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Luas Tanah</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Dokumen Tanah</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">5. Luas Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">6. Konstruksi Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">7. Tahun Dibangun</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">8. Harga Penawaran/ Transaksi</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">9. Waktu Penjualan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">10. Sumber Data</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Telp/Hp</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Catatan</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);


// set font
$pdf->SetFont('times', '', 10);
// set some text to print

$biodata = <<<EOD
<br><br>
<b>PEMBANDING 3</b>
EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<div>
<table>
<tr>
	<td width="35%">1. Jenis Jaminan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td>2. Alamat</td>
	<td border="1"> </td>
</tr>

<tr>
	<td>3. Luas Tanah</td>
	<td border="1"> </td>
</tr>

<tr>
	<td width="35%">4. Dokumen Tanah</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">5. Luas Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">6. Konstruksi Bangunan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">7. Tahun Dibangun</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">8. Harga Penawaran/ Transaksi</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">9. Waktu Penjualan</td>
	<td width="65%" border="1"> </td>
</tr>

<tr>
	<td width="35%">10. Sumber Data</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Telp/Hp</td>
	<td width="65%" border="1"> </td>
</tr>
<tr>
	<td width="35%"><span style="color:white">......</span>Catatan</td>
	<td width="65%" border="1"> </td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,23,'',$isibiodata,0,1,0,true,'L',true);





$pdf->SetFont('helvetica', '', 10);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->ScaleXY(103, 65,-5900);
$pdf->Text(123, 70 , 'Batam, ..................................................');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 75 , 'Petugas Penilai');
$pdf->SetFont('times', '', 10);
$pdf->Text(123, 100 , '.......................................................');
$pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportpernyataan.pdf','I');

//============================================================+
// END OF FILE
//============================================================+