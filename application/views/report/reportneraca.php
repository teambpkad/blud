<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'B', 12);

// add a page
$pdf->AddPage();

// set some text to print
foreach ($data as $row) {
$SetTitle = <<<EOD
USAHA MIKRO $row->nama_usaha / $row->nama_lengkap

NERACA 

PER $row->bulan

EOD;
}
// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', '', 11);
foreach ($data as $row) {
$isibiodata = <<<EOD
<br>
<div>
<table style="border: 1px solid black;">
<tr>
	<th width="5%"><b> NO</b> <br> </th>
	<th><b>PERKIRAAN</b></th>
	<th><b>JUMLAH</b></th>
	<th width="5%"><b>NO</b></th>
	<th><b>PERKIRAAN</b></th>
	<th><b>JUMLAH</b></th>
</tr>
<tr>
	<td width="5%"> A </td>
	<td>Aktiva Lancar<br></td>
	<td></td>
	<td> C </td>
	<td> Hutang</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>1. Kas <br></td>
	<td>Rp. $row->kas</td>
	<td> </td>
	<td>1. Hutang Usaha</td>
	<td>Rp. $row->htgusaha</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>2. Bank <br></td>
	<td>Rp. $row->bank</td>
	<td></td>
	<td><b>Jumlah Hutang</b></td>
	<td>Rp. $row->jmlhhutang</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>3. Piutang <br></td>
	<td>Rp. $row->piutang</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>4. Persediaan <br> </td>
	<td>Rp. $row->persediaan</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td><b>Jumlah Aktiva Lancar</b><br></td>
	<td>Rp. $row->jmlhaktiva_lancar</td>
</tr>

<tr>
	<td width="5%"> B </td>
	<td>Aktiva Tetap<br></td>
	<td></td>
	<td> D </td>
	<td> Modal</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>1. Tanah <br></td>
	<td>Rp. $row->tanah</td>
	<td> </td>
	<td>1. Modal Usaha</td>
	<td>Rp. $row->mdlusaha</td>
</tr>
<tr>
	<td width="5%"> </td>
	<td>2. Bangunan <br></td>
	<td>Rp. $row->bangunan</td>
	<td></td>
	<td>2. Laba Bulan Berjalan</td>
	<td>Rp. $row->laba</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td>3. Kendaraan <br></td>
	<td>Rp. $row->kendaraan</td>
	<td></td>
	<td><b>Jumlah Modal</b></td>
	<td>Rp. $row->jmljhmodal</td>
</tr>

<tr>
	<td width="5%">  </td>
	<td><b>Jumlah Aktiva Tetap</b><br></td>
	<td>Rp. $row->jmlhaktiva_tetap</td>
</tr>

<tr>
	<td width="5%"> </td>
	<td><b>Jumlah Aktiva</b><br></td>
	<td><b>Rp. $row->totalaktiva</b></td>
	<td></td>
	<td><b>Jumlah Hutang dan Modal</b></td>
	<td><b>Rp. $row->totalhtg</b></td>
</tr>
</table>
</div>
EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(200,0,'28','',$isibiodata,0,1,0,true,'L',true);





$pdf->ScaleXY(109, 65,-1500);
$pdf->Text(127, 70 , 'Batam, .............................');
$pdf->SetFont('times', 'B', 9);
$pdf->Text(140, 75 , 'PEMOHON');
$pdf->SetFont('times', '', 9);
$pdf->Text(125, 100 , '( ....................................................... )');

// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
// 	$table = $row->nama_lengkap.'<br>'
// 			.$row->no_ktp;
			
				
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('reportneraca.pdf','I');

//============================================================+
// END OF FILE
//============================================================+