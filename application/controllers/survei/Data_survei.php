 <?php
	class Data_survei extends CI_Controller{
		
		public function tambah_survei(){
			$id 		 		 = $this->input->post('id_user');
			$nama_survei 		 = $this->input->post('nama_survei');
			$rumah 		 		 = $this->input->post('rumah');
			$lokasi 	 	 	 = $this->input->post('lokasi');
			$lama 		 		 = $this->input->post('lama');
			$omset 				 = $this->input->post('omset');
			$operasional 		 = $this->input->post('operasional');
			$keluarga 	 		 = $this->input->post('keluarga');
			$adm 				 = $this->input->post('adm');
			$etika 		 		 = $this->input->post('etika');
			$cicilan	 		 = $this->input->post('cicilan');
			$sumber		 		 = $this->input->post('sumber');
			$hal 			 	 = $this->input->post('hal');
			$tjn_survei 		 = $this->input->post('tjn_survei');
			$tgl_survei 		 = $this->input->post('tgl_survei');
			$no_jaminan 		 = $this->input->post('no_jaminan');
			$tgl_keluar_jaminan  = $this->input->post('tgl_keluar_jaminan');
			$tgl_jatuh_jaminan 	 = $this->input->post('tgl_jatuh_jaminan');
			$luas_jaminan 		 = $this->input->post('luas_jaminan');
			$tahun_bangun 		 = $this->input->post('tahun_bangun');
			$tahun_renovasi 	 = $this->input->post('tahun_renovasi');
			$umur_efektif 		 = $this->input->post('umur_efektif');
			$luas_bangunan_fisik = $this->input->post('luas_bangunan_fisik');
			$luas_bangunan_imb 	 = $this->input->post('luas_bangunan_imb');
			$luas_tapak_bangunan = $this->input->post('luas_tapak_bangunan');
			$tahun_huni 		 = $this->input->post('tahun_huni');
			$jenis_bangunan 	 = $this->input->post('jenis_bangunan');
			$pemakai_bangunan 	 = $this->input->post('pemakai_bangunan');
			$kondisi_bangunan 	 = $this->input->post('kondisi_bangunan');
			$fasilitas_bangunan  = $this->input->post('fasilitas_bangunan');
			$taman 		 		 = $this->input->post('taman');
			$halaman 		 	 = $this->input->post('halaman');
			$pagar 		 	     = $this->input->post('pagar');
			$lainnya 		 	 = $this->input->post('lainnya');

			$config['upload_path']          = './uploads/survei/';
			$config['allowed_types']        = 'jpg|jpeg|png';

			$this->load->library('upload', $config);

            if ($this->upload->do_upload('lokasi_rumah'))
            {
                $lokasi_rumah  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('lokasi_usaha'))
            {
                $lokasi_usaha  =  $this->upload->data('file_name');
            }

			$data = array(
				'id_user' 				=> $id,
				'nama_survei' 			=> $nama_survei,
				'rumah'		    		=> $rumah,
				'lokasi' 				=> $lokasi,
				'lama' 					=> $lama,
				'omset' 				=> $omset,
				'operasional' 			=> $operasional,
				'keluarga' 				=> $keluarga,
				'adm' 					=> $adm,
				'etika' 				=> $etika,
				'cicilan' 				=> $cicilan,
				'sumber' 				=> $sumber,
				'hal'		    		=> $hal,
				'tjn_survei'		    => $tjn_survei,
				'tgl_survei'			=> $tgl_survei,
				'no_jaminan'			=> $no_jaminan,
				'tgl_keluar_jaminan'	=> $tgl_keluar_jaminan,
				'tgl_jatuh_jaminan'		=> $tgl_jatuh_jaminan,
				'luas_jaminan'			=> $luas_jaminan,
				'tahun_bangun'			=> $tahun_bangun,
				'tahun_renovasi'		=> $tahun_renovasi,
				'umur_efektif'			=> $umur_efektif,
				'luas_bangunan_fisik'	=> $luas_bangunan_fisik,
				'luas_bangunan_imb'		=> $luas_bangunan_imb,
				'luas_tapak_bangunan'	=> $luas_tapak_bangunan,
				'tahun_huni'			=> $tahun_huni,
				'jenis_bangunan'		=> $jenis_bangunan,
				'pemakai_bangunan'		=> $pemakai_bangunan,
				'kondisi_bangunan'		=> $kondisi_bangunan,
				'fasilitas_bangunan'	=> $fasilitas_bangunan,
				'taman'					=> $taman,
				'halaman'				=> $halaman,
				'pagar'					=> $pagar,
				'lainnya'				=> $lainnya,
				'lokasi_rumah'			=> $lokasi_rumah,
				'lokasi_usaha'			=> $lokasi_usaha,
			);
			
			$data2 = array(
				'status_survei'	=> 'Sudah',
				'ket'			=> 'Sudah Melakukan Survei, Harap Cek Berkala Untuk Proses Selanjutnya'
			);

			$config['upload_path']          = './uploads/survei/';
			$config['allowed_types']        = 'jpg|jpeg|png';

			$this->load->library('upload', $config);

			$jenis_jaminan_banding1		= $this->input->post('jenis_jaminan_banding1');
			$alamat_jaminan_banding1	= $this->input->post('alamat_jaminan_banding1');
			$luas_jaminan_banding1		= $this->input->post('luas_jaminan_banding1');
			$dokumen_tanah_banding1		= $this->input->post('dokumen_tanah_banding1');
			$luas_bangun_banding1		= $this->input->post('luas_bangun_banding1');
			$kontruksi_bangun_banding1	= $this->input->post('kontruksi_bangun_banding1');
			$tahun_bangun_banding1		= $this->input->post('tahun_bangun_banding1');
			$harga_penawaran_banding1	= $this->input->post('harga_penawaran_banding1');
			$waktu_penjual_banding1		= $this->input->post('waktu_penjual_banding1');
			$sumber_dana_banding1		= $this->input->post('sumber_dana_banding1');
			$telp_banding1				= $this->input->post('telp_banding1');
			$catatan_banding1			= $this->input->post('catatan_banding1');
			if ($this->upload->do_upload('lokasi_banding1'))
            {
                $lokasi_banding1  =  $this->upload->data('file_name');
            }

			$jenis_jaminan_banding2		= $this->input->post('jenis_jaminan_banding2');
			$alamat_jaminan_banding2	= $this->input->post('alamat_jaminan_banding2');
			$luas_jaminan_banding2		= $this->input->post('luas_jaminan_banding2');
			$dokumen_tanah_banding2		= $this->input->post('dokumen_tanah_banding2');
			$luas_bangun_banding2		= $this->input->post('luas_bangun_banding2');
			$kontruksi_bangun_banding2	= $this->input->post('kontruksi_bangun_banding2');
			$tahun_bangun_banding2		= $this->input->post('tahun_bangun_banding2');
			$harga_penawaran_banding2	= $this->input->post('harga_penawaran_banding2');
			$waktu_penjual_banding2		= $this->input->post('waktu_penjual_banding2');
			$sumber_dana_banding2		= $this->input->post('sumber_dana_banding2');
			$telp_banding2				= $this->input->post('telp_banding2');
			$catatan_banding2			= $this->input->post('catatan_banding2');
			if ($this->upload->do_upload('lokasi_banding2'))
            {
                $lokasi_banding2  =  $this->upload->data('file_name');
            }

			$jenis_jaminan_banding3		= $this->input->post('jenis_jaminan_banding3');
			$alamat_jaminan_banding3	= $this->input->post('alamat_jaminan_banding3');
			$luas_jaminan_banding3		= $this->input->post('luas_jaminan_banding3');
			$dokumen_tanah_banding3		= $this->input->post('dokumen_tanah_banding3');
			$luas_bangun_banding3		= $this->input->post('luas_bangun_banding3');
			$kontruksi_bangun_banding3	= $this->input->post('kontruksi_bangun_banding3');
			$tahun_bangun_banding3		= $this->input->post('tahun_bangun_banding3');
			$harga_penawaran_banding3	= $this->input->post('harga_penawaran_banding3');
			$waktu_penjual_banding3		= $this->input->post('waktu_penjual_banding3');
			$sumber_dana_banding3		= $this->input->post('sumber_dana_banding3');
			$telp_banding3				= $this->input->post('telp_banding3');
			$catatan_banding3			= $this->input->post('catatan_banding3');
			if ($this->upload->do_upload('lokasi_banding3'))
            {
                $lokasi_banding3  =  $this->upload->data('file_name');
            }

			$data3 = array(
				'id_user'					=> $id,
				'jenis_jaminan_banding1'	=> $jenis_jaminan_banding1,
				'alamat_jaminan_banding1'	=> $alamat_jaminan_banding1,
				'luas_jaminan_banding1'		=> $luas_jaminan_banding1,
				'dokumen_tanah_banding1'	=> $dokumen_tanah_banding1,
				'luas_bangun_banding1'		=> $luas_bangun_banding1,
				'kontruksi_bangun_banding1'	=> $kontruksi_bangun_banding1,
				'tahun_bangun_banding1'		=> $tahun_bangun_banding1,
				'harga_penawaran_banding1'	=> $harga_penawaran_banding1,
				'waktu_penjual_banding1'	=> $waktu_penjual_banding1,
				'sumber_dana_banding1'		=> $sumber_dana_banding1,
				'telp_banding1'				=> $telp_banding1,
				'catatan_banding1'			=> $catatan_banding1,
				'lokasi_banding1'			=> $lokasi_banding1,

				'jenis_jaminan_banding2'	=> $jenis_jaminan_banding2,
				'alamat_jaminan_banding2'	=> $alamat_jaminan_banding2,
				'luas_jaminan_banding2'		=> $luas_jaminan_banding2,
				'dokumen_tanah_banding2'	=> $dokumen_tanah_banding2,
				'luas_bangun_banding1'		=> $luas_bangun_banding2,
				'kontruksi_bangun_banding2'	=> $kontruksi_bangun_banding2,
				'tahun_bangun_banding2'		=> $tahun_bangun_banding2,
				'harga_penawaran_banding2'	=> $harga_penawaran_banding2,
				'waktu_penjual_banding2'	=> $waktu_penjual_banding2,
				'sumber_dana_banding2'		=> $sumber_dana_banding2,
				'telp_banding2'				=> $telp_banding2,
				'catatan_banding2'			=> $catatan_banding2,
				'lokasi_banding2'			=> $lokasi_banding2,

				'jenis_jaminan_banding3'	=> $jenis_jaminan_banding3,
				'alamat_jaminan_banding3'	=> $alamat_jaminan_banding3,
				'luas_jaminan_banding3'		=> $luas_jaminan_banding3,
				'dokumen_tanah_banding3'	=> $dokumen_tanah_banding3,
				'luas_bangun_banding3'		=> $luas_bangun_banding3,
				'kontruksi_bangun_banding3'	=> $kontruksi_bangun_banding3,
				'tahun_bangun_banding3'		=> $tahun_bangun_banding3,
				'harga_penawaran_banding3'	=> $harga_penawaran_banding3,
				'waktu_penjual_banding3'	=> $waktu_penjual_banding3,
				'sumber_dana_banding3'		=> $sumber_dana_banding3,
				'telp_banding3'				=> $telp_banding3,
				'catatan_banding3'			=> $catatan_banding3,
				'lokasi_banding3'			=> $lokasi_banding3,
			);
			$where = array (
				'id_user' =>$id
			);
			//var_dump($data);
			$this->model_survei->tambah_survei($data, 'survei');
			$this->model_survei->tambah_survei2($data3, 'banding');
			$this->model_survei->update_status_survei($where, $data2, 'status_pemohon');
			redirect('survei/dashboard_survei');
		}
	}
?>