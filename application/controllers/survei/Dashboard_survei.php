<?php
	class Dashboard_survei extends CI_Controller{
		
		public function index(){
			$data['b'] = $this->model_dashboard_survei->tampil_data()->result();
			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/survei/sidebar_dashboard');
            $this->load->view('survei/dashboard_survei', $data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}

		public function edit($id){
			$where = $id;
			$this->session->set_userdata('id_pemohon',$id);
			$pelogin = $this->db->query("SELECT * FROM survei, banding  WHERE banding.id_user= survei.id_user AND survei.id_user=$id")->row(); 
			if($pelogin==null){
				$data['biodata'] = $this->model_survei->edit_profile($where)->result();
				//var_dump($data); 
				
				//$data['biodata'] = $this->model_bio_pemohon_staff->tampil_data_bio()->result();
				//$data2['bio'] = $this->model_bio_pemohon_staff->tampil_data_jaminan()->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/survei/sidebar_dashboard');
	            $this->load->view('survei/data_survei',$data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}else{
				$data['biodata'] = $this->model_survei->edit_profile($where)->result();
				//var_dump($data); 
				
				//$data['biodata'] = $this->model_bio_pemohon_staff->tampil_data_bio()->result();
				//$data2['bio'] = $this->model_bio_pemohon_staff->tampil_data_jaminan()->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/survei/sidebar_dashboard');
	            $this->load->view('survei/edit_data_survei',$data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
			
		}
	}
?>