<?php
Class C_DataReport extends CI_Controller{


    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('Model_report');
    }
   

    function reportdata($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportdata', $data);
      }


       function reportpernyataan($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportpernyataan', $data);
      }

       function reportneraca($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportneraca', $data);
      }

       function reportpermohonanpinjaman($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportpermohonanpinjaman', $data);
      }

       function reportkuasasuamiistri($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportkuasasuamiistri', $data);
      }

      function reportkuasapersetujuan($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportkuasapersetujuan', $data);
      }

      function reportpenerimaandanpengeluaran($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportpenerimaandanpengeluaran', $data);
      }

      function reportsurveypenilaianjaminan($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportsurveypenilaianjaminan', $data);
      }

      function reportsurveypinjamandanabergulir($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportsurveypinjamandanabergulir', $data);
      }

      function reportpetalokasi($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportpetalokasi', $data);
      }

      function reportsurveypelaksana($id)
      {   
      $where = array('bio_pemohon.id_user' =>$id);
      $data['data'] = $this->Model_report->getreportdata($where);
      $this->load->view('report/reportsurveypelaksana', $data);
      }
  }
?>