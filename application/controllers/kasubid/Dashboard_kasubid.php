<?php
	class Dashboard_kasubid extends CI_Controller{
		
		public function index(){
			$data['b'] = $this->model_dashboard_kasubid->tampil_data()->result();
			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/kasubid/sidebar_dashboard');
            $this->load->view('kasubid/dashboard_kasubid',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}

		public function edit_data_status($id){
			$where = $id;
			$this->session->set_userdata('id_user',$id);
			$data['biodata'] = $this->model_dashboard_kasubid->edit_data_status($where)->result();

			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/kasubid/sidebar_dashboard');
            $this->load->view('kasubid/edit_data',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}

		public function update_data_status(){
		$id_user 		 = $this->input->post('id_user');
		$hasil_survei	 = $this->input->post('hasil_survei');
		$nilai	 		 = $this->input->post('nilai');

		$data = array(
				'hasil_survei' 	=> $hasil_survei. " ".$nilai
		);
		$where = array (
			'id_user' =>$id_user
		);
		$this->model_dashboard_kasubid->update_data_status($where, $data, 'status_pemohon');
		redirect('kasubid/dashboard_kasubid');
		}
	}
?>