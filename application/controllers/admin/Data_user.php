<?php
	class Data_user extends CI_Controller
	{
		public function index()
		{
			$data['user'] = $this->model_user->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_user');
			$this->load->view('admin/data_user',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$username = $this->input->post('username');
			$pwd      = $this->input->post('pwd');
			$level    = $this->input->post('level');

			$data = array (
				'username' => $username,
				//'pwd'      => md5($pwd),
				'pass'      => md5($pwd),
				'level'    => $level
			);

		$this->model_user->tambah_user($data, 'user');
		helper_log("add", "Tambah Data User");
		redirect('admin/data_user');
		}

		public function edit($id)
		{
			$where = array('id_user' =>$id);
			$data['user'] = $this->model_user->edit_user($where, 'user')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_user');
			$this->load->view('admin/edit_user',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id       = $this->input->post('id_user');
			$username = $this->input->post('username');
			$pwd      = $this->input->post('pwd');
			$level    = $this->input->post('level');

			$data = array(
				'username' => $username,
				'pass'      => md5($pwd),
				//'pwd'      => md5($pwd),
				'level'    => $level
			);
			$where = array (
				'id_user' =>$id
			);
			$this->model_user->update_data($where, $data, 'user');
			helper_log("edit", "Edit Data User");
			redirect('admin/data_user');
		}

		public function hapus($id)
		{
			$where = array('id_user' => $id);
			$this->model_user->hapus_data($where, 'user');
			helper_log("hapus", "Hapus Data User");
			redirect('admin/data_user');
		}
	}
?>