<?php
	class Dashboard_admin extends CI_Controller{

		public function __construct() 
		{
	        parent::__construct();
          $this->load->library('pagination');
	        $this->load->model('model_dashboard_admin');
    }

		public function index($offset=0)
		{
      $data['b'] = $this->model_dashboard_admin->tampil_data()->result();
      $this->load->view('templates_admin/header');
      $this->load->view('templates_admin/navbar');
      $this->load->view('templates_admin/admin/sidebar_admin');
      $this->load->view('admin/dashboard_admin', $data);
      $this->load->view('templates_admin/content');
      $this->load->view('templates_admin/footer');
    }

    public function edit_data_status($id){
      $where = $id;
      $this->session->set_userdata('id_user',$id);
      $data['biodata'] = $this->model_dashboard_admin->edit_data_status($where)->result();
      //var_dump($data);

      $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/admin/sidebar_admin');
            $this->load->view('admin/edit_data_status',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
    }

    public function update_data_status(){
      $id_user = $this->input->post('id_user');
      $ket   = $this->input->post('ket');

      $data = array(
          'ket'   => $ket
      );
      $where = array (
        'id_user' =>$id_user
      );
      $this->model_dashboard_admin->update_data_status($where, $data, 'status_pemohon');
      redirect('admin/dashboard_admin');
    }

    public function edit($id){
      $where = array('bio_pemohon.id_user' =>$id);
      $this->session->set_userdata('id_pemohon',$id);
      
      //$data['profile'] = $this->model_profile_jabatan->edit_profile($where, 'profile')->result();

      $data['biodata'] = $this->model_dashboard_admin->edit_profile($where, 'bio_pemohon');
      
      //$data['biodata'] = $this->model_bio_pemohon_staff->tampil_data_bio()->result();
      //$data2['bio'] = $this->model_bio_pemohon_staff->tampil_data_jaminan()->result();
      $this->load->view('templates_admin/header');
      $this->load->view('templates_admin/navbar');
      $this->load->view('templates_admin/admin/sidebar_admin');
      $this->load->view('admin/data_pemohon',$data);
      $this->load->view('templates_admin/content');
      $this->load->view('templates_admin/footer');
    }

    public function edit_bio(){
      $id_user         = $this->input->post('id_user');
      $nama_lengkap    = $this->input->post('nama_lengkap');
      $no_ktp          = $this->input->post('no_ktp');
      $no_hp           = $this->input->post('no_hp');
      $alamat_ktp      = $this->input->post('alamat_ktp');
      $alamat_domisili = $this->input->post('alamat_domisili');
      $tempat_lahir    = $this->input->post('tempat_lahir');
      $tgl_lahir       = $this->input->post('tgl_lahir');
      $pendidikan      = $this->input->post('pendidikan');
      $nama_ibu        = $this->input->post('nama_ibu');
      $nama_kerabat    = $this->input->post('nama_kerabat');
      $hp_kerabat      = $this->input->post('hp_kerabat');
      $nominal         = $this->input->post('nominal');

      $data = array(
        'id_user'         => $id_user,
        'nama_lengkap'    => $nama_lengkap,
        'no_ktp'          => $no_ktp,
        'no_hp'           => $no_hp,
        'alamat_ktp'      => $alamat_ktp,
        'alamat_domisili' => $alamat_domisili,
        'tempat_lahir'    => $tempat_lahir,
        'tgl_lahir'       => $tgl_lahir,
        'pendidikan'      => $pendidikan,
        'nama_ibu'        => $nama_ibu,
        'nama_kerabat'    => $nama_kerabat,
        'hp_kerabat'      => $hp_kerabat,
        'nominal'         => $nominal
      );
      $where = array (
        'id_user' =>$id_user
      );
      $this->model_dashboard_admin->update_data_bio($where, $data, 'bio_pemohon');
      helper_log("edit", "Edit Data User.$nama_lengkap ");
      redirect('admin/dashboard_admin');  
    }

    public function edit_usaha(){
      $id_user           = $this->input->post('id_user'); 
      $nama_usaha        = $this->input->post('nama_usaha'); 
      $bidang_usaha      = $this->input->post('bidang_usaha'); 
      $alamat_usaha      = $this->input->post('alamat_usaha'); 
      $mulai_usaha       = $this->input->post('mulai_usaha'); 
      $jmlh_karyawan     = $this->input->post('jmlh_karyawan'); 
      $hp_usaha          = $this->input->post('hp_usaha'); 

      $data = array(
        'id_user'         => $id_user,
        'nama_usaha'      => $nama_usaha,
        'bidang_usaha'    => $bidang_usaha,
        'alamat_usaha'    => $alamat_usaha,
        'mulai_usaha'     => $mulai_usaha,
        'jmlh_karyawan'   => $jmlh_karyawan,
        'hp_usaha'        => $hp_usaha,
      );
      $where = array(
        'id_user' => $id_user
      );
      $this->model_dashboard_admin->update_data_usaha($where, $data, 'usaha_pemohon');
      redirect('admin/dashboard_admin');
    }

    public function edit_jaminan(){
      $id_user          = $this->input->post('id_user'); 
      $bentuk_jaminan   = $this->input->post('bentuk_jaminan'); 
      $bukti_jaminan    = $this->input->post('bukti_jaminan'); 
      $nama_pemilik     = $this->input->post('nama_pemilik'); 
      $tgl_jaminan      = $this->input->post('tgl_jaminan'); 
      $alamat_jaminan   = $this->input->post('alamat_jaminan'); 
      $nilai_jaminan    = $this->input->post('nilai_jaminan'); 

      $data = array(
        'id_user'         => $id_user,
        'bentuk_jaminan'  => $bentuk_jaminan,
        'bukti_jaminan'   => $bukti_jaminan,
        'nama_pemilik'    => $nama_pemilik,
        'tgl_jaminan'     => $tgl_jaminan,
        'alamat_jaminan'  => $alamat_jaminan,
        'nilai_jaminan'   => $nilai_jaminan,
      );
      $where = array(
        'id_user' => $id_user
      );
      $this->model_dashboard_admin->update_data_jaminan($where, $data, 'jaminan_pemohon');
      redirect('admin/dashboard_admin');
    }

    public function edit_laba(){
      $id_user      = $this->input->post('id_user'); 
      $bulan1       = $this->input->post('bulan1'); 
      $bulan2       = $this->input->post('bulan2'); 
      $bulan3       = $this->input->post('bulan3'); 
      $pendusaha1     = $this->input->post('pendusaha1'); 
      $pendusaha2     = $this->input->post('pendusaha2'); 
      $pendusaha3     = $this->input->post('pendusaha3'); 
      $pendlain1      = $this->input->post('pendlain1'); 
      $pendlain2      = $this->input->post('pendlain2'); 
      $pendlain3      = $this->input->post('pendlain3'); 
      $pengbahan1     = $this->input->post('pengbahan1'); 
      $pengbahan2     = $this->input->post('pengbahan2'); 
      $pengbahan3     = $this->input->post('pengbahan3'); 
      $pengtenaga1    = $this->input->post('pengtenaga1'); 
      $pengtenaga2    = $this->input->post('pengtenaga2'); 
      $pengtenaga3    = $this->input->post('pengtenaga3'); 
      $penglistrik1   = $this->input->post('penglistrik1'); 
      $penglistrik2   = $this->input->post('penglistrik2'); 
      $penglistrik3   = $this->input->post('penglistrik3'); 
      $pengadm1     = $this->input->post('pengadm1'); 
      $pengadm2     = $this->input->post('pengadm2'); 
      $pengadm3     = $this->input->post('pengadm3'); 
      $pengtran1      = $this->input->post('pengtran1'); 
      $pengtran2      = $this->input->post('pengtran2'); 
      $pengtran3      = $this->input->post('pengtran3'); 
      $penglain1      = $this->input->post('penglain1'); 
      $penglain2      = $this->input->post('penglain2'); 
      $penglain3      = $this->input->post('penglain3'); 

      $data1 = array(
        'id_user'       => $id_user,
        'bulan1'        => $bulan1,
        'pendusaha1'    => $pendusaha1,
        'pendlain1'     => $pendlain1,
        'pengbahan1'    => $pengbahan1,
        'pengtenaga1'   => $pengtenaga1,
        'penglistrik1'  => $penglistrik1,
        'pengadm1'      => $pengadm1,
        'pengtran1'     => $pengtran1,
        'penglain1'     => $penglain1,
      );
      $data2 = array(
        'bulan2'        => $bulan2,
        'pendusaha2'    => $pendusaha2,
        'pendlain2'     => $pendlain2,
        'pengbahan2'    => $pengbahan2,
        'pengtenaga2'   => $pengtenaga2,
        'penglistrik2'  => $penglistrik2,
        'pengadm2'      => $pengadm2,
        'pengtran2'     => $pengtran2,
        'penglain2'     => $penglain2,
      );
      $data3 = array(
        'bulan3'        => $bulan3,
        'pendusaha3'    => $pendusaha3,
        'pendlain3'     => $pendlain3,
        'pengbahan3'    => $pengbahan3,
        'pengtenaga3'   => $pengtenaga3,
        'penglistrik3'  => $penglistrik3,
        'pengadm3'      => $pengadm3,
        'pengtran3'     => $pengtran3,
        'penglain3'     => $penglain3,
      );
      $where = array(
        'id_user' => $id_user
      );
      $this->model_dashboard_admin->update_data_laba1($where, $data1, 'laba1');
      $this->model_dashboard_admin->update_data_laba2($where, $data2, 'laba2');
      $this->model_dashboard_admin->update_data_laba3($where, $data3, 'laba3');
      redirect('admin/dashboard_admin');
    }
	}
?>