<?php
	class Data_biodata extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('bio_pemohon', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_bio');
	           	$this->load->view('pemohon/data_biodata');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$where = array('id_user' =>$id);
				$data['biodata'] = $this->model_bio_pemohon->edit_pemohon($where, 'bio_pemohon')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_bio');
	           	$this->load->view('pemohon/edit_biodata', $data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_bio()
		{
			$id_user			= $this->input->post('id_user');
			$tgl_input		    = date("Y-m-d");
			$nama_lengkap 		= $this->input->post('nama_lengkap');
			$no_ktp       	    = $this->input->post('no_ktp');
			$status       	    = $this->input->post('status');
			$alamat_ktp  	    = $this->input->post('alamat_ktp');
			$alamat_domisili 	= $this->input->post('alamat_domisili');
			$tempat_lahir 		= $this->input->post('tempat_lahir');
			$tgl_lahir 			= $this->input->post('tgl_lahir');
			$no_hp 				= $this->input->post('no_hp');
			$status_rumah 		= $this->input->post('status_rumah');
			$jk					= $this->input->post('jk');
			$pendidikan			= $this->input->post('pendidikan');
			$nama_ibu			= $this->input->post('nama_ibu');
			$nama_kerabat		= $this->input->post('nama_kerabat');
			$hp_kerabat			= $this->input->post('hp_kerabat');
			$nominal			= $this->input->post('nominal');

			$data = array (
				'id_user'			  => $id_user,
				'tgl_input'			  => $tgl_input,
				'nama_lengkap' 		  => $nama_lengkap,
				'no_ktp'         	  => $no_ktp,
				'status'         	  => $status,
				'alamat_ktp'     	  => $alamat_ktp,
				'alamat_domisili'  	  => $alamat_domisili,
				'tempat_lahir'  	  => $tempat_lahir,
				'tgl_lahir'  	  	  => $tgl_lahir,
				'no_hp'  	  		  => $no_hp,
				'status_rumah'  	  => $status_rumah,
				'jk'  	  			  => $jk,
				'pendidikan'  	  	  => $pendidikan,
				'nama_ibu'  	   	  => $nama_ibu,
				'nama_kerabat'  	  => $nama_kerabat,
				'nominal'  	  		  => $nominal,
				'hp_kerabat'  		  => $nama_kerabat
			);

			$this->model_bio_pemohon->tambah_bio($data, 'bio_pemohon');
			helper_log("add", "Tambah Data Biodata Pemohon");
			redirect('pemohon/data_biodata');
		}

		public function update_usaha()
		{
			$id_user	   = $this->input->post('id_user');
			$nama_usaha    = $this->input->post('nama_usaha');
			$jmlh_karyawan = $this->input->post('jmlh_karyawan');
			$alamat_usaha  = $this->input->post('alamat_usaha');
			$bidang_usaha  = $this->input->post('bidang_usaha');
			$mulai_usaha   = $this->input->post('mulai_usaha');
			$hp_usaha 	   = $this->input->post('hp_usaha');

			$data = array(
				'nama_usaha' 	=> $nama_usaha,
				'jmlh_karyawan' => $jmlh_karyawan,
				'alamat_usaha'  => $alamat_usaha,
				'bidang_usaha'  => $bidang_usaha,
				'mulai_usaha' 	=> $mulai_usaha,
				'hp_usaha' 		=> $hp_usaha
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_usaha1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

		public function update_jaminan()
		{
			$id_user	   	   = $this->input->post('id_user');
			$bentuk_jaminan    = $this->input->post('bentuk_jaminan');
			$bukti_jaminan 	   = $this->input->post('bukti_jaminan');
			$tgl_jaminan  	   = $this->input->post('tgl_jaminan');
			$nama_pemilik  	   = $this->input->post('nama_pemilik');
			$nilai_jaminan     = $this->input->post('nilai_jaminan');
			$alamat_jaminan    = $this->input->post('alamat_jaminan');

			$data = array(
				'bentuk_jaminan' 	=> $bentuk_jaminan,
				'bukti_jaminan'	    => $bukti_jaminan,
				'tgl_jaminan'  		=> $tgl_jaminan,
				'nama_pemilik'  	=> $nama_pemilik,
				'nilai_jaminan' 	=> $nilai_jaminan,
				'alamat_jaminan' 	=> $alamat_jaminan
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_jaminan1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

	}
?>