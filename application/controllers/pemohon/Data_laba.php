<?php
	class Data_laba extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('laba1', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_laba');
	           	$this->load->view('pemohon/data_laba');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$id = $this->session->userdata("id_user");
				$data1['lab1'] = $this->model_laba_pemohon->tampil_data($id)->result();
				// $data2['lab2'] = $this->model_laba_pemohon->edit_pemohon1($where, 'laba2')->result();
				// $data3['lab3'] = $this->model_laba_pemohon->edit_pemohon2($where, 'laba3')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_laba');
	           	$this->load->view('pemohon/edit_laba', $data1);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_laba()
		{
			$id_user			= $this->input->post('id_user');
			$bulan1 			= $this->input->post('bulan1');
			$pendusaha1 		= $this->input->post('pendusaha1');
			$pendlain1 			= $this->input->post('pendlain1');
			$pengbahan1 		= $this->input->post('pengbahan1');
			$pengtenaga1 		= $this->input->post('pengtenaga1');
			$penglistrik1 		= $this->input->post('penglistrik1');
			$pengadm1 			= $this->input->post('pengadm1');
			$pengtran1 			= $this->input->post('pengtran1');
			$penglain1 			= $this->input->post('penglain1');
			$pendusaha1int		=str_replace(".", "", $pendusaha1);
			$penglain1int 		=str_replace(".", "", $penglain1);
			$pengbahan1int 		=str_replace(".", "", $pengbahan1);
			$pengtenaga1int	    =str_replace(".", "", $pengtenaga1);
			$penglistrik1int 	=str_replace(".", "", $penglistrik1);
			$pengadm1int 		=str_replace(".", "", $pengadm1);
			$pengtran1int 		=str_replace(".", "", $pengtran1);
			$penglain1int 		=str_replace(".", "", $penglain1);

			$data1 = array (
				'id_user'			  => $id_user,
				'bulan1'			  => $bulan1,
				'pendusaha1'		  => $pendusaha1int,
				'pendlain1'			  => $pendusaha1int,
				'pengbahan1'		  => $pengbahan1int,
				'pengtenaga1'		  => $pengtenaga1int,
				'penglistrik1'		  => $penglistrik1int,
				'pengadm1'			  => $pengadm1int,
				'pengtran1'			  => $pengtran1int,
				'penglain1'			  => $penglain1int,
			);

			$bulan2 			= $this->input->post('bulan2');
			$pendusaha2 		= $this->input->post('pendusaha2');
			$pendlain2 			= $this->input->post('pendlain2');
			$pengbahan2 		= $this->input->post('pengbahan2');
			$pengtenaga2 		= $this->input->post('pengtenaga2');
			$penglistrik2 		= $this->input->post('penglistrik2');
			$pengadm2 			= $this->input->post('pengadm2');
			$pengtran2 			= $this->input->post('pengtran2');
			$penglain2 			= $this->input->post('penglain2');
			$pendusaha2int 		=str_replace(".", "", $pendusaha2);
			$penglain2int 		=str_replace(".", "", $pendlain2);
			$pengbahan2int 		=str_replace(".", "", $pengbahan2);
			$pengtenaga2int 	=str_replace(".", "", $pengtenaga2);
			$penglistrik2int 	=str_replace(".", "", $penglistrik2);
			$pengadm2int 		=str_replace(".", "", $pengadm2);
			$pengtran2int 		=str_replace(".", "", $pengtran2);
			$penglain2int 		=str_replace(".", "", $penglain2);

			$data2 = array (
				'id_user'			  => $id_user,
				'bulan2'			  => $bulan2,
				'pendusaha2'		  => $pendusaha2int,
				'pendlain2'			  => $penglain2int,
				'pengbahan2'		  => $pengbahan2int,
				'pengtenaga2'		  => $pengtenaga2int,
				'penglistrik2'		  => $penglistrik2int,
				'pengadm2'			  => $pengadm2int,
				'pengtran2'			  => $pengtran2int,
				'penglain2'			  => $penglain2int,
			);

			$bulan3 			= $this->input->post('bulan3');
			$pendusaha3 		= $this->input->post('pendusaha3');
			$pendlain3 			= $this->input->post('pendlain3');
			$pengbahan3 		= $this->input->post('pengbahan3');
			$pengtenaga3 		= $this->input->post('pengtenaga3');
			$penglistrik3 		= $this->input->post('penglistrik3');
			$pengadm3			= $this->input->post('pengadm3');
			$pengtran3 			= $this->input->post('pengtran3');
			$penglain3 			= $this->input->post('penglain3');
			$pendusaha3int 		=str_replace(".", "", $pendusaha3);
			$pendlain3int	    =str_replace(".", "", $pendlain3);
			$pengbahan3int 		=str_replace(".", "", $pengbahan3);
			$pengtenaga3int 	=str_replace(".", "", $pengtenaga3);
			$penglistrik3int    =str_replace(".", "", $penglistrik3);
			$pengadm3int 		=str_replace(".", "", $pengadm3);
			$pengtran3int 		=str_replace(".", "", $pengtran3);
			$penglain3int	    =str_replace(".", "", $penglain3);

			$data3 = array (
				'id_user'			  => $id_user,
				'bulan3'			  => $bulan3,
				'pendusaha3'		  => $pendusaha3int,
				'pendlain3'			  => $pendlain3int,
				'pengbahan3'		  => $pengbahan3int,
				'pengtenaga3'		  => $pengtenaga3int,
				'penglistrik3'		  => $penglistrik3int,
				'pengadm3'			  => $pengadm3int,
				'pengtran3'			  => $pengtran3int,
				'penglain3'			  => $penglain3int,
			);

			$this->model_laba_pemohon->tambah_laba1($data1, 'laba1');
			$this->model_laba_pemohon->tambah_laba2($data2, 'laba2');
			$this->model_laba_pemohon->tambah_laba3($data3, 'laba3');
			//helper_log("add", "Tambah Data Bio Pemohon");
			helper_log("add", "Tambah Data Laba Pemohon");
			redirect('pemohon/data_laba');
		}

		public function update_usaha()
		{
			$id_user	   = $this->input->post('id_user');
			$nama_usaha    = $this->input->post('nama_usaha');
			$jmlh_karyawan = $this->input->post('jmlh_karyawan');
			$alamat_usaha  = $this->input->post('alamat_usaha');
			$bidang_usaha  = $this->input->post('bidang_usaha');
			$mulai_usaha   = $this->input->post('mulai_usaha');
			$hp_usaha 	   = $this->input->post('hp_usaha');

			$data = array(
				'nama_usaha' 	=> $nama_usaha,
				'jmlh_karyawan' => $jmlh_karyawan,
				'alamat_usaha'  => $alamat_usaha,
				'bidang_usaha'  => $bidang_usaha,
				'mulai_usaha' 	=> $mulai_usaha,
				'hp_usaha' 		=> $hp_usaha
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_usaha1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

		public function update_jaminan()
		{
			$id_user	   	   = $this->input->post('id_user');
			$bentuk_jaminan    = $this->input->post('bentuk_jaminan');
			$bukti_jaminan 	   = $this->input->post('bukti_jaminan');
			$tgl_jaminan  	   = $this->input->post('tgl_jaminan');
			$nama_pemilik  	   = $this->input->post('nama_pemilik');
			$nilai_jaminan     = $this->input->post('nilai_jaminan');
			$alamat_jaminan    = $this->input->post('alamat_jaminan');

			$data = array(
				'bentuk_jaminan' 	=> $bentuk_jaminan,
				'bukti_jaminan'	    => $bukti_jaminan,
				'tgl_jaminan'  		=> $tgl_jaminan,
				'nama_pemilik'  	=> $nama_pemilik,
				'nilai_jaminan' 	=> $nilai_jaminan,
				'alamat_jaminan' 	=> $alamat_jaminan
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_jaminan1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

	}
?>