 <?php
	class Data_neraca extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('neraca', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_neraca');
	           	$this->load->view('pemohon/data_neraca');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$id = $this->session->userdata("id_user");
				$data1['biodata'] = $this->model_neraca_pemohon->tampil_data($id)->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_neraca');
	           	$this->load->view('pemohon/edit_neraca', $data1);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_neraca()
		{
			$id_user	   = $this->input->post('id_user');
			$bulan    	   = $this->input->post('bulan');
			$kas 	   	   = $this->input->post('kas');
			$bank  	   	   = $this->input->post('bank');
			$piutang  	   = $this->input->post('piutang');
			$persediaan    = $this->input->post('persediaan');
			$htgusaha      = $this->input->post('htgusaha');
			$tanah    	   = $this->input->post('tanah');
			$bangunan      = $this->input->post('bangunan');
			$kendaraan     = $this->input->post('kendaraan');
			$mdlusaha      = $this->input->post('mdlusaha');
			$laba    	   = $this->input->post('laba');
			$kasint 	   =str_replace(".", "", $kas);
			$bankint 	   =str_replace(".", "", $bank);
			$bankint 	   =str_replace(".", "", $bank);
			$piutangint    =str_replace(".", "", $piutang);
			$persediaanint =str_replace(".", "", $persediaan);
			$htgusahaint   =str_replace(".", "", $htgusaha);
			$tanahint 	   =str_replace(".", "", $tanah);
			$bangunanint   =str_replace(".", "", $bangunan);
			$kendaraanint  =str_replace(".", "", $kendaraan);
			$mdlusahaint   =str_replace(".", "", $mdlusaha);
			$labaint 	   =str_replace(".", "", $laba);

			$data = array(
				'id_user' 		=> $id_user,
				'bulan' 		=> $bulan,
				'kas'	    	=> $kasint,
				'bank'  		=> $bankint,
				'piutang'  		=> $piutangint,
				'persediaan' 	=> $persediaanint,
				'htgusaha' 		=> $htgusahaint,
				'tanah' 		=> $tanahint,
				'bangunan' 		=> $bangunanint,
				'kendaraan' 	=> $kendaraanint,
				'mdlusaha' 		=> $mdlusahaint,
				'laba' 			=> $labaint
			);
			
			$this->model_neraca_pemohon->tambah_neraca($data, 'neraca');
			helper_log("add", "Tambah Data Neraca Pemohon");
			redirect('pemohon/data_neraca');
		}

	}
?>