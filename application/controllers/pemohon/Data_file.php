<?php
	class Data_file extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('file_pemohon', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_file');
	           	$this->load->view('pemohon/data_file');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$where = array('id_user' =>$id);
				$data['biodata'] = $this->model_usaha_pemohon->edit_pemohon($where, 'file_pemohon')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_file');
	           	$this->load->view('pemohon/edit_file', $data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_file()
		{
			$config['upload_path']          = './uploads/file/';
			$config['allowed_types']        = 'jpg|jpeg|png';

			$this->load->library('upload', $config);

            if ($this->upload->do_upload('ktp_suami'))
            {
                $ktp_suami  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('ktp_istri'))
            {
                $ktp_istri  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('kk'))
            {
                $kk  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('iumk'))
            {
                $iumk  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('buku_nikah'))
            {
                $buku_nikah  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('foto_suami'))
            {
                $foto_suami  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('foto_istri'))
            {
                $foto_istri  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('foto_usaha'))
            {
                $foto_usaha  =  $this->upload->data('file_name');
            }
            if ($this->upload->do_upload('ser_jaminan'))
            {
                $ser_jaminan  =  $this->upload->data('file_name');
            }

            $id_user = $this->input->post('id_user');
			$data = array(
                'id_user'          => $id_user,                     
                'ktp_suami'        => $ktp_suami,                        
                'ktp_istri'        => $ktp_istri,                       
                'kk'        	   => $kk,                       
                'iumk'        	   => $iumk,                       
                'buku_nikah'       => $buku_nikah,                       
                'foto_suami'       => $foto_suami,                       
                'foto_istri'       => $foto_istri,                       
                'foto_usaha'       => $foto_usaha,                       
                'ser_jaminan'      => $ser_jaminan,                       
            );

            $tgl = date('Y-m-d');
            $data2 = array(
            	'id_user'	=> $id_user,
            	'tgl_pemohon'	=> $tgl,
            	'status_survei'	=> 'Belum',
            	'hasil_survei'	=> 'Belum',
            	'status_akhir'	=> 'Belum',
            	'ket'			=> 'Sudah Mengajukan Permohonan, Mohon Menunggu Informasi Selanjutnya'
            );

			$this->model_file_pemohon->tambah_file($data, 'file_pemohon');
			$this->model_file_pemohon->tambah_status($data2, 'status_pemohon');
			helper_log("add", "Tambah Data File Pemohon");
			redirect('pemohon/data_file');
		}

		public function update_jaminan()
		{
			$id_user	   	   = $this->input->post('id_user');
			$bentuk_jaminan    = $this->input->post('bentuk_jaminan');
			$bukti_jaminan 	   = $this->input->post('bukti_jaminan');
			$tgl_jaminan  	   = $this->input->post('tgl_jaminan');
			$nama_pemilik  	   = $this->input->post('nama_pemilik');
			$nilai_jaminan     = $this->input->post('nilai_jaminan');
			$alamat_jaminan    = $this->input->post('alamat_jaminan');

			$data = array(
				'bentuk_jaminan' 	=> $bentuk_jaminan,
				'bukti_jaminan'	    => $bukti_jaminan,
				'tgl_jaminan'  		=> $tgl_jaminan,
				'nama_pemilik'  	=> $nama_pemilik,
				'nilai_jaminan' 	=> $nilai_jaminan,
				'alamat_jaminan' 	=> $alamat_jaminan
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_jaminan1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

	}
?>