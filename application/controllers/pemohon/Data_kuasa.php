 <?php
	class Data_kuasa extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('kuasa', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/pemohon/navbar_pemohon');
	            $this->load->view('templates_admin/pemohon/sidebar_kuasa');
	           	$this->load->view('pemohon/data_kuasa');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$where = array('id_user' =>$id);
				$data['biodata'] = $this->model_kuasa_pemohon->edit_pemohon($where, 'kuasa')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/pemohon/navbar_pemohon');
	            $this->load->view('templates_admin/pemohon/sidebar_kuasa');
	           	$this->load->view('pemohon/edit_kuasa', $data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_kuasa()
		{
			$id_user	   	 = $this->input->post('id_user');
			$nm_kuasa    	 = $this->input->post('nm_kuasa');
			$no_ktp    		 = $this->input->post('no_ktp');
			$tempat_lahir    = $this->input->post('tempat_lahir');
			$tgl  		     = $this->input->post('tgl');
			$alamat_kuasa    = $this->input->post('alamat_kuasa');
			$nm_saksi1    	 = $this->input->post('nm_saksi1');
			$nm_saksi2    	 = $this->input->post('nm_saksi2');

			$data = array(
				'id_user' 				=> $id_user,
				'nm_kuasa' 				=> $nm_kuasa,
				'no_ktp_kuasa' 			=> $no_ktp,
				'tempat_lahir_kuasa' 	=> $tempat_lahir,
				'tgl_lahir_kuasa' 		=> $tgl,
				'alamat_kuasa' 			=> $alamat_kuasa,
				'nm_saksi1' 			=> $nm_saksi1,
				'nm_saksi2' 			=> $nm_saksi2
			);
			
			$this->model_kuasa_pemohon->tambah_kuasa($data, 'kuasa');
			helper_log("add", "Tambah Data Kuasa Pemohon");
			redirect('pemohon/data_kuasa');
		}

	}
?>