<?php
	class Dashboard_pemohon extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			//$data['biodata'] = $this->model_kuasa_pemohon->edit_pemohon($where, 'kuasa')->result();
			$data['data'] = $this->model_dashboard_pemohon->tampil_data($id)->result();
			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/pemohon/navbar_pemohon');
            $this->load->view('templates_admin/pemohon/sidebar_pemohon');
            $this->load->view('pemohon/dashboard_pemohon', $data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}
	}
?>