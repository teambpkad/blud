 <?php
	class Data_jaminan extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('jaminan_pemohon', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_jaminan');
	           	$this->load->view('pemohon/data_jaminan');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$where = array('id_user' =>$id);
				$data['biodata'] = $this->model_jaminan_pemohon->edit_pemohon($where, 'jaminan_pemohon')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_jaminan');
	           	$this->load->view('pemohon/edit_jaminan', $data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_jaminan()
		{
			$id_user	   	   = $this->input->post('id_user');
			$bentuk_jaminan    = $this->input->post('bentuk_jaminan');
			$bukti_jaminan 	   = $this->input->post('bukti_jaminan');
			$tgl_jaminan  	   = $this->input->post('tgl_jaminan');
			$nama_pemilik  	   = $this->input->post('nama_pemilik');
			$nilai_jaminan     = $this->input->post('nilai_jaminan');
			$alamat_jaminan    = $this->input->post('alamat_jaminan');
			$status_jaminan    = $this->input->post('status_jaminan');

			$data = array(
				'id_user' 			=> $id_user,
				'bentuk_jaminan' 	=> $bentuk_jaminan,
				'bukti_jaminan'	    => $bukti_jaminan,
				'tgl_jaminan'  		=> $tgl_jaminan,
				'nama_pemilik'  	=> $nama_pemilik,
				'nilai_jaminan' 	=> $nilai_jaminan,
				'status_jaminan' 	=> $status_jaminan,
				'alamat_jaminan' 	=> $alamat_jaminan
			);
			
			$this->model_jaminan_pemohon->tambah_jaminan($data, 'jaminan_pemohon');
			helper_log("add", "Tambah Data Jaminan Pemohon");
			redirect('pemohon/data_jaminan');
		}

	}
?>