<?php
	class Data_usaha extends CI_Controller{
		
		public function index(){
			$id = $this->session->userdata("id_user");
			$pelogin = $this->db->get_where('usaha_pemohon', array('id_user' => $id))->row(); 
			if ($pelogin==null){
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_usaha');
	           	$this->load->view('pemohon/data_usaha');
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');	
			}else{
				$where = array('id_user' =>$id);
				$data['biodata'] = $this->model_usaha_pemohon->edit_pemohon($where, 'usaha_pemohon')->result();
				$this->load->view('templates_admin/header');
	            $this->load->view('templates_admin/navbar');
	            $this->load->view('templates_admin/pemohon/sidebar_usaha');
	           	$this->load->view('pemohon/edit_usaha', $data);
	            $this->load->view('templates_admin/content');
	            $this->load->view('templates_admin/footer');
			}
				
		}

		public function tambah_usaha()
		{
			$id_user	   = $this->input->post('id_user');
			$nama_usaha    = $this->input->post('nama_usaha');
			$jmlh_karyawan = $this->input->post('jmlh_karyawan');
			$alamat_usaha  = $this->input->post('alamat_usaha');
			$bidang_usaha  = $this->input->post('bidang_usaha');
			$mulai_usaha   = $this->input->post('mulai_usaha');
			$hp_usaha 	   = $this->input->post('hp_usaha');

			$data = array(
				'nama_usaha' 	=> $nama_usaha,
				'id_user' 		=> $id_user,
				'jmlh_karyawan' => $jmlh_karyawan,
				'alamat_usaha'  => $alamat_usaha,
				'bidang_usaha'  => $bidang_usaha,
				'mulai_usaha' 	=> $mulai_usaha,
				'hp_usaha' 		=> $hp_usaha
			);
			$this->model_usaha_pemohon->tambah_usaha($data, 'usaha_pemohon');
			helper_log("add", "Tambah Data Usaha Pemohon");
			redirect('pemohon/data_usaha');
		}

		public function update_jaminan()
		{
			$id_user	   	   = $this->input->post('id_user');
			$bentuk_jaminan    = $this->input->post('bentuk_jaminan');
			$bukti_jaminan 	   = $this->input->post('bukti_jaminan');
			$tgl_jaminan  	   = $this->input->post('tgl_jaminan');
			$nama_pemilik  	   = $this->input->post('nama_pemilik');
			$nilai_jaminan     = $this->input->post('nilai_jaminan');
			$alamat_jaminan    = $this->input->post('alamat_jaminan');

			$data = array(
				'bentuk_jaminan' 	=> $bentuk_jaminan,
				'bukti_jaminan'	    => $bukti_jaminan,
				'tgl_jaminan'  		=> $tgl_jaminan,
				'nama_pemilik'  	=> $nama_pemilik,
				'nilai_jaminan' 	=> $nilai_jaminan,
				'alamat_jaminan' 	=> $alamat_jaminan
			);
			$where = array (
				'id_user' =>$id_user
			);
			$this->model_data_biodata->update_jaminan1($where, $data, 'pemohon');
			redirect('pemohon/data_biodata');
		}

	}
?>