<?php
	class Dashboard_staff extends CI_Controller{
		
		public function index(){
			$data['b'] = $this->model_dashboard_staff->tampil_data()->result();
			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/staff/sidebar_dashboard');
            $this->load->view('staff/dashboard_staff', $data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}
		public function edit($id){
			$where = array('bio_pemohon.id_user' =>$id);
			$this->session->set_userdata('id_pemohon',$id);
			
			//$data['profile'] = $this->model_profile_jabatan->edit_profile($where, 'profile')->result();

			$data['biodata'] = $this->model_bio_pemohon_staff->edit_profile($where, 'bio_pemohon');
			
			//$data['biodata'] = $this->model_bio_pemohon_staff->tampil_data_bio()->result();
			//$data2['bio'] = $this->model_bio_pemohon_staff->tampil_data_jaminan()->result();
			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/staff/sidebar_dashboard');
            $this->load->view('staff/data_pemohon',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}

		public function edit_data_status($id){
			$where = $id;
			$this->session->set_userdata('id_user',$id);
			$data['biodata'] = $this->model_dashboard_staff->edit_data_status($where)->result();
			//var_dump($data);

			$this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/staff/sidebar_dashboard');
            $this->load->view('staff/edit_data',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
		}

		public function update_data_status(){
		$id_user = $this->input->post('id_user');
		$ket	 = $this->input->post('ket');

		$data = array(
				'ket' 	=> $ket
		);
		$where = array (
			'id_user' =>$id_user
		);
		$this->model_dashboard_staff->update_data_status($where, $data, 'status_pemohon');
		redirect('staff/dashboard_staff');
		}
	}
?>