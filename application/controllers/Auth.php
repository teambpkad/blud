<?php 
	class Auth extends CI_Controller{
		public function __construct() 
		{
	        parent::__construct();
            
	        $this->load->model('model_auth');
    	}

    	public function cek_login(){
    		if (isset($_POST['login'])){
    			$username = $this->input->post('username', true);
    			$pass = $this->input->post('pass', true);
    			$passx = md5($pass);
    			$cek = $this->model_auth->proses_login($username, $passx);
    			$hasil = count($cek);
    			if($hasil > 0){
    				$pelogin = $this->db->get_where('user', array('username' => $username, 'pass' =>$passx))->row();   
    				$this->session->set_userdata('level',$pelogin->level);
    				$this->session->set_userdata('username',$pelogin->username);
    				$this->session->set_userdata('id_user',$pelogin->id_user);
    				if($pelogin->level == 'admin'){
    					redirect(base_url('admin/dashboard_admin'));
    				}elseif($pelogin->level == 'staff'){
    					redirect(base_url('staff/dashboard_staff'));
    				}elseif($pelogin->level == 'survei'){
                        redirect(base_url('survei/dashboard_survei'));
                    }elseif($pelogin->level == 'kasubid'){
                        redirect(base_url('kasubid/dashboard_kasubid'));
                    }else{
    					redirect(base_url('pemohon/dashboard_pemohon'));
    				}
    			}else{
    				echo "<script>alert('Login Gagal, Username / Password anda salah!');
                window.location='".base_url('auth/login')."';
                </script>";
    			}
    		}
    	}

		public function login()
		{

			$this->load->view('login');
// End fungsi login
		}

		public function logout()
		{
			$this->model_auth->logout();
			helper_log("Logout", "Logout");
		}
	}
 ?>