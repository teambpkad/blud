<?php

class Model_usaha_pemohon extends CI_Model
{
	
	public function tambah_usaha($data, $table){
		$this->db->insert($table, $data);
	}

	public function update_usaha1($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_jaminan1($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function edit_pemohon($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>