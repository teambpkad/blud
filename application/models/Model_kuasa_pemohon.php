<?php

class Model_kuasa_pemohon extends CI_Model
{
	
	public function tambah_kuasa($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_pemohon($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>