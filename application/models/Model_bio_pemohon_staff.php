<?php

class Model_bio_pemohon_staff extends CI_Model
{
	// public function tampil_data(){
	// 	$query = $this->db->query("SELECT * from bio_pemohon, file_pemohon, jaminan_pemohon, laba1, laba2 where bio_pemohon.id_user=2 and file_pemohon.id_user=2 and jaminan_pemohon.id_user=2 and laba1.id_user=2 and laba2.id_user=2");
	// 	return $query;
	// 	//return $this->db->get('bio_pemohon');
	// }
	public function tampil_data_bio(){
		return $this->db->get('bio_pemohon');
	}

	public function tampil_data_bio_report(){
		return $this->db->select('*, sum(laba1.pendusaha1 + laba1.pendlain1) as totalpend1, sum(laba2.pendusaha2 + laba2.pendlain2) as totalpend2, sum(laba3.pendusaha3 + laba3.pendlain3) as totalpend3, sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1) as totalpeng1, sum(laba2.pengbahan2 + laba2.pengtenaga2 + laba2.penglistrik2 + laba2.pengadm2 + laba2.pengtran2 + laba2.penglain2) as totalpeng2, sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3) as totalpeng3, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1))  as def1, (sum(laba2.pendusaha2 + laba2.pendlain2) - sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba2.penglain2))  as def2, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3))  as def3, , sum(kas+bank+piutang+persediaan) as jmlhaktiva_lancar, sum(tanah+bangunan+kendaraan) as jmlhaktiva_tetap, sum(htgusaha) as jmlhhutang, sum(mdlusaha+laba) as jmljhmodal,(sum(kas+bank+piutang+persediaan)+sum(tanah+bangunan+kendaraan))as totalaktiva , (sum(htgusaha)+sum(mdlusaha+laba)) as totalhtg')
						->from('bio_pemohon')
						->join('usaha_pemohon', 'usaha_pemohon.id_user = bio_pemohon.id_user')
						->join('jaminan_pemohon', 'jaminan_pemohon.id_user = bio_pemohon.id_user')
						->join('laba1', 'laba1.id_user = bio_pemohon.id_user')
						->join('laba2', 'laba2.id_user = bio_pemohon.id_user')
						->join('laba3', 'laba3.id_user = bio_pemohon.id_user')
						->join('neraca', 'neraca.id_user = bio_pemohon.id_user')
						->join('file_pemohon', 'file_pemohon.id_user = bio_pemohon.id_user')
						->get()
						->result();
	}


	public function edit_profile($where, $table){
	    return    $this->db->select('*, sum(laba1.pendusaha1 + laba1.pendlain1) as totalpend1, sum(laba2.pendusaha2 + laba2.pendlain2) as totalpend2, sum(laba3.pendusaha3 + laba3.pendlain3) as totalpend3, sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1) as totalpeng1, sum(laba2.pengbahan2 + laba2.pengtenaga2 + laba2.penglistrik2 + laba2.pengadm2 + laba2.pengtran2 + laba2.penglain2) as totalpeng2, sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3) as totalpeng3, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1))  as def1, (sum(laba2.pendusaha2 + laba2.pendlain2) - sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba2.penglain2))  as def2, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3))  as def3, , sum(kas+bank+piutang+persediaan) as jmlhaktiva_lancar, sum(tanah+bangunan+kendaraan) as jmlhaktiva_tetap, sum(htgusaha) as jmlhhutang, sum(mdlusaha+laba) as jmljhmodal,(sum(kas+bank+piutang+persediaan)+sum(tanah+bangunan+kendaraan))as totalaktiva , (sum(htgusaha)+sum(mdlusaha+laba)) as totalhtg')
						->from($table)
						->join('usaha_pemohon', 'usaha_pemohon.id_user = bio_pemohon.id_user')
						->join('jaminan_pemohon', 'jaminan_pemohon.id_user = bio_pemohon.id_user')
						->join('laba1', 'laba1.id_user = bio_pemohon.id_user')
						->join('laba2', 'laba2.id_user = bio_pemohon.id_user')
						->join('laba3', 'laba3.id_user = bio_pemohon.id_user')
						->join('neraca', 'neraca.id_user = bio_pemohon.id_user')
						->join('file_pemohon', 'file_pemohon.id_user = bio_pemohon.id_user')
						->where($where)
						->get()
						->result();
	
	}

	public function update_data_bio($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data_usaha($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data_jaminan($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data_laba1($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data_laba2($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data_laba3($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function tampil_data_jaminan(){
		return $this->db->get('jaminan_pemohon');
	}
	
	public function tambah_bio($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_pemohon($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>