<?php

class Model_survei extends CI_Model
{
	
	public function tambah_survei($data, $table){
		$this->db->insert($table, $data);
	}

	public function tambah_survei2($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_profile($id){
		return $this->db->query("SELECT * FROM bio_pemohon, usaha_pemohon, jaminan_pemohon, survei, banding where bio_pemohon.id_user=usaha_pemohon.id_user AND bio_pemohon.id_user=jaminan_pemohon.id_user AND bio_pemohon.id_user=survei.id_user AND bio_pemohon.id_user=banding.id_user AND bio_pemohon.id_user=$id");
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function update_status_survei($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}
}
?>