<?php

class Model_neraca_pemohon extends CI_Model
{
	
	public function tambah_neraca($data, $table){
		$this->db->insert($table, $data);
	}

	public function tampil_data($where){

		$query = $this->db->query("SELECT * , sum(kas+bank+piutang+persediaan) as jmlhaktiva_lancar, sum(tanah+bangunan+kendaraan) as jmlhaktiva_tetap, sum(htgusaha) as jmlhhutang, sum(mdlusaha+laba) as jmljhmodal,(sum(kas+bank+piutang+persediaan)+sum(tanah+bangunan+kendaraan))as totalaktiva , (sum(htgusaha)+sum(mdlusaha+laba)) as totalhtg FROM neraca WHERE id_user = $where");
		return $query;
	}

	public function edit_pemohon($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>