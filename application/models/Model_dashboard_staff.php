<?php

class Model_dashboard_staff extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->query("SELECT * FROM bio_pemohon, status_pemohon WHERE bio_pemohon.id_user=status_pemohon.id_user");
	}

	public function tambah_user($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_data_status($where){
		return $this->db->query("SELECT * FROM status_pemohon, bio_pemohon WHERE bio_pemohon.id_user=status_pemohon.id_user AND status_pemohon.id_user=$where");
	}

	public function update_data_status($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>