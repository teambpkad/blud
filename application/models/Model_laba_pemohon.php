<?php

class Model_laba_pemohon extends CI_Model
{
	
	public function tambah_laba1($data, $table){
		$this->db->insert($table, $data);
	}

	public function tambah_laba2($data, $table){
		$this->db->insert($table, $data);
	}

	public function tambah_laba3($data, $table){
		$this->db->insert($table, $data);
	}

	public function tampil_data($where){
		$query = $this->db->query("SELECT *, sum(laba1.pendusaha1 + laba1.pendlain1) as totalpend1, sum(laba2.pendusaha2 + laba2.pendlain2) as totalpend2, sum(laba3.pendusaha3 + laba3.pendlain3) as totalpend3, sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1) as totalpeng1, sum(laba2.pengbahan2 + laba2.pengtenaga2 + laba2.penglistrik2 + laba2.pengadm2 + laba2.pengtran2 + laba2.penglain2) as totalpeng2, sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3) as totalpeng3, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba1.penglistrik1 + laba1.pengadm1 + laba1.pengtran1 + laba1.penglain1))  as def1, (sum(laba2.pendusaha2 + laba2.pendlain2) - sum(laba3.pengbahan3 + laba3.pengtenaga3 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba2.penglain2))  as def2, (sum(laba1.pendusaha1 + laba1.pendlain1) - sum(laba1.pengbahan1 + laba1.pengtenaga1 + laba3.penglistrik3 + laba3.pengadm3 + laba3.pengtran3 + laba3.penglain3))  as def3 FROM `laba3`, `laba1` , `laba2` WHERE laba1.id_user = $where AND laba2.id_user = $where AND laba3.id_user = $where");
		return $query;
	}

	public function edit_pemohon($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function edit_pemohon1($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function edit_pemohon2($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>