-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jan 2020 pada 05.48
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blud`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `banding`
--

CREATE TABLE `banding` (
  `id_banding` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `jenis_jaminan_banding1` varchar(100) NOT NULL,
  `alamat_jaminan_banding1` varchar(100) NOT NULL,
  `luas_jaminan_banding1` varchar(100) NOT NULL,
  `dokumen_tanah_banding1` varchar(100) NOT NULL,
  `luas_bangun_banding1` varchar(100) NOT NULL,
  `kontruksi_bangun_banding1` varchar(100) NOT NULL,
  `tahun_bangun_banding1` varchar(100) NOT NULL,
  `harga_penawaran_banding1` varchar(100) NOT NULL,
  `waktu_penjual_banding1` varchar(100) NOT NULL,
  `sumber_dana_banding1` varchar(100) NOT NULL,
  `telp_banding1` varchar(100) NOT NULL,
  `catatan_banding1` varchar(100) NOT NULL,
  `jenis_jaminan_banding2` varchar(100) NOT NULL,
  `alamat_jaminan_banding2` varchar(100) NOT NULL,
  `luas_jaminan_banding2` varchar(100) NOT NULL,
  `dokumen_tanah_banding2` varchar(100) NOT NULL,
  `luas_bangun_banding2` varchar(100) NOT NULL,
  `kontruksi_bangun_banding2` varchar(100) NOT NULL,
  `tahun_bangun_banding2` varchar(100) NOT NULL,
  `harga_penawaran_banding2` varchar(100) NOT NULL,
  `waktu_penjual_banding2` varchar(100) NOT NULL,
  `sumber_dana_banding2` varchar(100) NOT NULL,
  `telp_banding2` varchar(100) NOT NULL,
  `catatan_banding2` varchar(100) NOT NULL,
  `jenis_jaminan_banding3` varchar(100) NOT NULL,
  `alamat_jaminan_banding3` varchar(100) NOT NULL,
  `luas_jaminan_banding3` varchar(100) NOT NULL,
  `dokumen_tanah_banding3` varchar(100) NOT NULL,
  `luas_bangun_banding3` varchar(100) NOT NULL,
  `kontruksi_bangun_banding3` varchar(100) NOT NULL,
  `tahun_bangun_banding3` varchar(100) NOT NULL,
  `harga_penawaran_banding3` varchar(100) NOT NULL,
  `waktu_penjual_banding3` varchar(100) NOT NULL,
  `sumber_dana_banding3` varchar(100) NOT NULL,
  `telp_banding3` varchar(100) NOT NULL,
  `catatan_banding3` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `banding`
--

INSERT INTO `banding` (`id_banding`, `id_user`, `jenis_jaminan_banding1`, `alamat_jaminan_banding1`, `luas_jaminan_banding1`, `dokumen_tanah_banding1`, `luas_bangun_banding1`, `kontruksi_bangun_banding1`, `tahun_bangun_banding1`, `harga_penawaran_banding1`, `waktu_penjual_banding1`, `sumber_dana_banding1`, `telp_banding1`, `catatan_banding1`, `jenis_jaminan_banding2`, `alamat_jaminan_banding2`, `luas_jaminan_banding2`, `dokumen_tanah_banding2`, `luas_bangun_banding2`, `kontruksi_bangun_banding2`, `tahun_bangun_banding2`, `harga_penawaran_banding2`, `waktu_penjual_banding2`, `sumber_dana_banding2`, `telp_banding2`, `catatan_banding2`, `jenis_jaminan_banding3`, `alamat_jaminan_banding3`, `luas_jaminan_banding3`, `dokumen_tanah_banding3`, `luas_bangun_banding3`, `kontruksi_bangun_banding3`, `tahun_bangun_banding3`, `harga_penawaran_banding3`, `waktu_penjual_banding3`, `sumber_dana_banding3`, `telp_banding3`, `catatan_banding3`) VALUES
(1, 0, '1', '12', '1q', '1', '21', '13', '1', '13', '13', '12', '12', '12', '21', '21', '21', '21', '', '21', '21', '21', '21', '21', '21', '1', '21', '12', '2', 'w122', '31S', '123', '1Q', '12', '12', '12', '21', 'DW');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bio_pemohon`
--

CREATE TABLE `bio_pemohon` (
  `id_pemohon` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `tgl_input` date NOT NULL,
  `nama_lengkap` varchar(200) NOT NULL,
  `no_ktp` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `alamat_ktp` text NOT NULL,
  `alamat_domisili` text NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `tempat_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(20) NOT NULL,
  `pendidikan` varchar(200) NOT NULL,
  `nama_ibu` varchar(200) NOT NULL,
  `nama_kerabat` varchar(200) NOT NULL,
  `hp_kerabat` varchar(20) NOT NULL,
  `nominal` int(11) NOT NULL,
  `keperluan` varchar(200) NOT NULL,
  `status_rumah` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bio_pemohon`
--

INSERT INTO `bio_pemohon` (`id_pemohon`, `id_user`, `tgl_input`, `nama_lengkap`, `no_ktp`, `status`, `alamat_ktp`, `alamat_domisili`, `no_hp`, `tempat_lahir`, `tgl_lahir`, `jk`, `pendidikan`, `nama_ibu`, `nama_kerabat`, `hp_kerabat`, `nominal`, `keperluan`, `status_rumah`) VALUES
(5, 2, '2019-12-05', 'RANDI PERDANA ARMAN', '2171110504949001', 'Belum Kawin', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', '087894336773', 'Batam', '1994-04-05', 'Laki-Laki', 'S1 (Sistem Informasi)', 'Jurmiati', 'Ardi', '087894336773', 5000000, 'Tambahan Dana untuk Usaha Air Galon', 'Milik Sendiri'),
(6, 5, '2019-12-06', 'SIARMAN', '2171110504949001', 'Belum Kawin', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', '087894336773', 'Batam', '1994-04-05', 'Laki-Laki', 'S1 (Sistem Informasi)', 'Jurmiati', 'Ardi', '087894336773', 10000000, 'Untuk Membeli Mesin Jahit 2020', 'Sewa'),
(7, 8, '2019-12-19', 'Aas komaria bt odt bn asmin', '217111', 'Menikah', 'sungai pelungut', 'sungai pelungut', '0878', 'batam', '2019-12-19', 'Perempuan', 'SMA', 'MAMA', 'Kerabat', 'Kerabat', 20000, '', 'Sewa'),
(8, 7, '2019-12-20', 'adsa', '2171110504949001', 'Menikah', 'ad', 'ad', 'ad', 'ad', '2019-12-20', 'Laki-Laki', 'ada', 'ad', 'ad', 'ad', 0, '', 'Milik Sendiri');

-- --------------------------------------------------------

--
-- Struktur dari tabel `file_pemohon`
--

CREATE TABLE `file_pemohon` (
  `id_file` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `ktp_suami` text NOT NULL,
  `ktp_istri` text NOT NULL,
  `kk` text NOT NULL,
  `iumk` text NOT NULL,
  `buku_nikah` text NOT NULL,
  `foto_suami` text NOT NULL,
  `foto_istri` text NOT NULL,
  `foto_usaha` text NOT NULL,
  `ser_jaminan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `file_pemohon`
--

INSERT INTO `file_pemohon` (`id_file`, `id_user`, `ktp_suami`, `ktp_istri`, `kk`, `iumk`, `buku_nikah`, `foto_suami`, `foto_istri`, `foto_usaha`, `ser_jaminan`) VALUES
(5, 2, 'KTP.jpg', 'KTP.jpg', 'KK.jpg', 'Capture.PNG', 'Capture.PNG', 'Randi_merah.jpg', 'Randi_merah.jpg', 'ijazah.jpg', 'Transkrip_Nilai.jpg'),
(6, 5, 'W1.jpg', 'W11.jpg', 'W12.jpg', 'W13.jpg', 'W14.jpg', 'W15.jpg', 'W16.jpg', 'W17.jpg', 'W18.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jaminan_pemohon`
--

CREATE TABLE `jaminan_pemohon` (
  `id_jaminan_pemohon` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `bentuk_jaminan` varchar(200) NOT NULL,
  `bukti_jaminan` varchar(200) NOT NULL,
  `tgl_jaminan` date NOT NULL,
  `nama_pemilik` varchar(200) NOT NULL,
  `nilai_jaminan` varchar(200) NOT NULL,
  `alamat_jaminan` text NOT NULL,
  `status_jaminan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jaminan_pemohon`
--

INSERT INTO `jaminan_pemohon` (`id_jaminan_pemohon`, `id_user`, `bentuk_jaminan`, `bukti_jaminan`, `tgl_jaminan`, `nama_pemilik`, `nilai_jaminan`, `alamat_jaminan`, `status_jaminan`) VALUES
(1, 5, 'gatau', 'asdadsa', '2019-12-11', 'SiArman', 'sadasd', 'Batam', 'Milik Sendiri'),
(2, 2, 'Mobil', '123', '2019-12-18', 'Randi Perdana Arman', 'Barang', 'Batu Aji', 'Milik Sendiri'),
(4, 6, 'as', 'asd', '2019-12-28', 'ad', 'ad', 'ada', 'Sewa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuasa`
--

CREATE TABLE `kuasa` (
  `id_kuasa` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nm_kuasa` varchar(200) NOT NULL,
  `no_ktp_kuasa` varchar(100) NOT NULL,
  `tempat_lahir_kuasa` text NOT NULL,
  `tgl_lahir_kuasa` date NOT NULL,
  `alamat_kuasa` text NOT NULL,
  `nm_saksi1` varchar(200) NOT NULL,
  `nm_saksi2` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuasa`
--

INSERT INTO `kuasa` (`id_kuasa`, `id_user`, `nm_kuasa`, `no_ktp_kuasa`, `tempat_lahir_kuasa`, `tgl_lahir_kuasa`, `alamat_kuasa`, `nm_saksi1`, `nm_saksi2`) VALUES
(1, 2, 'SITI AISYAH', '2171110504949902', 'Batam', '1994-12-20', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', 'Papa', 'Mama'),
(2, 5, 'JURMIATI', '2171110504949001', 'Padang', '2020-01-01', 'Kavling Saguba Blok P 157', 'Abang', 'Adek');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laba1`
--

CREATE TABLE `laba1` (
  `id_laba1` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `bulan1` varchar(20) NOT NULL,
  `pendusaha1` varchar(25) NOT NULL,
  `pendlain1` varchar(25) NOT NULL,
  `pengbahan1` varchar(25) NOT NULL,
  `pengtenaga1` varchar(25) NOT NULL,
  `penglistrik1` varchar(25) NOT NULL,
  `pengadm1` varchar(25) NOT NULL,
  `pengtran1` varchar(25) NOT NULL,
  `penglain1` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laba1`
--

INSERT INTO `laba1` (`id_laba1`, `id_user`, `bulan1`, `pendusaha1`, `pendlain1`, `pengbahan1`, `pengtenaga1`, `penglistrik1`, `pengadm1`, `pengtran1`, `penglain1`) VALUES
(1, 2, 'Januari', '4200000', '5000000', '0', '0', '1000000', '0', '0', '0'),
(2, 6, 'Maret', '15000', '12', '12', '13', '13', '13', '13', '13'),
(3, 5, 'Januari', '2', '2', '2', '2', '2', '2', '2', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laba2`
--

CREATE TABLE `laba2` (
  `id_laba2` int(11) NOT NULL,
  `id_user` int(10) NOT NULL,
  `bulan2` varchar(20) NOT NULL,
  `pendusaha2` int(10) NOT NULL,
  `pendlain2` int(10) NOT NULL,
  `pengbahan2` int(10) NOT NULL,
  `pengtenaga2` int(10) NOT NULL,
  `penglistrik2` int(10) NOT NULL,
  `pengadm2` int(10) NOT NULL,
  `pengtran2` int(10) NOT NULL,
  `penglain2` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laba2`
--

INSERT INTO `laba2` (`id_laba2`, `id_user`, `bulan2`, `pendusaha2`, `pendlain2`, `pengbahan2`, `pengtenaga2`, `penglistrik2`, `pengadm2`, `pengtran2`, `penglain2`) VALUES
(1, 2, 'Februari', 4200000, 5000000, 0, 0, 1000000, 0, 0, 0),
(2, 6, 'Mei', 4200000, 13, 12, 13, 13, 31, 123, 13),
(3, 5, 'Januari', 2, 2, 2, 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `laba3`
--

CREATE TABLE `laba3` (
  `id_laba3` int(11) NOT NULL,
  `id_user` int(10) NOT NULL,
  `bulan3` varchar(20) NOT NULL,
  `pendusaha3` int(10) NOT NULL,
  `pendlain3` int(10) NOT NULL,
  `pengbahan3` int(10) NOT NULL,
  `pengtenaga3` int(10) NOT NULL,
  `penglistrik3` int(10) NOT NULL,
  `pengadm3` int(10) NOT NULL,
  `pengtran3` int(10) NOT NULL,
  `penglain3` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laba3`
--

INSERT INTO `laba3` (`id_laba3`, `id_user`, `bulan3`, `pendusaha3`, `pendlain3`, `pengbahan3`, `pengtenaga3`, `penglistrik3`, `pengadm3`, `pengtran3`, `penglain3`) VALUES
(1, 2, 'Maret', 4200000, 5000000, 0, 0, 1000000, 0, 0, 0),
(2, 6, 'Juli', 132, 12, 13, 13, 13, 1, 13, 87),
(3, 5, 'Januari', 2, 2, 2, 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `neraca`
--

CREATE TABLE `neraca` (
  `id_neraca` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `kas` int(11) NOT NULL,
  `bank` int(11) NOT NULL,
  `piutang` int(10) NOT NULL,
  `persediaan` int(10) NOT NULL,
  `htgusaha` int(10) NOT NULL,
  `tanah` int(10) NOT NULL,
  `bangunan` int(10) NOT NULL,
  `kendaraan` int(10) NOT NULL,
  `mdlusaha` int(10) NOT NULL,
  `laba` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `neraca`
--

INSERT INTO `neraca` (`id_neraca`, `id_user`, `bulan`, `kas`, `bank`, `piutang`, `persediaan`, `htgusaha`, `tanah`, `bangunan`, `kendaraan`, `mdlusaha`, `laba`) VALUES
(1, 2, 'Januari', 12, 12, 12, 12, 12, 12, 12, 12, 12, 12),
(2, 5, 'Januari', 2, 2, 2, 2, 2, 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_pemohon`
--

CREATE TABLE `status_pemohon` (
  `id_status_pemohon` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `tgl_pemohon` date NOT NULL,
  `status_survei` varchar(50) NOT NULL,
  `hasil_survei` varchar(50) NOT NULL,
  `status_akhir` varchar(50) NOT NULL,
  `ket` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status_pemohon`
--

INSERT INTO `status_pemohon` (`id_status_pemohon`, `id_user`, `tgl_pemohon`, `status_survei`, `hasil_survei`, `status_akhir`, `ket`) VALUES
(2, 2, '2019-12-22', 'Sudah', 'yes', 'Sudah', 'Selesai\r\n'),
(4, 5, '2020-01-02', 'Belum', 'Belum', 'Belum', 'Sudah Mengajukan Permohonan, Mohon Menunggu Informasi Selanjutnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `survei`
--

CREATE TABLE `survei` (
  `id_survei` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nama_survei` varchar(200) NOT NULL,
  `rumah` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `lama` varchar(100) NOT NULL,
  `omset` int(11) NOT NULL,
  `operasional` int(11) NOT NULL,
  `keluarga` int(11) NOT NULL,
  `adm` varchar(100) NOT NULL,
  `etika` varchar(100) NOT NULL,
  `cicilan` int(11) NOT NULL,
  `sumber` varchar(100) NOT NULL,
  `hal` text NOT NULL,
  `tgl_survei` date NOT NULL,
  `no_jaminan` varchar(50) NOT NULL,
  `tgl_keluar_jaminan` date NOT NULL,
  `tgl_jatuh_jaminan` date NOT NULL,
  `luas_jaminan` varchar(200) NOT NULL,
  `tahun_bangun` varchar(10) NOT NULL,
  `tahun_renovasi` varchar(10) NOT NULL,
  `umur_efektif` varchar(10) NOT NULL,
  `luas_bangunan_fisik` varchar(100) NOT NULL,
  `luas_bangunan_imb` varchar(100) NOT NULL,
  `luas_tapak_bangunan` varchar(100) NOT NULL,
  `tahun_huni` varchar(10) NOT NULL,
  `jenis_bangunan` varchar(100) NOT NULL,
  `pemakai_bangunan` varchar(100) NOT NULL,
  `fasilitas_bangunan` varchar(200) NOT NULL,
  `taman` varchar(50) NOT NULL,
  `halaman` varchar(50) NOT NULL,
  `pagar` varchar(50) NOT NULL,
  `lainnya` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `survei`
--

INSERT INTO `survei` (`id_survei`, `id_user`, `nama_survei`, `rumah`, `lokasi`, `lama`, `omset`, `operasional`, `keluarga`, `adm`, `etika`, `cicilan`, `sumber`, `hal`, `tgl_survei`, `no_jaminan`, `tgl_keluar_jaminan`, `tgl_jatuh_jaminan`, `luas_jaminan`, `tahun_bangun`, `tahun_renovasi`, `umur_efektif`, `luas_bangunan_fisik`, `luas_bangunan_imb`, `luas_tapak_bangunan`, `tahun_huni`, `jenis_bangunan`, `pemakai_bangunan`, `fasilitas_bangunan`, `taman`, `halaman`, `pagar`, `lainnya`) VALUES
(2, 2, 'ardi', 'Milik Sendiri', 'Pemukiman', '1 Tahun', 1000, 1000, 1000, 'Ada dokumen, tercatat sesuai kaidah akuntansi & rapi', '', 100, 'dagang', 'bagus', '2019-12-31', '12', '2019-12-31', '2019-12-31', '121', '1999', '1990', '12', '100M', '100M', '100M', '2001', 'Rumah', '12', '12', '12', '1', '12', '12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_log`
--

CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL,
  `log_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_log`
--

INSERT INTO `tabel_log` (`log_id`, `log_time`, `log_user`, `log_tipe`, `log_desc`) VALUES
(1, '2019-10-31 03:14:42', NULL, 2, 'Tambah Data Req pemohon'),
(2, '2019-10-31 03:17:43', NULL, 2, 'Tambah Data Req pemohon'),
(3, '2019-10-31 03:26:25', NULL, 2, 'Tambah Data Req pemohon'),
(4, '2019-10-31 03:27:02', NULL, 2, 'Tambah Data Req pemohon'),
(5, '2019-11-19 04:49:28', NULL, 2, 'Tambah Data User'),
(6, '2019-11-21 09:09:15', 'ardi', 0, 'Login'),
(7, '2019-11-25 05:56:07', NULL, 2, 'Tambah Data Bio Pemohon'),
(8, '2019-11-26 02:39:57', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(9, '2019-11-26 02:41:59', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(10, '2019-11-27 00:30:00', NULL, 2, 'Tambah Data User'),
(11, '2019-11-27 00:30:39', 'edo', 2, 'Tambah Data Bio Pemohon'),
(12, '2019-11-27 06:06:51', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(13, '2019-11-27 06:09:27', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(14, '2019-11-27 06:22:02', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(15, '2019-11-27 06:36:41', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(16, '2019-11-27 07:55:29', 'edo', 2, 'Tambah Data Bio Pemohon'),
(17, '2019-11-29 02:23:48', 'edo', 2, 'Tambah Data Bio Pemohon'),
(18, '2019-12-05 07:27:26', 'ardi', 2, 'Tambah Data Bio Pemohon'),
(19, '2019-12-06 02:50:31', 'erma', 2, 'Tambah Data Bio Pemohon'),
(20, '2019-12-19 03:53:32', '21711111511269002', 2, 'Tambah Data Biodata Pemohon'),
(21, '2019-12-20 00:31:40', '2171110504949001', 2, 'Tambah Data Biodata Pemohon'),
(22, '2019-12-20 04:17:40', 'erma', 2, 'Tambah Data Kuasa Pemohon'),
(23, '2019-12-20 04:30:27', 'erma', 2, 'Tambah Data Kuasa Pemohon'),
(24, '2019-12-22 05:30:19', NULL, 2, 'Tambah Data File Pemohon'),
(25, '2019-12-22 05:34:04', NULL, 2, 'Tambah Data File Pemohon'),
(26, '2019-12-28 02:50:49', 'erma', 2, 'Tambah Data Jaminan Pemohon'),
(27, '2020-01-02 01:03:03', 'edo', 2, 'Tambah Data Laba Pemohon'),
(28, '2020-01-02 01:03:12', 'edo', 2, 'Tambah Data Neraca Pemohon'),
(29, '2020-01-02 01:03:52', 'edo', 2, 'Tambah Data File Pemohon'),
(30, '2020-01-07 03:24:47', 'Ardia', 2, 'Tambah Data Laba Pemohon'),
(31, '2020-01-07 03:30:28', 'Ardia', 2, 'Tambah Data Laba Pemohon'),
(32, '2020-01-07 03:33:03', 'Ardia', 2, 'Tambah Data Laba Pemohon'),
(33, '2020-01-07 07:25:08', 'Ardia', 2, 'Tambah Data Laba Pemohon');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usaha_pemohon`
--

CREATE TABLE `usaha_pemohon` (
  `id_usaha_pemohon` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nama_usaha` varchar(200) NOT NULL,
  `jmlh_karyawan` varchar(20) NOT NULL,
  `alamat_usaha` text NOT NULL,
  `bidang_usaha` varchar(100) NOT NULL,
  `mulai_usaha` varchar(100) NOT NULL,
  `hp_usaha` varchar(20) NOT NULL,
  `status_usaha` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usaha_pemohon`
--

INSERT INTO `usaha_pemohon` (`id_usaha_pemohon`, `id_user`, `nama_usaha`, `jmlh_karyawan`, `alamat_usaha`, `bidang_usaha`, `mulai_usaha`, `hp_usaha`, `status_usaha`) VALUES
(2, 6, 'JUAL HP', '2', 'BAtam', 'Elektronik', 'gatau', '087894336773', 'Milik Sendiri'),
(3, 2, 'GALON', '2', 'Kavling Sagulung Baru Blok P.157, Kel Sei Binti, Kec Sagulung', 'Jasa', '2012', '087894336773', 'Milik Sendiri'),
(4, 5, 'TUKANG JAHIT', '2', 'batam', 'gatau', 'gatau', '087894336773', 'Sewa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `pass`, `level`) VALUES
(1, 'asda', '523af537946b79c4f8369ed39ba78605', 'pemohon'),
(2, 'ardi', '202cb962ac59075b964b07152d234b70', 'pemohon'),
(3, 'ibnu', '202cb962ac59075b964b07152d234b70', 'admin'),
(4, 'randi', '202cb962ac59075b964b07152d234b70', 'staff'),
(5, 'edo', '202cb962ac59075b964b07152d234b70', 'pemohon'),
(6, 'erma', '202cb962ac59075b964b07152d234b70', 'kasubid'),
(7, '2171110504949001', '21232f297a57a5a743894a0e4a801fc3', 'pemohon'),
(8, 'ryo', '202cb962ac59075b964b07152d234b70', 'survei'),
(13, 'ryo', '202cb962ac59075b964b07152d234b70', 'pemohon'),
(14, 'rio', '202cb962ac59075b964b07152d234b70', 'pemohon'),
(15, 'Ardia', '202cb962ac59075b964b07152d234b70', 'pemohon');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `banding`
--
ALTER TABLE `banding`
  ADD PRIMARY KEY (`id_banding`);

--
-- Indeks untuk tabel `bio_pemohon`
--
ALTER TABLE `bio_pemohon`
  ADD PRIMARY KEY (`id_pemohon`);

--
-- Indeks untuk tabel `file_pemohon`
--
ALTER TABLE `file_pemohon`
  ADD PRIMARY KEY (`id_file`);

--
-- Indeks untuk tabel `jaminan_pemohon`
--
ALTER TABLE `jaminan_pemohon`
  ADD PRIMARY KEY (`id_jaminan_pemohon`);

--
-- Indeks untuk tabel `kuasa`
--
ALTER TABLE `kuasa`
  ADD PRIMARY KEY (`id_kuasa`);

--
-- Indeks untuk tabel `laba1`
--
ALTER TABLE `laba1`
  ADD PRIMARY KEY (`id_laba1`);

--
-- Indeks untuk tabel `laba2`
--
ALTER TABLE `laba2`
  ADD PRIMARY KEY (`id_laba2`);

--
-- Indeks untuk tabel `laba3`
--
ALTER TABLE `laba3`
  ADD PRIMARY KEY (`id_laba3`);

--
-- Indeks untuk tabel `neraca`
--
ALTER TABLE `neraca`
  ADD PRIMARY KEY (`id_neraca`);

--
-- Indeks untuk tabel `status_pemohon`
--
ALTER TABLE `status_pemohon`
  ADD PRIMARY KEY (`id_status_pemohon`);

--
-- Indeks untuk tabel `survei`
--
ALTER TABLE `survei`
  ADD PRIMARY KEY (`id_survei`);

--
-- Indeks untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indeks untuk tabel `usaha_pemohon`
--
ALTER TABLE `usaha_pemohon`
  ADD PRIMARY KEY (`id_usaha_pemohon`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `banding`
--
ALTER TABLE `banding`
  MODIFY `id_banding` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `bio_pemohon`
--
ALTER TABLE `bio_pemohon`
  MODIFY `id_pemohon` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `file_pemohon`
--
ALTER TABLE `file_pemohon`
  MODIFY `id_file` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `jaminan_pemohon`
--
ALTER TABLE `jaminan_pemohon`
  MODIFY `id_jaminan_pemohon` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kuasa`
--
ALTER TABLE `kuasa`
  MODIFY `id_kuasa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `laba1`
--
ALTER TABLE `laba1`
  MODIFY `id_laba1` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `laba2`
--
ALTER TABLE `laba2`
  MODIFY `id_laba2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `laba3`
--
ALTER TABLE `laba3`
  MODIFY `id_laba3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `neraca`
--
ALTER TABLE `neraca`
  MODIFY `id_neraca` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `status_pemohon`
--
ALTER TABLE `status_pemohon`
  MODIFY `id_status_pemohon` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `survei`
--
ALTER TABLE `survei`
  MODIFY `id_survei` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `usaha_pemohon`
--
ALTER TABLE `usaha_pemohon`
  MODIFY `id_usaha_pemohon` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `usaha_pemohon`
--
ALTER TABLE `usaha_pemohon`
  ADD CONSTRAINT `usaha_pemohon_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
